#include "core.h"
#include "db.h"
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <libsam3a.h>
#include "i2pjava.h"
#include "corelinux.h"

//Constants
#define MAX_I2P_DEVICE_REQUEST    128
#define I2P_CONNECTION_TIMEOUT_MS 9000

//Synchronization Types
#define I2P_STATE_PRESESSION      0
#define I2P_STATE_SESSIONREADY    1
#define I2P_STATE_SESSIONRUNNING  2

//Global variables
int i2p_state = I2P_STATE_PRESESSION;
sqlite3_int64 DeviceIDs[MAX_I2P_DEVICE_REQUEST];
Sam3AConnection* _Sam3AConnections[MAX_I2P_DEVICE_REQUEST];
const char* i2p_public_key_string_refs[MAX_I2P_DEVICE_REQUEST];
char* i2p_public_key_strings;

//Communications
typedef struct
{
    char privkey[SAM3A_PRIVKEY_SIZE+1]; /**  private key (asciiz) */
    char pubkey[SAM3A_PUBKEY_SIZE+1]; /** public key (asciiz) */
} KeyData;
typedef struct
{
    char *str;
    int strsize;
    int strused;
    int doQuit;
} ConnData;


static void cdAppendChar (ConnData *d, char ch) {
  if (d->strused+1 >= d->strsize) {
    // fuck errors
    d->strsize = d->strused+1024;
    d->str = realloc(d->str, d->strsize+1);
  }
  d->str[d->strused++] = ch;
  d->str[d->strused] = 0;
}


//Connection
static void ccbError (Sam3AConnection *pSam3AConnection)
{
  fprintf(stderr, "\n===============================\nCONNECTION_ERROR: [%s]\n===============================\n", pSam3AConnection->error);
}

static void ccbDisconnected (Sam3AConnection *pSam3AConnection)
{
  fprintf(stderr, "\n===============================\nCONNECTION_DISCONNECTED\n===============================\n");
}

static void ccbConnected (Sam3AConnection *pSam3AConnection)
{
  fprintf(stderr, "\n===============================\nCONNECTION_CONNECTED\n===============================\n");
  //sam3aCancelConnection(ct); // cbSent() will not be called
}

static void ccbAccepted (Sam3AConnection *pSam3AConnection)
{
  fprintf(stderr, "\n===============================\nCONNECTION_ACCEPTED\n===============================\n");
  fprintf(stderr, "FROM: %s\n===============================\n", pSam3AConnection->destkey);
}

static void ccbSent (Sam3AConnection *pSam3AConnection)
{
  ConnData *d = (ConnData *)pSam3AConnection->udata;
  //
  fprintf(stderr, "\n===============================\nCONNECTION_WANTBYTES\n===============================\n");
  if (d->doQuit)
  {
    sam3aCancelSession(pSam3AConnection->ses); // hehe
  }
}

static void ccbRead (Sam3AConnection *pSam3AConnection, const void *buf, int bufsize)
{
  const char *b = (const char *)buf;
  ConnData *d = (ConnData *)pSam3AConnection->udata;
  //
  fprintf(stderr, "\n===============================\nCONNECTION_GOTBYTES (%d)\n===============================\n", bufsize);
  while (bufsize > 0)
  {
    cdAppendChar(pSam3AConnection->udata, *b);
    if (*b == '\n')
    {
      fprintf(stderr, "cmd: %s", d->str);
      if (strcasecmp(d->str, "quit\n") == 0)
        d->doQuit = 1;
      if (sam3aSend(pSam3AConnection, d->str, -1) < 0)
      {
        //sam3aCancelConnection(pSam3AConnection); // hehe
        sam3aCancelSession(pSam3AConnection->ses); // hehe
        return;
      }
      d->str[0] = 0;
      d->strused = 0;
    }
    ++b;
    --bufsize;
  }
}

static void ccbDestroy (Sam3AConnection *pSam3AConnection)
{
  fprintf(stderr, "\n===============================\nCONNECTION_DESTROY\n===============================\n");
  if (pSam3AConnection->udata != NULL)
  {
    ConnData *d = (ConnData *)pSam3AConnection->udata;
    //
    if (d->str != NULL) free(d->str);
    free(d);
  }
}

static const Sam3AConnectionCallbacks _Sam3AConnectionCallbacks = {
  .cbError = ccbError,
  .cbDisconnected = ccbDisconnected,
  .cbConnected = ccbConnected,
  .cbAccepted = ccbAccepted,
  .cbSent = ccbSent,
  .cbRead = ccbRead,
  .cbDestroy = ccbDestroy,
};

//Session
static void scbError (Sam3ASession *ses) {
  fprintf(stderr, "\n===============================\nSESION_ERROR: [%s]\n===============================\n", ses->error);
}

static void scbCreatedKeys(Sam3ASession *pSam3ASession)
{
  Sam3AConnection *conn;
  KeyData *pKeyData;

  pKeyData = (KeyData*)pSam3ASession->udata;

  if (!pKeyData)
  {
    pSam3ASession->udata = calloc(1, sizeof(KeyData));
    pKeyData = (KeyData*)pSam3ASession->udata;
  }

  strncpy(pKeyData->pubkey, pSam3ASession->pubkey, SAM3A_PUBKEY_SIZE);
  strncpy(pKeyData->privkey, pSam3ASession->privkey, SAM3A_PRIVKEY_SIZE);
}

static void scbCreatedConnection(Sam3ASession *pSam3ASession)
{
    Sam3AConnection *conn;

    //
    fprintf(stderr, "\n===============================\nSESION_CREATED\n");
    fprintf(stderr, "\rPRIV: %s\n", pSam3ASession->privkey);
    fprintf(stderr, "\nPUB: %s\n===============================\n", pSam3ASession->pubkey);
    //

    if ((conn = sam3aStreamAccept(pSam3ASession, &_Sam3AConnectionCallbacks)) == NULL)
    {
      fprintf(stderr, "ERROR: CAN'T CREATE CONNECTION!\n");
      sam3aCancelSession(pSam3ASession);
      return;
    }

    //
    fprintf(stderr, "\n===============================\nSESION_CREATED\n");
    fprintf(stderr, "\rDEST: %s\n", conn->destkey);
    //

    conn->udata = calloc(1, sizeof(ConnData));
    fprintf(stderr, "GOON: accepting connection...\n");
    i2p_state = I2P_STATE_SESSIONREADY;
}


static void scbDisconnected(Sam3ASession *pSam3ASession)
{
  fprintf(stderr, "\n===============================\nSESION_DISCONNECTED\n===============================\n");
}


static void scbDGramRead(Sam3ASession *pSam3ASession, const void *buf, int bufsize)
{
  fprintf(stderr, "\n===============================\nSESION_DATAGRAM_READ\n===============================\n");
}


static void scbDestroy(Sam3ASession *pSam3ASession)
{
  fprintf(stderr, "\n===============================\nSESION_DESTROYED\n===============================\n");
}

static void scbDestroyKeys (Sam3ASession *pSam3ASession)
{
  if (pSam3ASession->udata != NULL)
  {
    KeyData *pKeyData = (KeyData *)pSam3ASession->udata;
    //
    free(pKeyData);
  }
}

static void scbErrorClose (Sam3ASession *pSam3ASession)
{
  fprintf(stderr, "\n===============================\nSESION_ERROR: [%s]\n===============================\n", pSam3ASession->error);
  sam3aCloseSession(pSam3ASession); // it's safe here
}

static const Sam3ASessionCallbacks _Sam3ASessionCallbacks_Keys = {
    .cbError = scbErrorClose,
    .cbCreated = scbCreatedKeys,
    .cbDisconnected = NULL,
    .cbDatagramRead = NULL,
    .cbDestroy = scbDestroyKeys,
};
static const Sam3ASessionCallbacks _Sam3ASessionCallbacks_Run = {
    .cbError = scbError,
    .cbCreated = scbCreatedConnection,
    .cbDisconnected = scbDisconnected,
    .cbDatagramRead = scbDGramRead,
    .cbDestroy = scbDestroy,
};


////////////////////////////////////////////////////////////////////////////////
#define HOST  SAM3A_HOST_DEFAULT

int create_i2p_keys(sqlite3 *db, sqlite3_int64 mDeviceID)
{
  Sam3ASession _Sam3ASession;
  fd_set rds, wrs;
  int maxfd, res;
  struct timeval to;
  KeyData *pKeyData;
  const char* sqlBufferUnused;
  sqlite3_stmt* statement;

  libsam3a_debug = 0;

  if (sam3aGenerateKeys(&_Sam3ASession, &_Sam3ASessionCallbacks_Keys, HOST, SAM3A_PORT_DEFAULT) < 0)
  {
    fprintf(stderr, "FATAL: can't generate keys!\n");
    return 1;
  }

  while (sam3aIsActiveSession(&_Sam3ASession))
  {
      // set up file descriptors for select()
      FD_ZERO(&rds);
      FD_ZERO(&wrs);

      maxfd = sam3aAddSessionToFDS(&_Sam3ASession, -1, &rds, &wrs);
      if (maxfd < 0)
      {
        break;
      }

      // set timeout to 1 second
      sam3ams2timeval(&to, 1000);

      // call select()
      res = select(maxfd+1, &rds, &wrs, NULL, &to);
      if (res < 0)
      {
        if (errno == EINTR)
          continue;
        fprintf(stderr, "FATAL: select() error!\n");
        break;
      }
      if (res == 0)
      {
        // idle, no activity
        fprintf(stdout, "."); fflush(stdout);
      }
      else
      {
        // we have activity, process io
        if (sam3aIsActiveSession(&_Sam3ASession))
          sam3aProcessSessionIO(&_Sam3ASession, &rds, &wrs);
      }
  }

  //Add to db
  pKeyData = (KeyData*)_Sam3ASession.udata;
  if (!pKeyData)
  {//Can't get key
      //TODO: log error
      goto ERROR;
  }

  //Update devices
  res = sqlite3_prepare_v3(db, SQL_UPDATE_DEVICES_FOR_I2P, -1, 0, &statement, &sqlBufferUnused);
  if (res)
  {
      //TODO: log error
      goto ERROR;
  }
  //I2PPublicKey
  res = sqlite3_bind_text(statement, 1, pKeyData->pubkey, -1, SQLITE_STATIC);
  if (res)
  {
      //TODO: log error
      goto ERROR_STATEMENT;
  }
  //I2PPrivateKey
  res = sqlite3_bind_text(statement, 2, pKeyData->privkey, -1, SQLITE_STATIC);
  if (res)
  {
      //TODO: log error
      goto ERROR_STATEMENT;
  }
  //DeviceID
  res = sqlite3_bind_int64(statement, 3, mDeviceID);
  if (res)
  {
      //TODO: log error
      goto ERROR_STATEMENT;
  }
  res = sqlite3_step(statement);
  if (res != SQLITE_DONE)
  {
      //TODO: log error
      goto ERROR_STATEMENT;
  }
  res = sqlite3_finalize(statement);
  if (res)
  {
      //TODO: log error
      goto ERROR;
  }

  sam3aCloseSession(&_Sam3ASession);

  return 0;

ERROR_STATEMENT:
  res = sqlite3_finalize(statement);
ERROR:
  sam3aCloseSession(&_Sam3ASession);
  return -1;
}

int get_i2p_publickey(sqlite3 *db, sqlite3_int64 DeviceID, char* pubkey)
{
  const char* sqlBufferUnused;
  sqlite3_stmt* statement;
  int res;
  const unsigned char* text;

  //Query device
  res = sqlite3_prepare_v3(db, SQL_QUERY_I2PPUBLICKEY_BY_DEVICEID, -1, 0, &statement, &sqlBufferUnused);
  if (res)
  {
      goto ERROR;
  }

  //DeviceID
  res = sqlite3_bind_int64(statement, 1, DeviceID);
  if (res)
  {
      goto ERROR_STATEMENT;
  }

  res = sqlite3_step(statement);
  if (res != SQLITE_ROW)
  {
    goto ERROR_STATEMENT;
  }

  text = sqlite3_column_text(statement, 0);
  strncpy(pubkey, (const char*)text, SAM3A_PUBKEY_SIZE);

  res = sqlite3_finalize(statement);
  if (res)
  {
      goto ERROR;
  }

  return 0;

ERROR_STATEMENT:
  res = sqlite3_finalize(statement);
ERROR:
  return -1;
}

int run_i2p(sqlite3 *db, sqlite3_int64 mDeviceID, const char** error)
{
  Sam3ASession _Sam3ASession;
  char privkey[SAM3A_PRIVKEY_SIZE+1];
  const char* sqlBufferUnused;
  sqlite3_stmt* statement;
  int res;
  const unsigned char* text;
  fd_set rds, wrs;
  int maxfd;
  struct timeval to;
  struct timespec tp_now;
  struct timespec tp_timeout;
  unsigned short devicecount;
  int loop;

  i2p_state = I2P_STATE_PRESESSION;
  i2p_public_key_strings = calloc(MAX_I2P_DEVICE_REQUEST, SAM3A_PUBKEY_SIZE+1);

  res = NOW_plus_seconds(180, &tp_timeout);
  if (res)
  {
      goto ERROR;
  }

  //Query device
  res = sqlite3_prepare_v3(db, SQL_QUERY_I2PPRIVATEKEY_BY_DEVICEID, -1, 0, &statement, &sqlBufferUnused);
  if (res)
  {
      goto ERROR;
  }

  //DeviceID
  res = sqlite3_bind_int64(statement, 1, mDeviceID);
  if (res)
  {
      goto ERROR_STATEMENT;
  }

  res = sqlite3_step(statement);
  if (res != SQLITE_ROW)
  {
      goto ERROR_STATEMENT;
  }

  if (sqlite3_column_type(statement, 0) == SQLITE_NULL)
  {
      goto ERROR_STATEMENT;
  }

  text = sqlite3_column_text(statement, 0);
  strncpy(privkey, (const char*)text, SAM3A_PRIVKEY_SIZE);

  res = sqlite3_finalize(statement);
  if (res)
  {
      goto ERROR;
  }

  libsam3a_debug = 0;

  if (sam3aCreateSession(&_Sam3ASession, &_Sam3ASessionCallbacks_Run, HOST, SAM3A_PORT_DEFAULT, privkey, SAM3A_SESSION_STREAM) < 0)
  {
    fprintf(stderr, "FATAL: can't create main session!\n");
    goto ERROR;
  }

  while (sam3aIsActiveSession(&_Sam3ASession))
  {
    FD_ZERO(&rds);
    FD_ZERO(&wrs);
    if ((maxfd = sam3aAddSessionToFDS(&_Sam3ASession, -1, &rds, &wrs)) < 0)
      break;
    sam3ams2timeval(&to, 1000);
    res = select(maxfd+1, &rds, &wrs, NULL, &to);
    if (res < 0)
    {
      if (errno == EINTR)
        continue;
      fprintf(stderr, "FATAL: select() error!\n");
      break;
    }
    if (res == 0)
    {
      if (i2p_state == I2P_STATE_SESSIONREADY)
      {
        res = get_devices_i2p(db, mDeviceID, &devicecount, DeviceIDs, i2p_public_key_strings);
        if (res)
        {
          break;
        }

        for (loop = 0; loop < devicecount; loop++)
        {
          i2p_public_key_string_refs[loop] = i2p_public_key_strings;
          _Sam3AConnections[loop] = sam3aStreamConnectEx(&_Sam3ASession, &_Sam3AConnectionCallbacks, i2p_public_key_string_refs[loop], I2P_CONNECTION_TIMEOUT_MS);
          if (!_Sam3AConnections[loop])
          {
            break;
          }
          i2p_public_key_strings += strlen(i2p_public_key_strings) + 1;
        }

        i2p_state = I2P_STATE_SESSIONRUNNING;
      }
      else if (i2p_state == I2P_STATE_SESSIONRUNNING)
      {
        res = NOW(&tp_now);
        if (res)
        {
          break;
        }

        if (tmcompare(&tp_timeout, &tp_now) > 0)
        {
          break;
        }
      }
    }
    else
    {
      if (sam3aIsActiveSession(&_Sam3ASession))
        sam3aProcessSessionIO(&_Sam3ASession, &rds, &wrs);
    }
  }

  sam3aCloseSession(&_Sam3ASession);

  free(i2p_public_key_strings);

  return 0;

ERROR_STATEMENT:
  res = sqlite3_finalize(statement);
ERROR:
  free(i2p_public_key_strings);
  return -1;
}
