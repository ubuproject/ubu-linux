#ifndef _I2PJAVA_H_
#define _I2PJAVA_H_

#include "core.h"

int get_i2p_publickey(sqlite3 *db, sqlite3_int64 mDeviceID, char* pubkey);
int run_i2p(sqlite3 *db, sqlite3_int64 mDeviceID, const char** error);

#endif
