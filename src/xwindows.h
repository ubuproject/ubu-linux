#ifndef _XWINDOWS_H_
#define _XWINDOWS_H_

#include "core.h"

#define TOP_WINDOW_NAME_SIZE    1024

#define XWINDOWS_SENSOR             "X11WNC"
#define XWINDOWS_SENSOR_TYPE        SENSOR_TYPE_WINDOWS
#define XWINDOWS_OBSERVATION_TYPE   OBSERVATION_TYPE_TEXT

int has_xwindows();
int run_xwindows(sqlite3 *db, sqlite3_int64 DeviceID, int firstRun);

#endif
