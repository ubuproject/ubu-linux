#ifndef _BLUETOOTH_H_
#define _BLUETOOTH_H_

#ifdef ANDROID
#include <jni.h>
#else
#include <bluetooth/bluetooth.h>
#endif
#include "core.h"

#ifdef ANDROID
/* BD Address */
typedef struct {
	uint8_t b[6];
} __attribute__((packed)) bdaddr_t;

typedef struct {
    JNIEnv* env;
    jbyteArray Buffer;
    jmethodID InputStream_read_id;
    jobject InputStream;
    jmethodID OutputStream_write_id;
    jobject OutputStream;
} sBluetoothSocket;
typedef const sBluetoothSocket* BluetoothSocket;

int btohl(int in);
int htobl(int in);
short htobs(short in);
int ba2str(const bdaddr_t* bdaddr, char* cbdaddr);
#else
typedef int BluetoothSocket;

int has_bt(const char** error);
int run_bt_client(sqlite3 *db, sqlite3_int64 cDeviceID, const char** error);
int run_bt_server(sqlite3 *db, sqlite3_int64 sDeviceID, const char** error);
#endif

int writebt(BluetoothSocket sock, int cbuffer_out, const char* cbuffer);
int readbt(BluetoothSocket sock, const char* cbuffer, int cbuffer_size, size_t cbuffer_needed, char tolerate_overflow, size_t* cbuffer_syncd);

int handle_bt_server_client(sqlite3 *db, sqlite3_int64 sDeviceID, const bdaddr_t* cbdaddr, BluetoothSocket csock, const char** error);
int handle_bt_client_server_connected(sqlite3 *db, sqlite3_int64 cDeviceID, sqlite3_int64 sDeviceID, const bdaddr_t* sbdaddr_t, unsigned short SynchType, BluetoothSocket ssock, const char** error);

#endif
