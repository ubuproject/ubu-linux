#ifndef _CORE_H_
#define _CORE_H_

#define USE_BASE_85
// #define USE_BASE_64

#include <stdio.h>
#include <time.h>
#ifdef ANDROID
#include "sqlite3.h"
#else
#include <sqlite3.h>
#endif

//Synchronization Types
#define SYNCH_TYPE_DISCONNECTED         0
#define SYNCH_TYPE_BLUETOOTH_L2CAP      1
#define SYNCH_TYPE_BLUETOOTH_RFCOMM     2
#define SYNCH_TYPE_I2P                  10

//Sensor Types
#define SENSOR_TYPE_WINDOWS     1
#define SENSOR_TYPE_TEST_INT32  9000
#define SENSOR_TYPE_TEST_INT64  9001
#define SENSOR_TYPE_TEST_REAL   9002
#define SENSOR_TYPE_TEST_TEXT   9003
#define SENSOR_TYPE_TEST_BLOB   9004

//Observation Value Types
#define OBSERVATION_TYPE_INT32  10
#define OBSERVATION_TYPE_INT64  11
#define OBSERVATION_TYPE_REAL   12
#define OBSERVATION_TYPE_TEXT   13
#define OBSERVATION_TYPE_BLOB   14

//Status Types
#define STATUS_TYPE_PHYSIOLOGICAL   1
#define STATUS_TYPE_EMOTIONAL       2
#define STATUS_TYPE_THOUGHT         4
#define STATUS_TYPE_ACTION          8
#define STATUS_TYPE_SPEECH          16
#define STATUS_TYPE_GEOGRAPHIC      32
#define STATUS_TYPE_SOCIAL          64
#define STATUS_TYPE_INTERACTION     128
#define STATUS_TYPE_ENVIRONMENT     256

#define DEVICE_NAME_SIZE            256

//Algorithm
#define UNIVERSAL_ID_SIZE_BYTES         32
#ifdef USE_BASE_85
/*256 bits can be stored in base 85 in 39.941392646 digits [256÷(log 85 ÷ log 2)]*/
#define UNIVERSAL_ID_BASE_85_SIZE_BYTES 40
#endif
#ifdef USE_BASE_64
/*256 bits can be stored in base 64 in 42.666666667 digits [256÷(log 64 ÷ log 2)]*/
#define UNIVERSAL_ID_BASE_64_SIZE_BYTES 43
#endif

typedef struct
{
    char            ID[UNIVERSAL_ID_SIZE_BYTES];
} universal_id;
#ifdef USE_BASE_85
typedef struct
{
    char            ID_base85[UNIVERSAL_ID_BASE_85_SIZE_BYTES];
} universal_id_base85;
#endif
#ifdef USE_BASE_64
typedef struct
{
    char            ID_base64[UNIVERSAL_ID_BASE_64_SIZE_BYTES];
} universal_id_base64;
#endif

#ifdef USE_BASE_85
void encode_85(char *buf, const unsigned char *data, int bytes);
int decode_85(char *dst, const char *buffer, int len);
#endif
#ifdef USE_BASE_64
void encode_64(char *buf, const unsigned char *data, int bytes);
int decode_64(char *dst, const char *buffer, int len);
#endif

//Time
int NOW(struct timespec *tp);
int NOW_plus_seconds(unsigned int seconds, struct timespec *tp);
int tmcompare(struct timespec *tpleft, struct timespec *tpright);

//DB
int create_db(const char* dbFilename, const char* ForceDevicename, sqlite3 **db, sqlite3_int64* DeviceID);
int connect_to_db(const char* dbFilename, sqlite3_int64 DeviceID, sqlite3 **db);
int close_db(sqlite3* db);
int delete_db(const char* dbFilename, sqlite3 *db);
int insert_config(sqlite3 *db, const char* Name, const char* Value);
int get_config(sqlite3 *db, const char* Name, char* Value, size_t ValueLength, const char* DefaultValue);
int set_config(sqlite3 *db, const char* Name, const char* Value);
int delete_config(sqlite3 *db, const char* Name, const char* Value);
int sqlite_datetime_to_time_t(const unsigned char* Datetime, __time_t* time_t);
int time_t_to_sqlite_datetime(const __time_t* time_t, unsigned char* Datetime);

//Comms
int load_json_connection(const char* DeviceName, const universal_id* puniversal_id, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused);
int handle_json_connection(const char* json, char* DeviceName, universal_id* puniversal_id);
int load_json_devices(sqlite3 *db, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused);
int handle_json_devices(sqlite3 *db, sqlite3_int64 oDeviceID, const char* json);
int load_json_algorithminstance(sqlite3 *db, const universal_id* puniversal_id, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused);
int load_json_sensor_detail_req(sqlite3 *db, sqlite3_int64 oDeviceID, unsigned short* sensorcount, sqlite3_int64* ForeignDeviceIDs, sqlite3_int64* ForeignSensorIDs, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused);
int handle_json_sensor_detail_req(const char* json, unsigned short* sensorcount, sqlite3_int64* DomesticSensorIDs);
int load_sensor_detail_json(sqlite3 *db, unsigned int sensorcount, sqlite3_int64* SensorIDs, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused);
int insert_sensor_by_json(sqlite3 *db, sqlite3_int64 oDeviceID, const char* json, unsigned short* sensorcount, sqlite3_int64* SensorID);
int load_devicereqs_json(unsigned short devicecount, sqlite3_int64* ForeignDeviceID, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused);
int handle_json_devicereq(const char* json, unsigned int* devicecount, sqlite3_int64* DeviceIDs);
int load_devices_json(sqlite3 *db, unsigned short devicecount, sqlite3_int64* DeviceIDs, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused);
int insert_algorithm_by_json(sqlite3 *db, sqlite3_int64 DeviceID, const char* json);
int upsert_foreign_devices_by_json(sqlite3 *db, sqlite3_int64 oDeviceID, const char* json, unsigned short* devicecount, sqlite3_int64* ForeignDeviceIDs, sqlite3_int64* DomesticDeviceIDs);
int load_json_observation_req(sqlite3 *db, unsigned short sensorcount, sqlite3_int64* ForeignSensorIDs, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused);
int insert_unmapped_foreigndevice(sqlite3 *db, sqlite3_int64 oDeviceID, sqlite3_int64 ForeignDeviceID);
int handle_json_observation_req(sqlite3 *db, const char* json, unsigned short* sensorcount, sqlite3_int64* SensorIDs, struct timespec *tp);
int get_devices_i2p(sqlite3 *db, sqlite3_int64 mDeviceID, unsigned short* devicecount, sqlite3_int64* DeviceIDs, char* i2p_public_key_strings);

//Objects
int synch_device_status(sqlite3 *db, sqlite3_int64 mDeviceID, sqlite3_int64 oDeviceID, unsigned short* SynchType, sqlite3_int64* SynchState);
int synch_device_begin(sqlite3 *db, sqlite3_int64 mDeviceID, sqlite3_int64 oDeviceID, unsigned short SynchType, sqlite3_int64 SynchState);
int synch_device_end(sqlite3 *db, sqlite3_int64 mDeviceID, sqlite3_int64 oDeviceID, unsigned short SynchType);
int get_device_by_name(sqlite3 *db, const char* DeviceName, sqlite3_int64* DeviceID);
int get_device_name(sqlite3 *db, sqlite3_int64 DeviceID, char* DeviceName);
int get_device_universal_id(sqlite3 *db, sqlite3_int64 DeviceID, universal_id* puniversal_id);
int insert_foreign_device(sqlite3 *db, sqlite3_int64 oDeviceID, sqlite3_int64 ForeignDeviceID, sqlite3_int64 DomesticDeviceID);
int update_foreign_device(sqlite3 *db, sqlite3_int64 oDeviceID, sqlite3_int64 ForeignDeviceID, sqlite3_int64 DomesticDeviceID);
int get_foreign_device(sqlite3 *db, sqlite3_int64 oDeviceID, sqlite3_int64 ForeignDeviceID, sqlite3_int64* DomesticDeviceID);
int create_db_sensor(sqlite3 *db, sqlite3_int64 DeviceID, const char* SensorName, int SensorType, int ValueType, sqlite3_int64* SensorID);
int get_db_sensor(sqlite3 *db, sqlite3_int64 DeviceID, const char* SensorName, sqlite3_int64* SensorID);
int upsert_foreign_sensor(sqlite3 *db, sqlite3_int64 oDeviceID, unsigned short sensorcount, sqlite3_int64* ForeignDeviceIDs, sqlite3_int64* ForeignSensorIDs, sqlite3_int64* DomesticSensorIDs);
int insert_unmapped_foreign_sensor(sqlite3 *db, sqlite3_int64 oDeviceID, sqlite3_int64 ForeignDeviceID, sqlite3_int64 ForeignSensorID);
int prepare_log_observationsint32(sqlite3 *db, sqlite3_int64 SensorID, sqlite3_stmt** statement);
int close_log_observationsint32(sqlite3_stmt* statement);
int prepare_log_observationsint64(sqlite3 *db, sqlite3_int64 SensorID, sqlite3_stmt** statement);
int close_log_observationsint64(sqlite3_stmt* statement);
int prepare_log_observationsreal(sqlite3 *db, sqlite3_int64 SensorID, sqlite3_stmt** statement);
int close_log_observationsreal(sqlite3_stmt* statement);
int prepare_log_observationstext(sqlite3 *db, sqlite3_int64 SensorID, sqlite3_stmt** statement);
int close_log_observationstext(sqlite3_stmt* statement);
int prepare_log_observationsblob(sqlite3 *db, sqlite3_int64 SensorID, sqlite3_stmt** statement);
int close_log_observationsblob(sqlite3_stmt* statement);
int authorize_device(sqlite3 *db, const universal_id* puniversal_id, const char* DeviceName, unsigned short DeviceType, unsigned short StorageStyle, const char* BtAddress, unsigned char RFCOMMChannel, unsigned short PSM, const char* I2PPublicKey, sqlite3_int64* DeviceID);
int verify_bt_device(sqlite3 *db, const char* DeviceName, const char* BtAddress, sqlite3_int64* DeviceID);
int log_observationsint32(sqlite3_stmt* statement, int value);
int log_observationsint64(sqlite3_stmt* statement, sqlite3_int64 value);
int log_observationsreal(sqlite3_stmt* statement, double value);
int log_observationstext(sqlite3_stmt* statement, const char* value);
int log_observationsblob(sqlite3_stmt* statement, const char* value, unsigned int length);
int get_last_logged_observationsint32(sqlite3 *db, sqlite3_int64 SensorID, struct timespec* tp);
int get_last_logged_observationsint64(sqlite3 *db, sqlite3_int64 SensorID, struct timespec* tp);
int get_last_logged_observationsreal(sqlite3 *db, sqlite3_int64 SensorID, struct timespec* tp);
int get_last_logged_observationstext(sqlite3 *db, sqlite3_int64 SensorID, struct timespec* tp);
int get_last_logged_observationsblob(sqlite3 *db, sqlite3_int64 SensorID, struct timespec* tp);

//External undefined
int check_file_exists_OS(const char* filename);
int get_device_name_OS(char* DeviceName);
int create_random_universal_id_OS(universal_id* puniversal_id);

//External DB
const char* DB_FILENAME_DEFAULT;

#ifdef WITH_BLUETOOTH
//External Bluetooth
int get_bt_address(char* cbdaddr);
#endif

#ifdef WITH_I2P
//External I2P
int create_i2p_keys(sqlite3 *db, sqlite3_int64 mDeviceID);
#endif

#endif
