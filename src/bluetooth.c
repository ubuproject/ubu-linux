#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "core.h"
#include "db.h"
#include "bluetooth.h"

#ifndef ANDROID
#include <signal.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/sdp.h>
#include <bluetooth/rfcomm.h>
#include <unistd.h>
#endif

//#define USE_L2CAP
#define USE_SDP_ADVERTISING

#ifndef ANDROID
#ifdef USE_L2CAP
#include <bluetooth/l2cap.h>
#endif
#ifdef USE_SDP_ADVERTISING
#include <bluetooth/sdp_lib.h>
#endif
#endif

#define CONFIG_BT_CLIENT_POLL_SECONDS           "BT_CLIENT_POLL_SECONDS"
#define CONFIG_BT_CLIENT_POLL_SECONDS_DEFAULT   "30"
#define CONFIG_BT_PEER_POLL_SECONDS             "BT_PEER_POLL_SECONDS"
#define CONFIG_BT_PEER_POLL_SECONDS_DEFAULT     "5"

/*1 MB = 1048576 B */
#define BLUETOOTH_BUFFER_SIZE               1048576
#define DEVICE_NAME_SIZE_MAX                1023
#define MAX_DEVICE_REQUEST                  1024
#define MAX_SENSOR_REQUEST                  1024

#ifdef USE_SDP_ADVERTISING
#define BT_SERVICE_NAME     "UbU"
#define BT_SERVICE_DSC      "The UbU system's device handler"
#define BT_SERVICE_PROV     "Strategoi Project"
uint8_t BT_SERVICE_UUID[]   = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xab, 0xcd };
#endif

//Comm requests
#define CS_SENSORS
#define CS_ALGORITHMS
#define CS_OBSERVATIONS

//Comm structures
typedef struct
{
    uint8_t rc_channel;
#ifdef USE_L2CAP
    uint16_t psm;
    uint8_t psm_mSequence;
#endif
} bt_connection_state;
typedef struct
{
    uint32_t            size_b;
    char                json[];
} json_message;
typedef struct
{
    long int        DateLoggedS;
    long int        DateLoggedN;
    int             Value;
} observationsint32_value_v1;
typedef struct
{
    sqlite3_int64                       SensorID;
    unsigned char                       HasMoreObservationValues;
    unsigned short                      observationcount;
    observationsint32_value_v1          values[];
} observationsint32_v1;
typedef struct
{
    long int        DateLoggedS;
    long int        DateLoggedN;
    long int        Value;
} observationsint64_value_v1;
typedef struct
{
    sqlite3_int64                       SensorID;
    unsigned char                       HasMoreObservationValues;
    unsigned short                      observationcount;
    observationsint64_value_v1          values[];
} observationsint64_v1;
typedef struct
{
    long int        DateLoggedS;
    long int        DateLoggedN;
    double          Value;
} observationsreal_value_v1;
typedef struct
{
    sqlite3_int64                       SensorID;
    unsigned char                       HasMoreObservationValues;
    unsigned short                      observationcount;
    observationsreal_value_v1           values[];
} observationsreal_v1;
typedef struct
{
    long int        DateLoggedS;
    long int        DateLoggedN;
    unsigned short  lValue;
} observationstext_value_v1;
typedef struct
{
    sqlite3_int64                       SensorID;
    unsigned char                       HasMoreObservationValues;
    unsigned short                      observationcount;
    observationstext_value_v1           values[];
} observationstext_v1;
typedef struct
{
    long int        DateLoggedS;
    long int        DateLoggedN;
    unsigned short  lValue;
} observationsblob_value_v1;
typedef struct
{
    sqlite3_int64                       SensorID;
    unsigned char                       HasMoreObservationValues;
    unsigned short                      observationcount;
    observationsblob_value_v1           values[];
} observationsblob_v1;
typedef struct
{
    universal_id    AlgorithmInstanceID;
} algorithm_detail_req_v1;
typedef struct
{
    unsigned short  algorithmcount;
    universal_id    AlgorithmInstanceIDs[];
} algorithms_v1;

//bt functions
#ifdef USE_L2CAP
int set_l2cap_mtu(BluetoothSocket sock, uint16_t mtu_requested, uint16_t* mtu_actual);
#endif

//communications functions
int run_bt_peer(sqlite3 *db, sqlite3_int64 mDeviceID, sqlite3_int64 oDeviceID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size);
#ifndef ANDROID
int prepare_bt_client_server(sqlite3 *db, sqlite3_int64 cDeviceID, const bdaddr_t* cbdaddr_t, sqlite3_int64 sDeviceID, const bdaddr_t* sbdaddr_t, unsigned char RFCOMMChannel, unsigned short PSM, const char** error);
int handle_bt_client_server(sqlite3 *db, sqlite3_int64 cDeviceID, sqlite3_int64 sDeviceID, const bdaddr_t* sbdaddr_t, bt_connection_state* pbt_connection_state, const char** error);
#endif

int receive_connection(sqlite3 *db, const bdaddr_t* obdaddr_t, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, sqlite3_int64* cDeviceID);
int send_connection(sqlite3 *db, sqlite3_int64 mDeviceID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size);
int send_devices(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size);
int receive_devices(sqlite3 *db, sqlite3_int64 oDeviceID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size);
int send_device_details(sqlite3 *db, sqlite3_int64 oDeviceID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size);
int receive_device_details(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size);
int send_device_detail(sqlite3 *db, unsigned int devicecount, sqlite3_int64* DeviceIDs, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size);
int receive_device_detail(sqlite3 *db, sqlite3_int64 oDeviceID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, unsigned short* devicecount, sqlite3_int64* ForeignDeviceIDs, sqlite3_int64* DomesticDeviceIDs);
int send_device_detail_req(BluetoothSocket sock, unsigned int devicecount, sqlite3_int64* ForeignDeviceID, char* cbuffer, size_t cbuffer_size);
int receive_device_detail_req(BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, unsigned int* devicecount, sqlite3_int64* DeviceIDs);
int send_sensor(BluetoothSocket sock, char* cbuffer, unsigned long cbuffer_out);
int send_sensor_details(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 oDeviceID, char* cbuffer, size_t cbuffer_size);
int receive_sensor_details(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size);
int receive_sensor_detail_req(BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, unsigned short* sensorcount, sqlite3_int64* DomesticSensorIDs);
int send_sensor_detail(sqlite3 *db, BluetoothSocket sock, unsigned int sensorcount, sqlite3_int64* DomesticSensorIDs, char* cbuffer, size_t cbuffer_size);
int receive_sensor_detail(sqlite3 *db, sqlite3_int64 oDeviceID, BluetoothSocket sock, unsigned short sensorcount, sqlite3_int64* ForeignDeviceIDs, sqlite3_int64* ForeignSensorIDs, char* cbuffer, size_t cbuffer_size, sqlite3_int64* DomesticSensorIDs);
int send_observations(sqlite3 *db, sqlite3_int64 mDeviceID, sqlite3_int64 oDeviceID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size);
int end_observations(sqlite3 *db, BluetoothSocket sock, struct timespec *tp, char* cbuffer, size_t cbuffer_size);
int receive_observations(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size);
int send_observation_req(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 ForeignSensorID, const struct timespec *tp, char* cbuffer, size_t cbuffer_size);
int receive_observation_req(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, sqlite3_int64* DomesticSensorID, struct timespec *tp);
int send_observation(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 DomesticSensorID, const struct timespec *tp, char* cbuffer, size_t cbuffer_size);
int send_observationsint32(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 DomesticSensorID, const struct timespec *tp, char* cbuffer, size_t cbuffer_size);
int receive_observationsint32(sqlite3 *db, sqlite3_int64 DomesticSensorID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, sqlite3_int64* ForeignSensorID, unsigned char* HasMoreObservationValues, struct timespec *tp);
int send_observationsint64(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 DomesticSensorID, const struct timespec *tp, char* cbuffer, size_t cbuffer_size);
int receive_observationsint64(sqlite3 *db, sqlite3_int64 DomesticSensorID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, sqlite3_int64* ForeignSensorID, unsigned char* HasMoreObservationValues, struct timespec *tp);
int send_observationsreal(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 DomesticSensorID, const struct timespec *tp, char* cbuffer, size_t cbuffer_size);
int receive_observationsreal(sqlite3 *db, sqlite3_int64 DomesticSensorID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, sqlite3_int64* ForeignSensorID, unsigned char* HasMoreObservationValues, struct timespec *tp);
int send_observationstext(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 DomesticSensorID, const struct timespec *tp, char* cbuffer, size_t cbuffer_size);
int receive_observationstext(sqlite3 *db, sqlite3_int64 DomesticSensorID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, sqlite3_int64* ForeignSensorID, unsigned char* HasMoreObservationValues, struct timespec *tp);
int send_observationsblob(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 DomesticSensorID, const struct timespec *tp, char* cbuffer, size_t cbuffer_size);
int receive_observationsblob(sqlite3 *db, sqlite3_int64 DomesticSensorID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, sqlite3_int64* ForeignSensorID, unsigned char* HasMoreObservationValues, struct timespec *tp);

//algorithms
int send_algorithm_instances(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size);
int receive_algorithm_instances(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size);
int send_algorithm_instance(BluetoothSocket sock, char* cbuffer, unsigned long cbuffer_out);
int send_algorithm_instance_details(sqlite3 *db, sqlite3_int64 oDeviceID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size);
int end_algorithm_instance_details(BluetoothSocket sock, char* cbuffer, size_t cbuffer_size);
int receive_algorithm_instance_details(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size);
int send_algorithm_instance_detail_req(BluetoothSocket sock, const char* AlgorithmInstanceID, char* cbuffer, size_t cbuffer_size);
int receive_algorithm_instance_detail_req(BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, universal_id* puniversal_id);
int send_algorithm_instance_detail(sqlite3 *db, BluetoothSocket sock, const universal_id* puniversal_id, char* cbuffer, size_t cbuffer_size);
int receive_algorithm_instance_detail(sqlite3 *db, sqlite3_int64 oDeviceID, BluetoothSocket sock, const char* AlgorithmInstanceID, char* cbuffer, size_t cbuffer_size);

//db helper functions
int insert_unmapped_algorithm(sqlite3 *db, universal_id* puniversal_id);

//TODO: #31:
// int set_bt_authenticate(BluetoothSocket sock)
// {
// 	struct l2cap_options opts;
//     socklen_t optlen = sizeof(opts);
//     int err;
//     err = getsockopt(sock, SOL_L2CAP, SO_BTH_AUTHENTICATE, &opts, &optlen);
//     if (!err)
//     {
//         opts.omtu = opts.imtu = mtu;
//         err = setsockopt(sock, SOL_L2CAP, SO_BTH_AUTHENTICATE, &opts, optlen);
//     }
//     return err;
// }
#ifdef USE_L2CAP
int set_l2cap_mtu(BluetoothSocket sock, uint16_t mtu_requested, uint16_t* mtu_actual)
{
	struct l2cap_options opts;
    uint16_t mtu_set;
    socklen_t optlen;
    int res;

    optlen = sizeof(opts);
    res = getsockopt(sock, SOL_L2CAP, L2CAP_OPTIONS, &opts, &optlen);
    if (res)
    {
        return res;
    }

    mtu_set = opts.omtu;
    opts.omtu = opts.imtu = mtu_requested;
    res = setsockopt(sock, SOL_L2CAP, L2CAP_OPTIONS, &opts, optlen);
    if (res)
    {
        *mtu_actual = mtu_set;
    }
    else
    {
        *mtu_actual = mtu_requested;
    }
    return 0;
}
#endif

#ifndef ANDROID
int get_bt_address(char* cbdaddr)
{
    int dev_id;
    int res;
    bdaddr_t bdaddr;

    //TODO: #27: Support multiple bt devices
    dev_id = hci_get_route(NULL);
    if (dev_id < 0)
    {
        return -1;
    }

    res = hci_devba(dev_id, &bdaddr);
    if (res)
    {
        return -1;
    }

    res = ba2str(&bdaddr, cbdaddr);
    if (res < 0)
    {
        return -1;
    }

    return 0;
}

int has_bt(const char** error)
{//Check if there's at least one enabled device
    //TODO: #27: Support Bluetooth devices that exist but are disabled
    int dev_id;

    dev_id = hci_get_route(NULL);
    if (dev_id < 0)
    {//There's no enabled device
        *error = "No enabled Bluetooth device";
        return -1;
    }

    *error = NULL;
    return 0;
}
#endif

static void signalHandler(int signum)
{
    printf("Receive signal: %d\n", signum);
    fflush(stdout);
}

#ifndef ANDROID
int run_bt_client(sqlite3 *db, sqlite3_int64 cDeviceID, const char** error)
{
    char cbuffer[64];
    int res;
    int dev_id;
    bdaddr_t cbdaddr_t;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    int loopDevice;
    sqlite3_int64 sDeviceID;
    unsigned short SynchType;
    sqlite3_int64 SynchState;
    const unsigned char* BtAddress;
    int RFCOMMChannel;
    int PSM;
    bdaddr_t sbdaddr_t;
    int bt_client_poll_seconds;
    __pid_t child;
    const size_t cbuffer_size = sizeof(cbuffer);

    // setup signal handlers
    signal(SIGINT, signalHandler);
    signal(SIGKILL, signalHandler);
    signal(SIGHUP, signalHandler);
    signal(SIGUSR1, signalHandler);
    signal(SIGUSR2, signalHandler);

    while (1)
    {
        //TODO: #27: Support Bluetooth devices that exist but are disabled
        dev_id = hci_get_route(NULL);
        if (dev_id < 0)
        {//There's no enabled device
            *error = "No enabled Bluetooth device";
            goto ERROR;
        }

        res = hci_devba(dev_id, &cbdaddr_t);
        if (res)
        {
            goto ERROR;
        }

        res = sqlite3_prepare_v3(db, SQL_QUERY_DEVICES_FOR_BTADDRESS, -1, 0, &statement, &sqlBufferUnused);
        if (res)
        {
            goto ERROR;
        }

        //DeviceID
        res = sqlite3_bind_int64(statement, 1, cDeviceID);
        if (res)
        {
            goto ERROR_STATEMENT;
        }

        for (loopDevice = 0; ; loopDevice++)
        {
            res = sqlite3_step(statement);
            if (res == SQLITE_DONE)
            {
                break;
            }
            if (res != SQLITE_ROW)
            {
                goto ERROR_STATEMENT;
            }

            sDeviceID = sqlite3_column_int64(statement, 0);
            BtAddress = sqlite3_column_text(statement, 1);
            if (sqlite3_column_type(statement, 2) == SQLITE_NULL)
            {
                RFCOMMChannel = 0;
            }
            else
            {
                RFCOMMChannel = sqlite3_column_int(statement, 2);
            }
            if (sqlite3_column_type(statement, 3) == SQLITE_NULL)
            {
                PSM = 0;
            }
            else
            {
                PSM = sqlite3_column_int(statement, 3);
            }

            //Check to make sure not already connected
            res = synch_device_status(db, cDeviceID, sDeviceID, &SynchType, &SynchState);
            if (res)
            {
                goto ERROR_STATEMENT;
            }

            res = str2ba((const char*)BtAddress, &sbdaddr_t);
            if (res)
            {
                goto ERROR_STATEMENT;
            }

#ifdef RELEASE
            //Fork bt process
            if ((child=fork()) < 0)
            {//failed fork
                *error = "fork for run_bt() failed";
                return -1;
            }
            if (child)
            {//parent - continue to wait for new connections
                //TODO: log somewhere
            }
            else
#endif
            {//child
                res = prepare_bt_client_server(db, cDeviceID, &cbdaddr_t, sDeviceID, &sbdaddr_t, RFCOMMChannel, PSM, error);

#ifdef RELEASE
                exit(res);
#endif
            }
        }

        //Finish up
        res = sqlite3_finalize(statement);
        if (res)
        {
            goto ERROR;
        }

        //query CONFIG_BT_CLIENT_POLL_SECONDS every time
        memset(cbuffer, 0, cbuffer_size);
        res = get_config(db, CONFIG_BT_CLIENT_POLL_SECONDS, cbuffer, cbuffer_size, CONFIG_BT_CLIENT_POLL_SECONDS_DEFAULT);
        if (res)
        {
            //TODO: log error
            goto ERROR;
        }

        bt_client_poll_seconds = atoi(cbuffer);

#ifdef RELEASE
        //sleep
        sleep(bt_client_poll_seconds);
#else
        break;
#endif
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int prepare_bt_client_server(sqlite3 *db, sqlite3_int64 cDeviceID, const bdaddr_t* cbdaddr_t, sqlite3_int64 sDeviceID, const bdaddr_t* sbdaddr_t, unsigned char RFCOMMChannel, unsigned short PSM, const char** error)
{
    int res;
#ifdef USE_SDP_ADVERTISING
    uuid_t svc_uuid;
    sdp_list_t *response_list, *search_list, *attrid_list;
    sdp_session_t *session;
    uint32_t range;
#endif
    bt_connection_state mbt_connection_state;

#ifdef USE_SDP_ADVERTISING
    // connect to the SDP server running on the remote machine
    session = sdp_connect(BDADDR_ANY, sbdaddr_t, SDP_RETRY_IF_BUSY | SDP_WAIT_ON_CLOSE);
    if (!session)
    {
char* text = strerror(errno);
        *error = "failed sdp_connect()";
        return -1;
    }

    // specify the UUID of the application we're searching for
    sdp_uuid128_create(&svc_uuid, &BT_SERVICE_UUID);
    search_list = sdp_list_append(NULL, &svc_uuid);

    // specify that we want a list of all the matching applications' attributes
    range = 0x0000ffff;
    attrid_list = sdp_list_append(NULL, &range);

    // get a list of service records that have UUID
    response_list = NULL;
    res = sdp_service_search_attr_req(session, search_list, SDP_ATTR_REQ_RANGE, attrid_list, &response_list);
    if (res)
    {
        *error = "failed sdp_service_search_attr_req()";
        return -1;
    }

    mbt_connection_state.rc_channel = 0;
#ifdef USE_L2CAP
    mbt_connection_state.psm = 0;
    mbt_connection_state.psm_mSequence = 0;
#endif
    sdp_list_t *r = response_list;

    // go through each of the service records
    for (; r; r = r->next)
    {
        sdp_record_t *rec = (sdp_record_t*)r->data;
        sdp_list_t *proto_list;

        // get a list of the protocol sequences
        if (sdp_get_access_protos(rec, &proto_list) == 0)
        {
            sdp_list_t *p = proto_list;

            // go through each protocol sequence
            for (; p; p = p->next)
            {
                sdp_list_t *pds = (sdp_list_t*)p->data;

                // go through each protocol list of the protocol sequence
                for (; pds; pds = pds->next)
                {
                    // check the protocol attributes
                    sdp_data_t *d = (sdp_data_t*)pds->data;
                    int proto = 0;
                    int i = 0;

                    for (; d; d = d->next, i++)
                    {
                        switch (d->dtd)
                        {
                            case SDP_UUID16:
                            case SDP_UUID32:
                            case SDP_UUID128:
                                proto = sdp_uuid_to_proto(&d->val.uuid);
                                break;
                            case SDP_UINT8:
                                if(proto == RFCOMM_UUID)
                                {
                                    mbt_connection_state.rc_channel = d->val.int8;
                                }
                                break;
#ifdef USE_L2CAP
                            case SDP_UINT16:
                                if (proto == L2CAP_UUID)
                                {
                                    if (i == 1)
                                    {
                                        mbt_connection_state.psm = btohs(d->val.uint16);
                                    }
                                }
                                break;
#endif
                            default:
                                break;
                        }
                    }
                }

                sdp_list_free( (sdp_list_t*)p->data, 0 );
            }

            sdp_list_free( proto_list, 0 );
        }

        sdp_record_free( rec );
    }

    res = sdp_close(session);
    if (res < 0)
    {
        return -1;
    }
#else
#ifdef USE_L2CAP
    CARP = PSM;
#endif

    if (!RFCOMMChannel)
    {
        RFCOMMChannel = 1;
    }

    mbt_connection_state.rc_channel = RFCOMMChannel;
#endif

#ifdef USE_L2CAP
    if ((!mbt_connection_state.psm) && (!mbt_connection_state.rc_channel))
#else
    if (!mbt_connection_state.rc_channel)
#endif
    {
        *error = "failed prepare_bt_client_server()";
        return -1;
    }

    res = handle_bt_client_server(db, cDeviceID, sDeviceID, sbdaddr_t, &mbt_connection_state, error);
    if (res < 0)
    {
        if ((!error) || (!*error))
        {
            *error = "failed handle_bt_client_server()";
        }

        return -1;
    }

    return 0;
}

int run_bt_server(sqlite3 *db, sqlite3_int64 sDeviceID, const char** error)
{
    int res;
    static int reuse_addr = 1;
    struct bt_security bt_sec;
#ifdef USE_L2CAP
    struct sockaddr_l2 saddr_l2;
    socklen_t saddr_l2len;
    BluetoothSocket ssock_l2;
#endif
    bt_connection_state mbt_connection_state;
    struct sockaddr_rc saddr_rc;
    socklen_t saddr_rclen;
    BluetoothSocket ssock_rc;
#ifdef USE_L2CAP
    BluetoothSocket csock_l2;
    struct sockaddr_l2 caddr_l2;
    socklen_t caddr_l2len;
#endif
    BluetoothSocket csock_rc;
    struct sockaddr_rc caddr_rc;
    socklen_t caddr_rclen;
    int child;
    uuid_t root_uuid, l2cap_uuid, rfcomm_uuid, svc_uuid;
    struct timeval timeout;
    fd_set fdset;
#ifdef USE_SDP_ADVERTISING
    sdp_session_t *session;
    sdp_list_t *l2cap_list,
               *rfcomm_list,
               *root_list,
               *proto_list,
               *access_proto_list;
#ifdef USE_L2CAP
    sdp_data_t *psm;
#endif
    sdp_data_t *channel;
#endif

#ifdef USE_L2CAP
    //L2CAP
    ssock_l2 = socket(AF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);
    if (ssock_l2 < 0)
    {
        *error = "Opening L2CAP socket failed";
        return -1;
    }

    res = setsockopt(ssock_l2, SOL_SOCKET, SO_REUSEADDR, &reuse_addr, sizeof(reuse_addr));
    if (res)
    {
char* text = strerror(errno);
        *error = "Setting L2CAP reusable bind failed";
        return -1;
    }

    memset(&saddr_l2, 0, sizeof(saddr_l2));
    saddr_l2.l2_bdaddr = *BDADDR_ANY;
    saddr_l2.l2_family = AF_BLUETOOTH;
    //TODO: #31: support both LE/smart and classic
    // saddr_l2.l2_bdaddr_type = BDADDR_LE_PUBLIC;
    saddr_l2.l2_cid = htobs(0);
    saddr_l2.l2_psm = htobs(0);

    res = bind(ssock_l2, (struct sockaddr*)&saddr_l2, sizeof(saddr_l2));
    if (res < 0)
    {
char* text = strerror(errno);
        *error = "Binding L2CAP socket failed";
        return -1;
    }

    memset(&bt_sec, 0, sizeof(bt_sec));
    bt_sec.level = BT_SECURITY_LOW;//BT_SECURITY_MEDIUM;
    res = setsockopt(ssock_l2, SOL_BLUETOOTH, BT_SECURITY, &bt_sec, sizeof(bt_sec));
    if (res)
    {
        *error = "Setting L2CAP security level failed";
        return -1;
    }

    res = listen(ssock_l2, SOMAXCONN);
    if (res < 0)
    {
        *error = "Listening on L2CAP socket failed";
        return -1;
    }

    /* Check for socket address */
    memset(&saddr_l2, 0, sizeof(saddr_l2));
    saddr_l2len = sizeof(saddr_l2);

    res = getsockname(ssock_l2, (struct sockaddr *)&saddr_l2, &saddr_l2len);
    if (res < 0)
    {
char* text = strerror(errno);
        *error = "Binding RFCOMM socket failed";
        return -1;
    }

    memset(&mbt_connection_state, 0, sizeof(mbt_connection_state));

    if (!saddr_l2.l2_psm)
    {
        *error = "bind() failed to assign dynamic l2_psm";
        return -1;
    }
    mbt_connection_state.psm = saddr_l2.l2_psm;
#endif

    //RFCOMM
    ssock_rc = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
    if (ssock_rc < 0)
    {
        *error = "Opening RFCOMM socket failed";
        return -1;
    }

    res = setsockopt(ssock_rc, SOL_SOCKET, SO_REUSEADDR, &reuse_addr, sizeof(reuse_addr));
    if (res)
    {
char* text = strerror(errno);
        *error = "Setting RFCOMM reusable bind failed";
        return -1;
    }

    memset(&saddr_rc, 0, sizeof(saddr_rc));
    saddr_rc.rc_family = AF_BLUETOOTH;
    saddr_rc.rc_bdaddr = *BDADDR_ANY;
    saddr_rc.rc_channel = 0;

    res = bind(ssock_rc, (struct sockaddr*)&saddr_rc, sizeof(saddr_rc));
    if (res < 0)
    {
char* text = strerror(errno);
        *error = "Binding RFCOMM socket failed";
        return -1;
    }

    memset(&bt_sec, 0, sizeof(bt_sec));
    bt_sec.level = BT_SECURITY_LOW;//BT_SECURITY_MEDIUM;
    res = setsockopt(ssock_rc, SOL_BLUETOOTH, BT_SECURITY, &bt_sec, sizeof(bt_sec));
    if (res)
    {
        *error = "Setting RFCOMM security level failed";
        return -1;
    }

    res = listen(ssock_rc, SOMAXCONN);
    if (res < 0)
    {
        *error = "Listening on RFCOMM socket failed";
        return -1;
    }

    /* Check for socket address */
    memset(&saddr_rc, 0, sizeof(saddr_rc));
saddr_rclen = sizeof(saddr_rc);

    res = getsockname(ssock_rc, (struct sockaddr *)&saddr_rc, &saddr_rclen);
    if (res < 0)
    {
char* text = strerror(errno);
        *error = "Binding RFCOMM socket failed";
        return -1;
    }

    if (!saddr_rc.rc_channel)
    {
        *error = "bind() failed to assign dynamic rc_channel";
        return -1;
    }
    mbt_connection_state.rc_channel = saddr_rc.rc_channel;

    //Advertise service
#ifdef USE_SDP_ADVERTISING
#ifdef USE_L2CAP
    psm = NULL;
#endif
    channel = NULL;
    l2cap_list = NULL;
    root_list = NULL;
    proto_list = NULL;
    access_proto_list = NULL;

    sdp_record_t *record = sdp_record_alloc();
    if (!record)
    {
        *error = "sdp_record_alloc";
        return -1;
    }

    // set the general service ID
    sdp_uuid128_create(&svc_uuid, &BT_SERVICE_UUID);
    sdp_set_service_id(record, svc_uuid);

    // make the service record publicly browsable
    sdp_uuid16_create(&root_uuid, PUBLIC_BROWSE_GROUP);
    root_list = sdp_list_append(0, &root_uuid);
    res = sdp_set_browse_groups(record, root_list);
    if (res)
    {
        *error = "sdp_set_browse_groups";
        return -1;
    }

    // set l2cap information
    sdp_uuid16_create(&l2cap_uuid, L2CAP_UUID);
#ifdef USE_L2CAP
    psm = sdp_data_alloc(SDP_UINT16, &mbt_connection_state.psm);
#endif
    l2cap_list = sdp_list_append(0, &l2cap_uuid);
#ifdef USE_L2CAP
    l2cap_list = sdp_list_append(l2cap_list, psm);
#endif
    proto_list = sdp_list_append(0, l2cap_list);

    // set rfcomm information
    sdp_uuid16_create(&rfcomm_uuid, RFCOMM_UUID);
    channel = sdp_data_alloc(SDP_UINT8, &mbt_connection_state.rc_channel);
    rfcomm_list = sdp_list_append(0, &rfcomm_uuid);
    sdp_list_append(rfcomm_list, channel);
    sdp_list_append(proto_list, rfcomm_list);

    // attach protocol information to service record
    access_proto_list = sdp_list_append(0, proto_list);
    res = sdp_set_access_protos(record, access_proto_list);
    if (res)
    {
        *error = "sdp_set_access_protos";
        return -1;
    }

    // set the name, provider, and description
    sdp_set_info_attr(record, BT_SERVICE_NAME, BT_SERVICE_PROV, BT_SERVICE_DSC);

    session = 0;

    // connect to the local SDP server, register the service record, and disconnect
    session = sdp_connect(BDADDR_ANY, BDADDR_LOCAL, SDP_RETRY_IF_BUSY);
    if (!session)
    {
char* text = strerror(errno);
        *error = "Cannot connect to SDP";
        return -1;
    }

    res = sdp_record_register(session, record, 0);
    if (res)
    {
        *error = "Cannot register to SDP";
        return -1;
    }

    // cleanup
    sdp_list_free(l2cap_list, 0);
    sdp_list_free(root_list, 0);
    sdp_list_free(access_proto_list, 0);
    sdp_list_free(proto_list, 0);
#endif

    // setup signal handlers
    signal(SIGINT, signalHandler);
    signal(SIGKILL, signalHandler);
    signal(SIGHUP, signalHandler);
    signal(SIGUSR1, signalHandler);
    signal(SIGUSR2, signalHandler);

    //accept new connections
#ifdef USE_L2CAP
    caddr_l2len = sizeof(caddr_l2);
#endif
    caddr_rclen = sizeof(caddr_rc);

    timeout.tv_sec = 10;
    timeout.tv_usec = 0;//TODO: #31: adjust to reasonable timeout

    while (1)
    {
        FD_ZERO(&fdset);
#ifdef USE_L2CAP
        FD_SET(ssock_l2, &fdset);
#endif
        FD_SET(ssock_rc, &fdset);

#ifdef USE_L2CAP
        res = select(1 + ((ssock_l2 > ssock_rc) ? ssock_l2 : ssock_rc), &fdset, NULL, NULL, &timeout);
#else
        res = select(1 + ssock_rc, &fdset, NULL, NULL, &timeout);
#endif

        if(res < 0)
        {
            break;
        }
        else if (res == 0)
        {//Timeout
        }
#ifdef USE_L2CAP
        else if (res == 1)
        {
            csock_l2 = accept(ssock_l2, (struct sockaddr*)&caddr_l2, &caddr_l2len);
            if (csock_l2 >= 0)
            {//Client ready
#ifdef RELEASE
                //Fork bt process
                if ((child=fork()) < 0)
                {//failed fork
                    *error = "fork for setsockopt() failed";
                    return -1;
                }
                if (child)
                {//parent - continue to wait for new connections
                    //TODO: log somewhere
                }
                else
#endif
                {//child
                    res = handle_bt_server_client(db, sDeviceID, &caddr_l2.l2_bdaddr, csock_l2, error);

#ifdef RELEASE
                    exit(res);
#else
                    break;
#endif
                }
            }
        }
        else if (res == 2)
#else
        else if (res == 1)
#endif
        {
            csock_rc = accept(ssock_rc, (struct sockaddr*)&caddr_rc, &caddr_rclen);
            if (csock_rc >= 0)
            {//Client ready
#ifdef RELEASE
                //Fork bt process
                if ((child=fork()) < 0)
                {//failed fork
                    *error = "fork for setsockopt() failed";
                    return -1;
                }
                if (child)
                {//parent - continue to wait for new connections
                    //TODO: log somewhere
                }
                else
#endif
                {//child
                    res = handle_bt_server_client(db, sDeviceID, &caddr_rc.rc_bdaddr, csock_rc, error);

#ifdef RELEASE
                    exit(res);
#else
                    break;
#endif
                }
            }
        }
        else
        {
            break;
        }
    }

    //Close
#ifdef USE_L2CAP
    res = close(ssock_l2);
    if (res < 0)
    {
        *error = "Unable to close L2CAP socket after success";
        return -1;
    }
#endif
    res = close(ssock_rc);
    if (res < 0)
    {
        *error = "Unable to close RFCOMM socket after success";
        return -1;
    }

#ifdef USE_SDP_ADVERTISING
    res = sdp_close(session);
    if (res < 0)
    {
        *error = "Unable to close SDP session after success";
        return -1;
    }
#endif

    *error = NULL;
    return 0;
}
#endif

int handle_bt_server_client(sqlite3 *db, sqlite3_int64 sDeviceID, const bdaddr_t* cbdaddr, BluetoothSocket csock, const char** error)
{
    char cbuffer[BLUETOOTH_BUFFER_SIZE];
    // uint16_t mtu;
    sqlite3_int64 cDeviceID;
    int res;
    unsigned short SynchType;
    const size_t cbuffer_size = sizeof(cbuffer);

#ifdef USE_L2CAP
    TODO: #31:
    res = set_l2cap_mtu(csock, (uint16_t)BLUETOOTH_MTU_SIZE_DESIRED, &mtu);
    if (res)
    {
        //TODO: log info: reject

        res = close(csock);
        return -1;
    }
#endif

    memset(cbuffer, 0, cbuffer_size);

    //receive connection
    res = receive_connection(db, cbdaddr, csock, cbuffer, cbuffer_size, &cDeviceID);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SOCKET;
    }

    //TODO: #31:
    // if ()
    // {
    //     SynchType = SYNCH_TYPE_BLUETOOTH_L2CAP;
    // }
    // else
    // {
        SynchType = SYNCH_TYPE_BLUETOOTH_RFCOMM;
    // }
    res = synch_device_begin(db, cDeviceID, sDeviceID, SynchType, 0);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SOCKET;
    }

    //send connection
    res = send_connection(db, sDeviceID, csock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //receive devices
    res = receive_devices(db, cDeviceID, csock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //send devices
    res = send_devices(db, csock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //receive device_details
    res = receive_device_details(db, csock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //send device_details
    res = send_device_details(db, cDeviceID, csock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

#ifdef CS_SENSORS
    //receive sensor_details
    res = receive_sensor_details(db, csock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //send sensor_details
    res = send_sensor_details(db, csock, cDeviceID, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }
#endif

#ifdef CS_ALGORITHMS
    //receive algorithms
    res = receive_algorithm_instances(db, csock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //send algorithms
    res = send_algorithm_instances(db, csock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //receive algorithm_details
    res = receive_algorithm_instance_details(db, csock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //send algorithm_details
    res = send_algorithm_instance_details(db, cDeviceID, csock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }
#endif

#ifdef CS_OBSERVATIONS
    //receive observations
    res = receive_observations(db, csock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //send observations
    res = send_observations(db, sDeviceID, cDeviceID, csock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }
#endif

    //Go to peer-to-peer
    res = run_bt_peer(db, sDeviceID, cDeviceID, csock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //Success
    res = synch_device_end(db, cDeviceID, sDeviceID, SynchType);

#ifndef ANDROID
    res = close(csock);
#endif

    return 0;

ERROR_SYNCH:
    res = synch_device_end(db, cDeviceID, sDeviceID, SynchType);
ERROR_SOCKET:
#ifndef ANDROID
    res = close(csock);
#endif
ERROR:
    return -1;
}

#ifndef ANDROID
int handle_bt_client_server(sqlite3 *db, sqlite3_int64 cDeviceID, sqlite3_int64 sDeviceID, const bdaddr_t* sbdaddr_t, bt_connection_state* pbt_connection_state, const char** error)
{
    int res;
#ifdef USE_L2CAP
    struct sockaddr_l2 saddr_l2;
    struct bt_security bt_sec;
#endif
    BluetoothSocket ssock;
    unsigned short SynchType;
    struct sockaddr_rc saddr_rc;

#ifdef USE_L2CAP
    //L2CAP
    ssock = socket(AF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);
    if (ssock < 0)
    {
        *error = "Opening L2CAP socket failed";
        goto ERROR;
    }

    memset(&saddr_l2, 0, sizeof(saddr_l2));
    saddr_l2.l2_family = AF_BLUETOOTH;
    saddr_l2.l2_psm = htobs(pbt_connection_state->psm);
    saddr_l2.l2_cid = htobs(0);
    bacpy(&saddr_l2.l2_bdaddr, sbdaddr_t);
    // saddr_l2.l2_bdaddr_type = BDADDR_LE_PUBLIC;//TODO: #31: support both LE/smart and classic

    memset(&bt_sec, 0, sizeof(bt_sec));
    bt_sec.level = BT_SECURITY_LOW;//BT_SECURITY_MEDIUM;
    res = setsockopt(ssock, SOL_BLUETOOTH, BT_SECURITY, &bt_sec, sizeof(bt_sec));
    if (res)
    {
        *error = "Setting L2CAP security level failed";
        goto ERROR_SOCKET;
    }

    // connect to server
    res = connect(ssock, (struct sockaddr *)&saddr_rc, sizeof(saddr_rc));

    if (!res)
    {//connected L2CAP
        //TODO: #31:
        res = set_l2cap_mtu(ssock, (uint16_t)BLUETOOTH_MTU_SIZE_DESIRED, &mtu);
        if (res)
        {
char* text = strerror(errno);
            //TODO: log info: reject

            *error = "failed set_l2cap_mtu()";

            res = close(ssock);
            return -1;
        }

        SynchType = SYNCH_TYPE_BLUETOOTH_L2CAP;
        res = synch_device_begin(db, cDeviceID, sDeviceID, SynchType, 0);
        if (res)
        {
            //TODO: log info: reject

            goto ERROR_SOCKET;
        }
    }
    else
    {
#endif
        //RFCOMM
        ssock = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

        memset(&saddr_rc, 0, sizeof(saddr_rc));
        saddr_rc.rc_family = AF_BLUETOOTH;
        saddr_rc.rc_channel = pbt_connection_state->rc_channel;
        bacpy(&saddr_rc.rc_bdaddr, sbdaddr_t);

        // connect to server
        res = connect(ssock, (struct sockaddr *)&saddr_rc, sizeof(saddr_rc));
        if (res < 0)
        {
char* text = strerror(errno);
            //TODO: log info: reject

            *error = "failed connect()";
            goto ERROR_SOCKET;
        }

        SynchType = SYNCH_TYPE_BLUETOOTH_RFCOMM;
        res = synch_device_begin(db, cDeviceID, sDeviceID, SynchType, 0);
        if (res)
        {
            //TODO: log info: reject

            goto ERROR_SOCKET;
        }
#ifdef USE_L2CAP
    }
#endif

    return handle_bt_client_server_connected(db, cDeviceID, sDeviceID, sbdaddr_t, SynchType, ssock, error);

ERROR_SOCKET:
#ifndef ANDROID
    res = close(ssock);
#endif
ERROR:
    return -1;
}
#endif

int handle_bt_client_server_connected(sqlite3 *db, sqlite3_int64 cDeviceID, sqlite3_int64 sDeviceID, const bdaddr_t* sbdaddr_t, unsigned short SynchType, BluetoothSocket ssock, const char** error)
{
    int res;
    char cbuffer[BLUETOOTH_BUFFER_SIZE];
    sqlite3_int64 sDeviceID_Ack;
    const size_t cbuffer_size = sizeof(cbuffer);

    memset(cbuffer, 0, cbuffer_size);

    //send connection
    res = send_connection(db, cDeviceID, ssock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //receive connection
    res = receive_connection(db, sbdaddr_t, ssock, cbuffer, cbuffer_size, &sDeviceID_Ack);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    if (sDeviceID != sDeviceID_Ack)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //send devices
    res = send_devices(db, ssock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //receive devices
    res = receive_devices(db, sDeviceID, ssock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //send device_details
    res = send_device_details(db, sDeviceID, ssock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //receive device_details
    res = receive_device_details(db, ssock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

#ifdef CS_SENSORS
    //send sensor_details
    res = send_sensor_details(db, ssock, sDeviceID, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //receive sensor_details
    res = receive_sensor_details(db, ssock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }
#endif

#ifdef CS_ALGORITHMS
    //send algorithms
    res = send_algorithm_instances(db, ssock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //receive algorithms
    res = receive_algorithm_instances(db, ssock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //send algorithm_details
    res = send_algorithm_instance_details(db, sDeviceID, ssock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //receive algorithm_details
    res = receive_algorithm_instance_details(db, ssock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }
#endif

#ifdef CS_OBSERVATIONS
    //send observations
    res = send_observations(db, cDeviceID, sDeviceID, ssock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //receive observations
    res = receive_observations(db, ssock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }
#endif

    //Go to peer-to-peer
    res = run_bt_peer(db, cDeviceID, sDeviceID, ssock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_SYNCH;
    }

    //Success
    res = synch_device_end(db, cDeviceID, sDeviceID, SynchType);

#ifndef ANDROID
    res = close(ssock);
#endif

    return 0;

ERROR_SYNCH:
    res = synch_device_end(db, cDeviceID, sDeviceID, SynchType);
ERROR_SOCKET:
#ifndef ANDROID
    res = close(ssock);
#endif
ERROR:
    return -1;
}

int run_bt_peer(sqlite3 *db, sqlite3_int64 mDeviceID, sqlite3_int64 oDeviceID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size)
{
    int db_poll_seconds;
    int res;

    //get db_poll_seconds
    memset(cbuffer, 0, cbuffer_size);
    res = get_config(db, CONFIG_BT_PEER_POLL_SECONDS, cbuffer, cbuffer_size, CONFIG_BT_PEER_POLL_SECONDS_DEFAULT);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    db_poll_seconds = atoi(cbuffer);

    //TODO: #31: handle peer loop
    return 0;
}

int receive_devices(sqlite3 *db, sqlite3_int64 oDeviceID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size)
{
    size_t cbuffer_syncd;
    size_t cbuffer_needed;
    int res;
    json_message* pjson_message;

    cbuffer_needed = sizeof(json_message);

    cbuffer_syncd = 0;
    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    //Received the json_message header
    pjson_message = (json_message*)cbuffer;
    cbuffer_needed += btohl(pjson_message->size_b);

    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 0, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    pjson_message->json[btohl(pjson_message->size_b)] = '\0';

    res = handle_json_devices(db, oDeviceID, pjson_message->json);
    if (res < 0)
    {
        //TODO: log info: reject

        return -1;
    }

    //Success
    return 0;
}

int send_devices(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size)
{
    int res;
    json_message* pjson_message;
    size_t cbuffer_sizeused;
    unsigned long cbuffer_out;

    pjson_message = (json_message*)cbuffer;

    res = load_json_devices(db, pjson_message->json, cbuffer_size - sizeof(json_message), &cbuffer_sizeused);
    if (res)
    {
        return -1;
    }
    pjson_message->size_b = htobl(cbuffer_sizeused);

    cbuffer_out = sizeof(json_message) + cbuffer_sizeused;

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        return -1;
    }

    //Success
    return 0;
}

int receive_connection(sqlite3 *db, const bdaddr_t* obdaddr_t, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, sqlite3_int64* oDeviceID)
{
    size_t cbuffer_syncd;
    size_t cbuffer_needed;
    json_message* pjson_message;
    char oDeviceName[DEVICE_NAME_SIZE + 1];
    int res;
    char cbdaddr[19];
    universal_id _universal_id;

    cbuffer_needed = sizeof(json_message);

    cbuffer_syncd = 0;
    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    //Received the json_message header
    pjson_message = (json_message*)cbuffer;
    cbuffer_needed += btohl(pjson_message->size_b);

    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 0, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    pjson_message->json[btohl(pjson_message->size_b)] = '\0';

    res = handle_json_connection(pjson_message->json, oDeviceName, &_universal_id);
    if (res < 0)
    {
        //TODO: log info: reject

        return -1;
    }

    res = ba2str(obdaddr_t, cbdaddr);
    if (res < 0)
    {
        //TODO: log info: reject

        return -1;
    }

    res = verify_bt_device(db, oDeviceName, cbdaddr, oDeviceID);
    if (res)
    {
printf("Rejected connection from %s", cbdaddr);
        //TODO: log info: reject

        return -1;
    }

    //Success
    return 0;
}

int send_connection(sqlite3 *db, sqlite3_int64 mDeviceID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size)
{
    int res;
    char cDeviceName[DEVICE_NAME_SIZE + 1];
    json_message* pjson_message;
    universal_id _universal_id;
    size_t cbuffer_sizeused;
    unsigned short cbuffer_out;

    memset(cDeviceName, 0, sizeof(cDeviceName));

    res = get_device_name(db, mDeviceID, cDeviceName);
    if (res)
    {//This is a very weird error and should never happen!
        //TODO: log error

        return -1;
    }

    res = get_device_universal_id(db, mDeviceID, &_universal_id);
    if (res)
    {//This is a very weird error and should never happen!
        //TODO: log error

        return -1;
    }

    pjson_message = (json_message*)cbuffer;

    res = load_json_connection(cDeviceName, &_universal_id, pjson_message->json, cbuffer_size - sizeof(json_message), &cbuffer_sizeused);
    if (res)
    {
        //TODO: log error

        return -1;
    }
    pjson_message->size_b = htobl(cbuffer_sizeused);

    cbuffer_out = sizeof(json_message) + cbuffer_sizeused;

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        return -1;
    }

    //Success
    return 0;
}

int send_device_details(sqlite3 *db, sqlite3_int64 oDeviceID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    unsigned int devicecount;
    sqlite3_int64* ForeignDeviceIDs;
    sqlite3_int64* ForeignDeviceIDs_Ack;
    sqlite3_int64* DomesticDeviceIDs;
    int loop;
    unsigned short devicecount_Ack;

    res = sqlite3_prepare_v3(db, SQL_QUERY_UNMAPPED_FOREIGNDEVICES_BY_DEVICEID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //DeviceID
    res = sqlite3_bind_int64(statement, 1, oDeviceID);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }

    ForeignDeviceIDs = calloc(MAX_DEVICE_REQUEST, sizeof(sqlite3_int64));
    devicecount = 0;

    while(1)
    {
        res = sqlite3_step(statement);
        if (res == SQLITE_DONE)
        {
            break;
        }
        if (res != SQLITE_ROW)
        {
            //TODO: log error
            free(ForeignDeviceIDs);
            goto ERROR_STATEMENT;
        }

        ForeignDeviceIDs[devicecount] = sqlite3_column_int64(statement, 0);
        devicecount++;
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        free(ForeignDeviceIDs);
        goto ERROR;
    }

    //Send
    res = send_device_detail_req(sock, devicecount, ForeignDeviceIDs, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log error
        free(ForeignDeviceIDs);
        goto ERROR;
    }

    ForeignDeviceIDs_Ack = calloc(1, devicecount * sizeof(sqlite3_int64));
    DomesticDeviceIDs = calloc(1, devicecount * sizeof(sqlite3_int64));

    //receive
    res = receive_device_detail(db, oDeviceID, sock, cbuffer, cbuffer_size, &devicecount_Ack, ForeignDeviceIDs_Ack, DomesticDeviceIDs);
    if (res)
    {
        //TODO: log error
        free(ForeignDeviceIDs);
        free(ForeignDeviceIDs_Ack);
        free(DomesticDeviceIDs);
        goto ERROR;
    }

    for (loop = 0; loop < devicecount; loop++)
    {
        if (ForeignDeviceIDs[loop] != ForeignDeviceIDs_Ack[loop])
        {//This is an odd error with db in unknown state
            //TODO: consider deleting odd device with DeviceID=DomesticDeviceID
            free(ForeignDeviceIDs);
            free(ForeignDeviceIDs_Ack);
            free(DomesticDeviceIDs);
            goto ERROR;
        }
    }

    free(ForeignDeviceIDs);
    free(ForeignDeviceIDs_Ack);
    free(DomesticDeviceIDs);

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int receive_device_details(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size)
{
    int res;
    sqlite3_int64* DeviceIDs;
    unsigned int devicecount;

    DeviceIDs = calloc(MAX_DEVICE_REQUEST, sizeof(sqlite3_int64));

    res = receive_device_detail_req(sock, cbuffer, cbuffer_size, &devicecount, DeviceIDs);
    if (res)
    {
        //TODO: log error
        free(DeviceIDs);
        return -1;
    }

    res = send_device_detail(db, devicecount, DeviceIDs, sock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log error
        free(DeviceIDs);
        return -1;
    }

    free(DeviceIDs);

    //Success
    return 0;
}

int send_sensor_details(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 oDeviceID, char* cbuffer, size_t cbuffer_size)
{
    int res;
    unsigned short sensorcount;
    json_message* pjson_message;
    sqlite3_int64* ForeignDeviceIDs;
    sqlite3_int64* ForeignSensorIDs;
    size_t cbuffer_sizeused;
    unsigned long cbuffer_out;
    sqlite3_int64* DomesticSensorIDs;

    ForeignDeviceIDs = calloc(MAX_SENSOR_REQUEST, sizeof(sqlite3_int64));
    ForeignSensorIDs = calloc(MAX_SENSOR_REQUEST, sizeof(sqlite3_int64));

    pjson_message = (json_message*)cbuffer;

    res = load_json_sensor_detail_req(db, oDeviceID, &sensorcount, ForeignDeviceIDs, ForeignSensorIDs, pjson_message->json, cbuffer_size - sizeof(json_message), &cbuffer_sizeused);
    if (res)
    {
        //TODO: log error

        return -1;
    }

    pjson_message->size_b = htobl(cbuffer_sizeused);

    cbuffer_out = sizeof(json_message) + cbuffer_sizeused;

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        return -1;
    }

    DomesticSensorIDs = calloc(sensorcount, sizeof(sqlite3_int64));

    res = receive_sensor_detail(db, oDeviceID, sock, sensorcount, ForeignDeviceIDs, ForeignSensorIDs, cbuffer, cbuffer_size, DomesticSensorIDs);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    //Success
    return 0;
}

int receive_sensor_details(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size)
{
    int res;
    unsigned short* sensorcount;
    sqlite3_int64* DomesticSensorIDs;

    DomesticSensorIDs = calloc(MAX_SENSOR_REQUEST, sizeof(sqlite3_int64));

    res = receive_sensor_detail_req(sock, cbuffer, cbuffer_size, sensorcount, DomesticSensorIDs);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    res = send_sensor_detail(db, sock, *sensorcount, DomesticSensorIDs, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    //Success
    return 0;
}

int send_sensor_detail(sqlite3 *db, BluetoothSocket sock, unsigned int sensorcount, sqlite3_int64* DomesticSensorIDs, char* cbuffer, size_t cbuffer_size)
{
    int res;
    json_message* pjson_message;
    size_t cbuffer_sizeused;
    unsigned long cbuffer_out;

    pjson_message = (json_message*)cbuffer;

    res = load_sensor_detail_json(db, sensorcount, DomesticSensorIDs, pjson_message->json, cbuffer_size - sizeof(json_message), &cbuffer_sizeused);
    if (res)
    {
        return -1;
    }

    pjson_message->size_b = htobl(cbuffer_sizeused);

    cbuffer_out = sizeof(json_message) + cbuffer_sizeused;

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        return -1;
    }

    //Success
    return 0;
}

int receive_sensor_detail(sqlite3 *db, sqlite3_int64 oDeviceID, BluetoothSocket sock, unsigned short sensorcount, sqlite3_int64* ForeignDeviceIDs, sqlite3_int64* ForeignSensorIDs, char* cbuffer, size_t cbuffer_size, sqlite3_int64* DomesticSensorIDs)
{
    size_t cbuffer_syncd;
    size_t cbuffer_needed;
    json_message* pjson_message;
    int res;
    unsigned short sensorcount_Ack;

    cbuffer_needed = sizeof(json_message);

    cbuffer_syncd = 0;
    res = readbt(sock, cbuffer, BLUETOOTH_BUFFER_SIZE, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    //Received the json_message header
    pjson_message = (json_message*)cbuffer;

    cbuffer_needed += btohl(pjson_message->size_b);

    res = readbt(sock, cbuffer, BLUETOOTH_BUFFER_SIZE, cbuffer_needed, 0, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    pjson_message->json[btohl(pjson_message->size_b)] = '\0';

    res = insert_sensor_by_json(db, oDeviceID, pjson_message->json, &sensorcount_Ack, DomesticSensorIDs);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    if (sensorcount != sensorcount_Ack)
    {
        //TODO: log error
        return -1;
    }

    res = upsert_foreign_sensor(db, oDeviceID, sensorcount, ForeignDeviceIDs, ForeignSensorIDs, DomesticSensorIDs);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    //Success
    return 0;
}

int receive_sensor_detail_req(BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, unsigned short* sensorcount, sqlite3_int64* DomesticSensorIDs)
{
    json_message* pjson_message;
    size_t cbuffer_syncd;
    int res;
    size_t cbuffer_needed;

    cbuffer_needed = sizeof(json_message);

    cbuffer_syncd = 0;
    res = readbt(sock, cbuffer, BLUETOOTH_BUFFER_SIZE, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    //Received the json_message header
    pjson_message = (json_message*)cbuffer;

    cbuffer_needed += btohl(pjson_message->size_b);

    res = readbt(sock, cbuffer, BLUETOOTH_BUFFER_SIZE, cbuffer_needed, 0, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    pjson_message->json[btohl(pjson_message->size_b)] = '\0';

    //Received the json_message
    res = handle_json_sensor_detail_req(pjson_message->json, sensorcount, DomesticSensorIDs);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    return 0;
}

int send_sensor(BluetoothSocket sock, char* cbuffer, unsigned long cbuffer_out)
{
    int res;

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        return -1;
    }

    return 0;
}

int send_observations(sqlite3 *db, sqlite3_int64 mDeviceID, sqlite3_int64 oDeviceID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    unsigned short SensorType;
    unsigned short ValueType;
    sqlite3_int64 DomesticSensorID;
    sqlite3_int64 ForeignSensorID;
    sqlite3_int64 ForeignSensorID_Ack;
    struct timespec tp;
    unsigned char HasMoreObservationValues;

    //1) Query join of ForeignSensors and Sensors where (ForeignSensors.DeviceID=oDeviceID) AND (Sensors.DeviceID!=mDeviceID) [since we know that our own sensors are always cannonical]
    res = sqlite3_prepare_v3(db, SQL_QUERY_FOREIGN_SENSORS_FOR_UPDATES, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    //DeviceID
    res = sqlite3_bind_int64(statement, 1, oDeviceID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DeviceID
    res = sqlite3_bind_int64(statement, 2, mDeviceID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    while (1)
    {
        res = sqlite3_step(statement);
        if (res == SQLITE_DONE)
        {
            break;
        }
        if (res != SQLITE_ROW)
        {
            goto ERROR_STATEMENT;
        }

        SensorType = sqlite3_column_int(statement, 0);
        ValueType = sqlite3_column_int(statement, 1);
        ForeignSensorID = sqlite3_column_int64(statement, 2);
        DomesticSensorID = sqlite3_column_int64(statement, 3);

        //2) Decide if we want to store that data
        // if (???)
        // {
        //     continue;
        // }

        //3) Query for last known observation
        if (ValueType == OBSERVATION_TYPE_INT32)
        {//Int32
            res = get_last_logged_observationsint32(db, DomesticSensorID, &tp);
            if (res)
            {
                //TODO: log error
                goto ERROR_STATEMENT;
            }

            do
            {
                res = send_observation_req(db, sock, ForeignSensorID, &tp, cbuffer, cbuffer_size);
                if (res)
                {
                    //TODO: log error
                    goto ERROR_STATEMENT;
                }

                res = receive_observationsint32(db, DomesticSensorID, sock, cbuffer, cbuffer_size, &ForeignSensorID_Ack, &HasMoreObservationValues, &tp);
                if (res)
                {
                    //TODO: log error
                    goto ERROR_STATEMENT;
                }
            } while (HasMoreObservationValues);
        }
        else if (ValueType == OBSERVATION_TYPE_INT64)
        {//Int64
            res = get_last_logged_observationsint64(db, DomesticSensorID, &tp);
            if (res)
            {
                //TODO: log error
                goto ERROR_STATEMENT;
            }

            do
            {
                res = send_observation_req(db, sock, ForeignSensorID, &tp, cbuffer, cbuffer_size);
                if (res)
                {
                    //TODO: log error
                    goto ERROR_STATEMENT;
                }

                res = receive_observationsint64(db, DomesticSensorID, sock, cbuffer, cbuffer_size, &ForeignSensorID_Ack, &HasMoreObservationValues, &tp);
                if (res)
                {
                    //TODO: log error
                    goto ERROR_STATEMENT;
                }
            } while (HasMoreObservationValues);
        }
        else if (ValueType == OBSERVATION_TYPE_REAL)
        {//real
            res = get_last_logged_observationsreal(db, DomesticSensorID, &tp);
            if (res)
            {
                //TODO: log error
                goto ERROR_STATEMENT;
            }

            do
            {
                res = send_observation_req(db, sock, ForeignSensorID, &tp, cbuffer, cbuffer_size);
                if (res)
                {
                    //TODO: log error
                    goto ERROR_STATEMENT;
                }

                res = receive_observationsreal(db, DomesticSensorID, sock, cbuffer, cbuffer_size, &ForeignSensorID_Ack, &HasMoreObservationValues, &tp);
                if (res)
                {
                    //TODO: log error
                    goto ERROR_STATEMENT;
                }
            } while (HasMoreObservationValues);
        }
        else if (ValueType == OBSERVATION_TYPE_TEXT)
        {//Text
            res = get_last_logged_observationstext(db, DomesticSensorID, &tp);
            if (res)
            {
                //TODO: log error
                goto ERROR_STATEMENT;
            }

            do
            {
                res = send_observation_req(db, sock, ForeignSensorID, &tp, cbuffer, cbuffer_size);
                if (res)
                {
                    //TODO: log error
                    goto ERROR_STATEMENT;
                }

                res = receive_observationstext(db, DomesticSensorID, sock, cbuffer, cbuffer_size, &ForeignSensorID_Ack, &HasMoreObservationValues, &tp);
                if (res)
                {
                    //TODO: log error
                    goto ERROR_STATEMENT;
                }
            } while (HasMoreObservationValues);
        }
        else if (ValueType == OBSERVATION_TYPE_BLOB)
        {//blob
            res = get_last_logged_observationsblob(db, DomesticSensorID, &tp);
            if (res)
            {
                //TODO: log error
                goto ERROR_STATEMENT;
            }

            do
            {
                res = send_observation_req(db, sock, ForeignSensorID, &tp, cbuffer, cbuffer_size);
                if (res)
                {
                    //TODO: log error
                    goto ERROR_STATEMENT;
                }

                res = receive_observationsblob(db, DomesticSensorID, sock, cbuffer, cbuffer_size, &ForeignSensorID_Ack, &HasMoreObservationValues, &tp);
                if (res)
                {
                    //TODO: log error
                    goto ERROR_STATEMENT;
                }
            } while (HasMoreObservationValues);
        }
        else
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }

    res = end_observations(db, sock, &tp, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int receive_observations(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size)
{
    int res;
    sqlite3_int64 DomesticSensorID;
    struct timespec tp;

    while (1)
    {
        res = receive_observation_req(db, sock, cbuffer, cbuffer_size, &DomesticSensorID, &tp);
        if (res)
        {
            //TODO: log error
            return -1;
        }

        if (DomesticSensorID == 0LL)
        {//end command
            break;
        }

        res = send_observation(db, sock, DomesticSensorID, &tp, cbuffer, cbuffer_size);
        if (res)
        {
            //TODO: log error
            return -1;
        }
    }

    //Success
    return 0;
}

int send_observation_req(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 ForeignSensorID, const struct timespec *tp, char* cbuffer, size_t cbuffer_size)
{
    int res;
    json_message* pjson_message;
    size_t cbuffer_sizeused;
    unsigned long cbuffer_out;

    pjson_message = (json_message*)cbuffer;

    res = load_json_observation_req(db, 1, &ForeignSensorID, pjson_message->json, cbuffer_size - sizeof(json_message), &cbuffer_sizeused);
    if (res)
    {
        //TODO: log error

        return -1;
    }

    pjson_message->size_b = htobl(cbuffer_sizeused);

    cbuffer_out = sizeof(json_message) + cbuffer_sizeused;

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        //TODO: log error

        return -1;
    }

    //Success
    return 0;
}

int end_observations(sqlite3 *db, BluetoothSocket sock, struct timespec *tp, char* cbuffer, size_t cbuffer_size)
{
    tp->tv_sec = 0;
    tp->tv_nsec = 0;

    return send_observation_req(db, sock, 0, tp, cbuffer, cbuffer_size);
}

int receive_observation_req(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, sqlite3_int64* DomesticSensorID, struct timespec *tp)
{
    size_t cbuffer_syncd;
    json_message* pjson_message;
    int res;
    size_t cbuffer_needed;
    unsigned short sensorcount;

    cbuffer_needed = sizeof(json_message);

    cbuffer_syncd = 0;
    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    //Received the json_message header
    pjson_message = (json_message*)cbuffer;
    cbuffer_needed += btohl(pjson_message->size_b);

    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 0, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    pjson_message->json[btohl(pjson_message->size_b)] = '\0';

    res = handle_json_observation_req(db, pjson_message->json, &sensorcount, DomesticSensorID, tp);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    //Success
    return 0;
}

int send_device_detail(sqlite3 *db, unsigned int devicecount, sqlite3_int64* DeviceIDs, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size)
{
    int res;
    json_message* pjson_message;
    size_t cbuffer_sizeused;
    unsigned long cbuffer_out;

    pjson_message = (json_message*)cbuffer;

    res = load_devices_json(db, devicecount, DeviceIDs, pjson_message->json, cbuffer_size - sizeof(json_message), &cbuffer_sizeused);
    if (res)
    {
        return -1;
    }

    pjson_message->size_b = htobl(cbuffer_sizeused);

    cbuffer_out = sizeof(json_message) + cbuffer_sizeused;

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        return -1;
    }

    //Success
    return 0;
}

int receive_device_detail(sqlite3 *db, sqlite3_int64 oDeviceID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, unsigned short* devicecount, sqlite3_int64* ForeignDeviceIDs, sqlite3_int64* DomesticDeviceIDs)
{
    json_message* pjson_message;
    size_t cbuffer_syncd;
    int res;
    size_t cbuffer_needed;

    cbuffer_needed = sizeof(json_message);

    cbuffer_syncd = 0;
    res = readbt(sock, cbuffer, BLUETOOTH_BUFFER_SIZE, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    //Received the json_message header
    pjson_message = (json_message*)cbuffer;

    cbuffer_needed += btohl(pjson_message->size_b);

    res = readbt(sock, cbuffer, BLUETOOTH_BUFFER_SIZE, cbuffer_needed, 0, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    pjson_message->json[btohl(pjson_message->size_b)] = '\0';

    //Received the json_message
    res = upsert_foreign_devices_by_json(db, oDeviceID, pjson_message->json, devicecount, ForeignDeviceIDs, DomesticDeviceIDs);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    return 0;
}

int send_device_detail_req(BluetoothSocket sock, unsigned int devicecount, sqlite3_int64* ForeignDeviceID, char* cbuffer, size_t cbuffer_size)
{
    int res;
    json_message* pjson_message;
    size_t cbuffer_sizeused;
    unsigned long cbuffer_out;

    pjson_message = (json_message*)cbuffer;

    res = load_devicereqs_json(devicecount, ForeignDeviceID, pjson_message->json, cbuffer_size - sizeof(json_message), &cbuffer_sizeused);
    if (res)
    {
        return -1;
    }

    cbuffer_out = sizeof(json_message) + cbuffer_sizeused;

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        return -1;
    }

    //Success
    return 0;
}

int receive_device_detail_req(BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, unsigned int* devicecount, sqlite3_int64* DeviceIDs)
{
    size_t cbuffer_syncd;
    size_t cbuffer_needed;
    json_message* pjson_message;
    int res;
    unsigned int loop;

    cbuffer_needed = sizeof(json_message);

    cbuffer_syncd = 0;
    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    //Received the json_message header
    pjson_message = (json_message*)cbuffer;
    cbuffer_needed += btohl(pjson_message->size_b);

    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 0, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    pjson_message->json[btohl(pjson_message->size_b)] = '\0';

    res = handle_json_devicereq(pjson_message->json, devicecount, DeviceIDs);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    //Success
    return 0;
}

int send_observation(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 DomesticSensorID, const struct timespec *tp, char* cbuffer, size_t cbuffer_size)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    unsigned short ValueType;

    res = sqlite3_prepare_v3(db, SQL_QUERY_VALUETYPE_BY_SENSORID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    //SensorID
    res = sqlite3_bind_int64(statement, 1, DomesticSensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    res = sqlite3_step(statement);
    if (res != SQLITE_ROW)
    {
        goto ERROR_STATEMENT;
    }

    ValueType = sqlite3_column_int(statement, 0);
    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    switch (ValueType)
    {
        case OBSERVATION_TYPE_INT32:
            res = send_observationsint32(db, sock, DomesticSensorID, tp, cbuffer, cbuffer_size);
            if (res)
            {
                goto ERROR;
            }
            break;
        case OBSERVATION_TYPE_INT64:
            res = send_observationsint64(db, sock, DomesticSensorID, tp, cbuffer, cbuffer_size);
            if (res)
            {
                goto ERROR;
            }
            break;
        case OBSERVATION_TYPE_REAL:
            res = send_observationsreal(db, sock, DomesticSensorID, tp, cbuffer, cbuffer_size);
            if (res)
            {
                goto ERROR;
            }
            break;
        case OBSERVATION_TYPE_TEXT:
            res = send_observationstext(db, sock, DomesticSensorID, tp, cbuffer, cbuffer_size);
            if (res)
            {
                goto ERROR;
            }
            break;
        case OBSERVATION_TYPE_BLOB:
            res = send_observationsblob(db, sock, DomesticSensorID, tp, cbuffer, cbuffer_size);
            if (res)
            {
                goto ERROR;
            }
            break;
        default:
            goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int send_observationstext(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 DomesticSensorID, const struct timespec *tp, char* cbuffer, size_t cbuffer_size)
{
    int res;
    observationstext_v1* pobservationstext_v1;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    int loopObservation;
    int loopRightShift;
    const unsigned char* pValue;
    unsigned long cbuffer_out;

    res = sqlite3_prepare_v3(db, SQL_QUERY_OBSERVATIONSTEXT_BY_SENSORID_DATELOGGED, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    //SensorID
    res = sqlite3_bind_int64(statement, 1, DomesticSensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DateLoggedS
    res = sqlite3_bind_int64(statement, 2, tp->tv_sec);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DateLoggedS
    res = sqlite3_bind_int64(statement, 3, tp->tv_sec);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DateLoggedN
    res = sqlite3_bind_int64(statement, 4, tp->tv_nsec);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    pobservationstext_v1 = (observationstext_v1*)cbuffer;
	pobservationstext_v1->SensorID = DomesticSensorID;
    pobservationstext_v1->observationcount = htobs(0);

    cbuffer_out = sizeof(observationstext_v1);

    for (loopObservation = 0; ; loopObservation++)
    {
        res = sqlite3_step(statement);
        if (res == SQLITE_DONE)
        {
            pobservationstext_v1->HasMoreObservationValues = 0;
            break;
        }
        if (res != SQLITE_ROW)
        {
            goto ERROR_STATEMENT;
        }

        //Check if this observation value is too big and finish if so
        if (cbuffer_out + sizeof(observationstext_value_v1) + pobservationstext_v1->values[loopObservation].lValue > cbuffer_size)
        {
            pobservationstext_v1->HasMoreObservationValues = 1;
            break;
        }

        if (loopObservation)
        {//Already have values
            //Shift existing Values to the right by sizeof(_observationstext_value_v1)
            for (loopRightShift = cbuffer_out + sizeof(observationstext_value_v1) - 1;
                loopRightShift >= sizeof(observationstext_v1) + ((loopObservation + 1) * sizeof(observationstext_value_v1));
                    loopRightShift--)
            {
                cbuffer[loopRightShift] = cbuffer[loopRightShift - sizeof(observationstext_value_v1)];
            }
        }

        //Add 1 more observation value
        pobservationstext_v1->observationcount = htobs(loopObservation + 1);

        //DateLoggedS
        pobservationstext_v1->values[loopObservation].DateLoggedS = sqlite3_column_int64(statement, 0);
        //DateLoggedN
        pobservationstext_v1->values[loopObservation].DateLoggedN = sqlite3_column_int64(statement, 1);

        pValue = sqlite3_column_text(statement, 2);
        pobservationstext_v1->values[loopObservation].lValue = strlen((const char*)pValue);
        strncpy(cbuffer + cbuffer_out + sizeof(observationstext_value_v1), (const char*)pValue, pobservationstext_v1->values[loopObservation].lValue);

        cbuffer_out += sizeof(observationstext_value_v1) + pobservationstext_v1->values[loopObservation].lValue;
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int receive_observationstext(sqlite3 *db, sqlite3_int64 DomesticSensorID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, sqlite3_int64* ForeignSensorID, unsigned char* HasMoreObservationValues, struct timespec *tp)
{
    observationstext_v1* pobservationstext_v1;
    size_t cbuffer_syncd;
    size_t cbuffer_needed;
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    int loopValue;
    const char* pValue;

    cbuffer_needed = sizeof(observationstext_v1);

    cbuffer_syncd = 0;
    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR;
    }

    //Received the observationstext_v1 header
    pobservationstext_v1 = (observationstext_v1*)cbuffer;

    cbuffer_needed += pobservationstext_v1->observationcount * sizeof(observationstext_value_v1);

    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR;
    }

    //Received the observationstext_v1 without text
    for (loopValue = 0; loopValue < pobservationstext_v1->observationcount; loopValue++)
    {
        cbuffer_needed += pobservationstext_v1->values[loopValue].lValue;
    }

    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 0, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR;
    }

    //Received the observationstext_v1 with text
    *ForeignSensorID = pobservationstext_v1->SensorID;
    *HasMoreObservationValues = pobservationstext_v1->HasMoreObservationValues;
    pValue = cbuffer + sizeof(observationstext_v1) + (pobservationstext_v1->observationcount * sizeof(observationstext_value_v1));

    res = sqlite3_prepare_v3(db, SQL_INSERT_OBSERVATIONSTEXT, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    for (loopValue = 0; loopValue < pobservationstext_v1->observationcount; loopValue++)
    {
        tp->tv_sec = pobservationstext_v1->values[loopValue].DateLoggedS;
        tp->tv_nsec = pobservationstext_v1->values[loopValue].DateLoggedN;

        //SensorID
        res = sqlite3_bind_int64(statement, 1, DomesticSensorID);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_bind_int64(statement, 2, pobservationstext_v1->values[loopValue].DateLoggedS);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_bind_int64(statement, 3, pobservationstext_v1->values[loopValue].DateLoggedN);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        //Value = value
        res = sqlite3_bind_text(statement, 4, pValue, pobservationstext_v1->values[loopValue].lValue, SQLITE_STATIC);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_step(statement);
        if (res != SQLITE_DONE)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_reset(statement);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        pValue += pobservationstext_v1->values[loopValue].lValue;
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int send_observationsblob(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 DomesticSensorID, const struct timespec *tp, char* cbuffer, size_t cbuffer_size)
{
    int res;
    observationsblob_v1* pobservationsblob_v1;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    int loopObservation;
    int loopRightShift;
    const unsigned char* pValue;
    unsigned long cbuffer_out;

    res = sqlite3_prepare_v3(db, SQL_QUERY_OBSERVATIONSBLOB_BY_SENSORID_DATELOGGED, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    //SensorID
    res = sqlite3_bind_int64(statement, 1, DomesticSensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DateLoggedS
    res = sqlite3_bind_int64(statement, 2, tp->tv_sec);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DateLoggedS
    res = sqlite3_bind_int64(statement, 3, tp->tv_sec);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DateLoggedN
    res = sqlite3_bind_int64(statement, 4, tp->tv_nsec);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    pobservationsblob_v1 = (observationsblob_v1*)cbuffer;
	pobservationsblob_v1->SensorID = DomesticSensorID;
    pobservationsblob_v1->observationcount = htobs(0);

    cbuffer_out = sizeof(observationsblob_v1);

    for (loopObservation = 0; ; loopObservation++)
    {
        res = sqlite3_step(statement);
        if (res == SQLITE_DONE)
        {
            pobservationsblob_v1->HasMoreObservationValues = 0;
            break;
        }
        if (res != SQLITE_ROW)
        {
            goto ERROR_STATEMENT;
        }

        //Check if this observation value is too big and finish if so
        if (cbuffer_out + sizeof(observationsblob_value_v1) + pobservationsblob_v1->values[loopObservation].lValue > cbuffer_size)
        {
            pobservationsblob_v1->HasMoreObservationValues = 1;
            break;
        }

        if (loopObservation)
        {//Already have values
            //Shift existing Values to the right by sizeof(_observationsblob_value_v1)
            for (loopRightShift = cbuffer_out + sizeof(observationsblob_value_v1) - 1;
                loopRightShift >= sizeof(observationsblob_v1) + ((loopObservation + 1) * sizeof(observationsblob_value_v1));
                    loopRightShift--)
            {
                cbuffer[loopRightShift] = cbuffer[loopRightShift - sizeof(observationsblob_value_v1)];
            }
        }

        //Add 1 more observation value
        pobservationsblob_v1->observationcount = htobs(loopObservation + 1);

        //DateLoggedS
        pobservationsblob_v1->values[loopObservation].DateLoggedS = sqlite3_column_int64(statement, 0);
        //DateLoggedN
        pobservationsblob_v1->values[loopObservation].DateLoggedN = sqlite3_column_int64(statement, 1);

        pValue = sqlite3_column_blob(statement, 2);
        pobservationsblob_v1->values[loopObservation].lValue = strlen((const char*)pValue);
        strncpy(cbuffer + cbuffer_out + sizeof(observationsblob_value_v1), (const char*)pValue, pobservationsblob_v1->values[loopObservation].lValue);

        cbuffer_out += sizeof(observationsblob_value_v1) + pobservationsblob_v1->values[loopObservation].lValue;
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int receive_observationsblob(sqlite3 *db, sqlite3_int64 DomesticSensorID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, sqlite3_int64* ForeignSensorID, unsigned char* HasMoreObservationValues, struct timespec *tp)
{
    observationsblob_v1* pobservationsblob_v1;
    size_t cbuffer_syncd;
    size_t cbuffer_needed;
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    int loopValue;
    const char* pValue;

    cbuffer_needed = sizeof(observationsblob_v1);

    cbuffer_syncd = 0;
    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR;
    }

    //Received the observationsblob_v1 header
    pobservationsblob_v1 = (observationsblob_v1*)cbuffer;

    cbuffer_needed += pobservationsblob_v1->observationcount * sizeof(observationsblob_value_v1);

    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR;
    }

    //Received the observationsblob_v1 without blob
    for (loopValue = 0; loopValue < pobservationsblob_v1->observationcount; loopValue++)
    {
        cbuffer_needed += pobservationsblob_v1->values[loopValue].lValue;
    }

    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 0, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR;
    }

    //Received the observationsblob_v1 with blob
    *ForeignSensorID = pobservationsblob_v1->SensorID;
    *HasMoreObservationValues = pobservationsblob_v1->HasMoreObservationValues;
    pValue = cbuffer + sizeof(observationsblob_v1) + (pobservationsblob_v1->observationcount * sizeof(observationsblob_value_v1));

    res = sqlite3_prepare_v3(db, SQL_INSERT_OBSERVATIONSBLOB, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    for (loopValue = 0; loopValue < pobservationsblob_v1->observationcount; loopValue++)
    {
        tp->tv_sec = pobservationsblob_v1->values[loopValue].DateLoggedS;
        tp->tv_nsec = pobservationsblob_v1->values[loopValue].DateLoggedN;

        //SensorID
        res = sqlite3_bind_int64(statement, 1, DomesticSensorID);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_bind_int64(statement, 2, pobservationsblob_v1->values[loopValue].DateLoggedS);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_bind_int64(statement, 3, pobservationsblob_v1->values[loopValue].DateLoggedN);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        //Value = value
        res = sqlite3_bind_blob(statement, 4, pValue, pobservationsblob_v1->values[loopValue].lValue, SQLITE_STATIC);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_step(statement);
        if (res != SQLITE_DONE)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_reset(statement);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        pValue += pobservationsblob_v1->values[loopValue].lValue;
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int send_observationsint32(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 DomesticSensorID, const struct timespec *tp, char* cbuffer, size_t cbuffer_size)
{
    int res;
    observationsint32_v1* pobservationsint32_v1;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    unsigned long cbuffer_out;
    int loopObservation;

    res = sqlite3_prepare_v3(db, SQL_QUERY_OBSERVATIONSINT_BY_SENSORID_DATELOGGED, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    //SensorID
    res = sqlite3_bind_int64(statement, 1, DomesticSensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DateLoggedS
    res = sqlite3_bind_int64(statement, 2, tp->tv_sec);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DateLoggedS
    res = sqlite3_bind_int64(statement, 3, tp->tv_sec);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DateLoggedN
    res = sqlite3_bind_int64(statement, 4, tp->tv_nsec);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    pobservationsint32_v1 = (observationsint32_v1*)cbuffer;
	pobservationsint32_v1->SensorID = DomesticSensorID;
    pobservationsint32_v1->observationcount = htobs(0);

    cbuffer_out = sizeof(observationsint32_v1);

    for (loopObservation = 0; ; loopObservation++)
    {
        res = sqlite3_step(statement);
        if (res == SQLITE_DONE)
        {
            pobservationsint32_v1->HasMoreObservationValues = 0;
            break;
        }
        if (res != SQLITE_ROW)
        {
            goto ERROR_STATEMENT;
        }

        //Check if this observation value is too big and finish if so
        if (cbuffer_out + sizeof(observationsint32_value_v1) > cbuffer_size)
        {
            pobservationsint32_v1->HasMoreObservationValues = 1;
            break;
        }

        //Add 1 more observation value
        pobservationsint32_v1->observationcount = htobs(loopObservation + 1);

        //DateLoggedS
        pobservationsint32_v1->values[loopObservation].DateLoggedS = sqlite3_column_int64(statement, 0);
        //DateLoggedN
        pobservationsint32_v1->values[loopObservation].DateLoggedN = sqlite3_column_int64(statement, 1);

        pobservationsint32_v1->values[loopObservation].Value = sqlite3_column_int(statement, 2);
        cbuffer_out += sizeof(observationsint32_value_v1);
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int receive_observationsint32(sqlite3 *db, sqlite3_int64 DomesticSensorID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, sqlite3_int64* ForeignSensorID, unsigned char* HasMoreObservationValues, struct timespec *tp)
{
    observationsint32_v1* pobservationsint32_v1;
    size_t cbuffer_syncd;
    size_t cbuffer_needed;
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    int loopValue;

    cbuffer_needed = sizeof(observationsint32_v1);

    cbuffer_syncd = 0;
    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR;
    }

    //Received the observationsint32_v1 header
    pobservationsint32_v1 = (observationsint32_v1*)cbuffer;

    cbuffer_needed += pobservationsint32_v1->observationcount * sizeof(observationsint32_value_v1);

    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 0, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR;
    }

    //Received the observationsint32_v1
    *ForeignSensorID = pobservationsint32_v1->SensorID;
    *HasMoreObservationValues = pobservationsint32_v1->HasMoreObservationValues;

    res = sqlite3_prepare_v3(db, SQL_INSERT_OBSERVATIONSINT, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    for (loopValue = 0; loopValue < pobservationsint32_v1->observationcount; loopValue++)
    {
        tp->tv_sec = pobservationsint32_v1->values[loopValue].DateLoggedS;
        tp->tv_nsec = pobservationsint32_v1->values[loopValue].DateLoggedN;

        //SensorID
        res = sqlite3_bind_int64(statement, 1, DomesticSensorID);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_bind_int64(statement, 2, pobservationsint32_v1->values[loopValue].DateLoggedS);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_bind_int64(statement, 3, pobservationsint32_v1->values[loopValue].DateLoggedN);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        //Value = value
        res = sqlite3_bind_int(statement, 4, pobservationsint32_v1->values[loopValue].Value);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_step(statement);
        if (res != SQLITE_DONE)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_reset(statement);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int send_observationsint64(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 DomesticSensorID, const struct timespec *tp, char* cbuffer, size_t cbuffer_size)
{
    int res;
    observationsint64_v1* pobservationsint64_v1;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    unsigned long cbuffer_out;
    int loopObservation;

    res = sqlite3_prepare_v3(db, SQL_QUERY_OBSERVATIONSINT_BY_SENSORID_DATELOGGED, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    //SensorID
    res = sqlite3_bind_int64(statement, 1, DomesticSensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DateLoggedS
    res = sqlite3_bind_int64(statement, 2, tp->tv_sec);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DateLoggedS
    res = sqlite3_bind_int64(statement, 3, tp->tv_sec);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DateLoggedN
    res = sqlite3_bind_int64(statement, 4, tp->tv_nsec);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    pobservationsint64_v1 = (observationsint64_v1*)cbuffer;
	pobservationsint64_v1->SensorID = DomesticSensorID;
    pobservationsint64_v1->observationcount = htobs(0);

    cbuffer_out = sizeof(observationsint64_v1);

    for (loopObservation = 0; ; loopObservation++)
    {
        res = sqlite3_step(statement);
        if (res == SQLITE_DONE)
        {
            pobservationsint64_v1->HasMoreObservationValues = 0;
            break;
        }
        if (res != SQLITE_ROW)
        {
            goto ERROR_STATEMENT;
        }

        //Check if this observation value is too big and finish if so
        if (cbuffer_out + sizeof(observationsint64_value_v1) > cbuffer_size)
        {
            pobservationsint64_v1->HasMoreObservationValues = 1;
            break;
        }

        //Add 1 more observation value
        pobservationsint64_v1->observationcount = htobs(loopObservation + 1);

        //DateLoggedS
        pobservationsint64_v1->values[loopObservation].DateLoggedS = sqlite3_column_int64(statement, 0);
        //DateLoggedN
        pobservationsint64_v1->values[loopObservation].DateLoggedN = sqlite3_column_int64(statement, 1);

        pobservationsint64_v1->values[loopObservation].Value = sqlite3_column_int64(statement, 2);
        cbuffer_out += sizeof(observationsint64_value_v1);
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int receive_observationsint64(sqlite3 *db, sqlite3_int64 DomesticSensorID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, sqlite3_int64* ForeignSensorID, unsigned char* HasMoreObservationValues, struct timespec *tp)
{
    observationsint64_v1* pobservationsint64_v1;
    size_t cbuffer_syncd;
    size_t cbuffer_needed;
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    int loopValue;

    cbuffer_needed = sizeof(observationsint64_v1);

    cbuffer_syncd = 0;
    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR;
    }

    //Received the observationsint64_v1 header
    pobservationsint64_v1 = (observationsint64_v1*)cbuffer;

    cbuffer_needed += pobservationsint64_v1->observationcount * sizeof(observationsint64_value_v1);

    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 0, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR;
    }

    //Received the observationsint64_v1
    *ForeignSensorID = pobservationsint64_v1->SensorID;
    *HasMoreObservationValues = pobservationsint64_v1->HasMoreObservationValues;

    res = sqlite3_prepare_v3(db, SQL_INSERT_OBSERVATIONSINT, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    for (loopValue = 0; loopValue < pobservationsint64_v1->observationcount; loopValue++)
    {
        tp->tv_sec = pobservationsint64_v1->values[loopValue].DateLoggedS;
        tp->tv_nsec = pobservationsint64_v1->values[loopValue].DateLoggedN;

        //SensorID
        res = sqlite3_bind_int64(statement, 1, DomesticSensorID);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_bind_int64(statement, 2, pobservationsint64_v1->values[loopValue].DateLoggedS);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_bind_int64(statement, 3, pobservationsint64_v1->values[loopValue].DateLoggedN);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        //Value = value
        res = sqlite3_bind_int64(statement, 4, pobservationsint64_v1->values[loopValue].Value);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_step(statement);
        if (res != SQLITE_DONE)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_reset(statement);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int send_observationsreal(sqlite3 *db, BluetoothSocket sock, sqlite3_int64 DomesticSensorID, const struct timespec *tp, char* cbuffer, size_t cbuffer_size)
{
    int res;
    observationsreal_v1* pobservationsreal_v1;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    unsigned long cbuffer_out;
    int loopObservation;

    res = sqlite3_prepare_v3(db, SQL_QUERY_OBSERVATIONSREAL_BY_SENSORID_DATELOGGED, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    //SensorID
    res = sqlite3_bind_int64(statement, 1, DomesticSensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DateLoggedS
    res = sqlite3_bind_int64(statement, 2, tp->tv_sec);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DateLoggedS
    res = sqlite3_bind_int64(statement, 3, tp->tv_sec);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //DateLoggedN
    res = sqlite3_bind_int64(statement, 4, tp->tv_nsec);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    pobservationsreal_v1 = (observationsreal_v1*)cbuffer;
	pobservationsreal_v1->SensorID = DomesticSensorID;
    pobservationsreal_v1->observationcount = htobs(0);

    cbuffer_out = sizeof(observationsreal_v1);

    for (loopObservation = 0; ; loopObservation++)
    {
        res = sqlite3_step(statement);
        if (res == SQLITE_DONE)
        {
            pobservationsreal_v1->HasMoreObservationValues = 0;
            break;
        }
        if (res != SQLITE_ROW)
        {
            goto ERROR_STATEMENT;
        }

        //Check if this observation value is too big and finish if so
        if (cbuffer_out + sizeof(observationsreal_value_v1) > cbuffer_size)
        {
            pobservationsreal_v1->HasMoreObservationValues = 1;
            break;
        }

        //Add 1 more observation value
        pobservationsreal_v1->observationcount = htobs(loopObservation + 1);

        //DateLoggedS
        pobservationsreal_v1->values[loopObservation].DateLoggedS = sqlite3_column_int64(statement, 0);
        //DateLoggedN
        pobservationsreal_v1->values[loopObservation].DateLoggedN = sqlite3_column_int64(statement, 1);

        pobservationsreal_v1->values[loopObservation].Value = sqlite3_column_double(statement, 2);
        cbuffer_out += sizeof(observationsreal_value_v1);
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int receive_observationsreal(sqlite3 *db, sqlite3_int64 DomesticSensorID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, sqlite3_int64* ForeignSensorID, unsigned char* HasMoreObservationValues, struct timespec *tp)
{
    observationsreal_v1* pobservationsreal_v1;
    size_t cbuffer_syncd;
    size_t cbuffer_needed;
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    int loopValue;

    cbuffer_needed = sizeof(observationsreal_v1);

    cbuffer_syncd = 0;
    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR;
    }

    //Received the observationsreal_v1 header
    pobservationsreal_v1 = (observationsreal_v1*)cbuffer;

    cbuffer_needed += pobservationsreal_v1->observationcount * sizeof(observationsreal_value_v1);

    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 0, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR;
    }

    //Received the observationsreal_v1
    *ForeignSensorID = pobservationsreal_v1->SensorID;
    *HasMoreObservationValues = pobservationsreal_v1->HasMoreObservationValues;

    res = sqlite3_prepare_v3(db, SQL_INSERT_OBSERVATIONSREAL, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    for (loopValue = 0; loopValue < pobservationsreal_v1->observationcount; loopValue++)
    {
        tp->tv_sec = pobservationsreal_v1->values[loopValue].DateLoggedS;
        tp->tv_nsec = pobservationsreal_v1->values[loopValue].DateLoggedN;

        //SensorID
        res = sqlite3_bind_int64(statement, 1, DomesticSensorID);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_bind_int64(statement, 2, pobservationsreal_v1->values[loopValue].DateLoggedS);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_bind_int64(statement, 3, pobservationsreal_v1->values[loopValue].DateLoggedN);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        //Value = value
        res = sqlite3_bind_double(statement, 4, pobservationsreal_v1->values[loopValue].Value);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_step(statement);
        if (res != SQLITE_DONE)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_reset(statement);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int send_algorithm_instances(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    algorithms_v1* palgorithms_v1;
    int loop_algorithm;
    short sizeDeviceNamercopy;
    unsigned long cbuffer_out;
    const unsigned char* pID;

    palgorithms_v1 = (algorithms_v1*)cbuffer;
    cbuffer_out = sizeof(algorithms_v1);
    palgorithms_v1->algorithmcount = 0;//First time

    res = sqlite3_prepare_v3(db, SQL_QUERY_ALGORITHMINSTANCEID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    for (loop_algorithm = 0; ; loop_algorithm++)
    {
        res = sqlite3_step(statement);
        if (res == SQLITE_DONE)
        {
            break;
        }
        if (res != SQLITE_ROW)
        {
            goto ERROR_STATEMENT;
        }

        //append
        palgorithms_v1->algorithmcount = htobs(loop_algorithm + 1);
        cbuffer_out += sizeof(sqlite3_int64);

        pID = sqlite3_column_blob(statement, 0);
        if (!pID)
        {
            goto ERROR_STATEMENT;
        }

        memcpy(palgorithms_v1->AlgorithmInstanceIDs[loop_algorithm].ID, pID, UNIVERSAL_ID_SIZE_BYTES);
    }

    //Send
    res = send_algorithm_instance(sock, cbuffer, cbuffer_out);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    //Finish up
    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int receive_algorithm_instances(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    algorithms_v1* palgorithms_v1;
    int loop_algorithm;
    size_t cbuffer_syncd;
    size_t cbuffer_needed;

    res = sqlite3_prepare_v3(db, SQL_QUERY_UNMAPPED_ALGORITHMINSTANCES_BY_ALGORITHMINSTANCEID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    cbuffer_needed = sizeof(algorithms_v1);

    cbuffer_syncd = 0;
    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_STATEMENT;
    }

    palgorithms_v1 = (algorithms_v1*)cbuffer;

    //Received the algorithms_v1 header
    cbuffer_needed += (palgorithms_v1->algorithmcount * sizeof(universal_id));

    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 0, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        goto ERROR_STATEMENT;
    }

    //Received the algorithms_v1
    for (loop_algorithm = 0; loop_algorithm < palgorithms_v1->algorithmcount; loop_algorithm++)
    {
        //AlgorithmInstanceID
        res = sqlite3_bind_blob(statement, 1, palgorithms_v1->AlgorithmInstanceIDs[loop_algorithm].ID, sizeof(universal_id), SQLITE_STATIC);
        if (res)
        {
            goto ERROR_STATEMENT;
        }

        res = sqlite3_step(statement);
        if (res == SQLITE_DONE)
        {//Need to map this algorithm, so insert as null map
            res = insert_unmapped_algorithm(db, &palgorithms_v1->AlgorithmInstanceIDs[loop_algorithm]);
            if (res)
            {
                //TODO: log error
                goto ERROR_STATEMENT;
            }
        }
        else if (res == SQLITE_ROW)
        {
            if (sqlite3_column_type(statement, 0) == SQLITE_NULL)
            {//It's still unmapped, so leave it be
                //TODO: Reconsider this situation
            }
            else
            {//Already have this algorithm mapped
            }
        }
        else
        {
            goto ERROR_STATEMENT;
        }

        res = sqlite3_reset(statement);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }

    //Finish up
    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int send_algorithm_instance_details(sqlite3 *db, sqlite3_int64 oDeviceID, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    const char* AlgorithmInstanceID;

    //1) list needed algorithms
    res = sqlite3_prepare_v3(db, SQL_QUERY_UNMAPPED_ALGORITHMINSTANCES, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    //2) req algorithm_details
    while(1)
    {
        res = sqlite3_step(statement);
        if (res == SQLITE_DONE)
        {
            break;
        }
        if (res != SQLITE_ROW)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }

        AlgorithmInstanceID = sqlite3_column_blob(statement, 0);

        res = send_algorithm_instance_detail_req(sock, AlgorithmInstanceID, cbuffer, cbuffer_size);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }

        res = receive_algorithm_instance_detail(db, oDeviceID, sock, AlgorithmInstanceID, cbuffer, cbuffer_size);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }

        //Success
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    //3) end algorithm_details
    res = end_algorithm_instance_details(sock, cbuffer, cbuffer_size);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int send_algorithm_instance(BluetoothSocket sock, char* cbuffer, unsigned long cbuffer_out)
{
    int res;

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        return -1;
    }

    return 0;
}

int receive_algorithm_instance_detail(sqlite3 *db, sqlite3_int64 oDeviceID, BluetoothSocket sock, const char* AlgorithmInstanceID, char* cbuffer, size_t cbuffer_size)
{
    size_t cbuffer_syncd;
    size_t cbuffer_needed;
    json_message* pjson_message;
    int res;

    cbuffer_needed = sizeof(json_message);

    cbuffer_syncd = 0;
    res = readbt(sock, cbuffer, BLUETOOTH_BUFFER_SIZE, cbuffer_needed, 1, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    //Received the json_message
    pjson_message = (json_message*)cbuffer;

    cbuffer_needed += btohl(pjson_message->size_b);

    res = readbt(sock, cbuffer, BLUETOOTH_BUFFER_SIZE, cbuffer_needed, 0, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    pjson_message->json[btohl(pjson_message->size_b)] = '\0';

    res = insert_algorithm_by_json(db, oDeviceID, pjson_message->json);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    //Success
    return 0;
}

int end_algorithm_instance_details(BluetoothSocket sock, char* cbuffer, size_t cbuffer_size)
{
    return send_algorithm_instance_detail_req(sock, NULL, cbuffer, cbuffer_size);
}

int send_algorithm_instance_detail_req(BluetoothSocket sock, const char* AlgorithmInstanceID, char* cbuffer, size_t cbuffer_size)
{
    int res;
    algorithm_detail_req_v1* palgorithm_detail_req_v1;
    int cbuffer_out;

    palgorithm_detail_req_v1 = (algorithm_detail_req_v1*)cbuffer;
    if (AlgorithmInstanceID)
    {
        memcpy(palgorithm_detail_req_v1->AlgorithmInstanceID.ID, AlgorithmInstanceID, sizeof(universal_id));
    }
    else
    {
        memset(palgorithm_detail_req_v1->AlgorithmInstanceID.ID, 0, sizeof(universal_id));
    }

    cbuffer_out = sizeof(algorithm_detail_req_v1);

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        return -1;
    }

    //Success
    return 0;
}

int receive_algorithm_instance_details(sqlite3 *db, BluetoothSocket sock, char* cbuffer, size_t cbuffer_size)
{
    int res;
    universal_id _universal_id;
    int loopZero;
    int allZeros;

    while (1)
    {
        res = receive_algorithm_instance_detail_req(sock, cbuffer, cbuffer_size, &_universal_id);
        if (res)
        {
            //TODO: log error
            return -1;
        }

        allZeros = 1;
        for (int x = 0; x < sizeof(_universal_id); x++)
        {
            if (_universal_id.ID[x] != 0)
            {
                allZeros = 0;
                break;
            }
        }
        if (allZeros)
        {//end command
            break;
        }

        res = send_algorithm_instance_detail(db, sock, &_universal_id, cbuffer, cbuffer_size);
        if (res)
        {
            //TODO: log error
            return -1;
        }
    }

    //Success
    return 0;
}

int send_algorithm_instance_detail(sqlite3 *db, BluetoothSocket sock, const universal_id* puniversal_id, char* cbuffer, size_t cbuffer_size)
{
    int res;
    json_message* pjson_message;
    size_t cbuffer_sizeused;
    unsigned long cbuffer_out;

    pjson_message = (json_message*)cbuffer;

    res = load_json_algorithminstance(db, puniversal_id, pjson_message->json, cbuffer_size - sizeof(json_message), &cbuffer_sizeused);
    if (res)
    {
        //TODO: log error

        return -1;
    }

    pjson_message->size_b = htobl(cbuffer_sizeused);

    cbuffer_out = sizeof(json_message) + cbuffer_sizeused;

    res = writebt(sock, cbuffer_out, cbuffer);
    if (res)
    {
        //TODO: log error

        return -1;
    }

    //Success
    return 0;
}

int receive_algorithm_instance_detail_req(BluetoothSocket sock, char* cbuffer, size_t cbuffer_size, universal_id* puniversal_id)
{
    size_t cbuffer_syncd;
    size_t cbuffer_needed;
    algorithm_detail_req_v1* palgorithm_detail_req_v1;
    int res;

    cbuffer_needed = sizeof(algorithm_detail_req_v1);

    cbuffer_syncd = 0;
    res = readbt(sock, cbuffer, cbuffer_size, cbuffer_needed, 0, &cbuffer_syncd);
    if (res)
    {
        //TODO: log info: reject

        return -1;
    }

    //Received the algorithm_detail_req_v1
    palgorithm_detail_req_v1 = (algorithm_detail_req_v1*)cbuffer;

    memcpy(puniversal_id->ID, palgorithm_detail_req_v1->AlgorithmInstanceID.ID, sizeof(universal_id));

    //Success
    return 0;
}

int insert_unmapped_algorithm(sqlite3 *db, universal_id* puniversal_id)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;

    //Insert devices
    res = sqlite3_prepare_v3(db, SQL_INSERT_UNMAPPED_ALGORITHMINSTANCES, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }
    //AlgorithmInstanceID
    res = sqlite3_bind_blob(statement, 1, puniversal_id->ID, sizeof(universal_id), SQLITE_STATIC);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}
