#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>
#include <sodium.h>
#include <libconfig.h>
#include <libsam3a.h>
#ifdef WITH_BLUETOOTH
#include "bluetooth.h"
#endif
#ifdef WITH_I2P
#include "i2pjava.h"
#endif
#include "corelinux.h"

#define TIMEOUT_SECONDS_CLIENT      15
#define TIMEOUT_SECONDS_POSTCLIENT  1

#define CONFIG_FILENAME_I2P_SELF    "i2p.conf"
#define DB_FILENAME_I2P_SELF        "i2p.db"
#define CONFIG_FILENAME_I2P_OTHER   "i2p.other.conf"
#define DB_FILENAME_I2P_OTHER       "i2p.other.db"
#define CONFIG_FILENAME_SERVER      "server.conf"
#define DB_FILENAME_SERVER          "server.db"
#define CONFIG_FILENAME_CLIENT      "client.conf"
#define DB_FILENAME_CLIENT          "client.db"

#ifdef WITH_I2P
int test_i2p(sqlite3 *db, sqlite3_int64 sDeviceID);
#endif

#ifdef WITH_BLUETOOTH
int test_bt_server(sqlite3 *db, sqlite3_int64 sDeviceID);
int test_bt_client(sqlite3 *db, sqlite3_int64 cDeviceID);
int auth_bt_test_devices(sqlite3 *db);
#endif

int test_prepare(const char* ConfigFilename, const char* dbFilename, const char* ForceHostname, sqlite3 **db, sqlite3_int64 *mDeviceID);
int test_close(const char* ConfigFilename, const char* dbFilename, sqlite3 *db, sqlite3_int64 mDeviceID);
int generate_test_data(sqlite3 *db, sqlite3_int64 DeviceID, int MinObservations, int MaxObservations);

int main(int argc, const char** argv)
{
    int res;
    sqlite3 *db;
    sqlite3_int64 mDeviceID;
    char pubkey[SAM3A_PUBKEY_SIZE+1];
    universal_id _universal_id_device;
    sqlite3 *db_other;
    sqlite3_int64 oDeviceID;
    char pubkey_other[SAM3A_PUBKEY_SIZE+1];
    universal_id _universal_id_device_other;
    const char* error;
    sqlite3_int64 mDeviceID_Ack;
    sqlite3_int64 oDeviceID_Ack;
    __pid_t child;

    if (sodium_init() == -1)
    {
        return 1;
    }

    if (argc < 2)
    {
        return -1;
    }

    if (strcmp(argv[1], "1") == 0)
    {//I2P
#ifdef WITH_I2P
        res = test_prepare(CONFIG_FILENAME_I2P_SELF, DB_FILENAME_I2P_SELF, "Self", &db, &mDeviceID);
        if (res)
        {
            return -1;
        }

        res = get_device_universal_id(db, mDeviceID, &_universal_id_device);
        if (res)
        {
            return -1;
        }

        res = get_i2p_publickey(db, mDeviceID, pubkey);
        if (res)
        {
            return -1;
        }

        res = test_prepare(CONFIG_FILENAME_I2P_OTHER, DB_FILENAME_I2P_OTHER, "Other", &db_other, &oDeviceID);
        if (res)
        {
            return -1;
        }

        res = get_device_universal_id(db_other, oDeviceID, &_universal_id_device_other);
        if (res)
        {
            return -1;
        }

        res = get_i2p_publickey(db_other, oDeviceID, pubkey_other);
        if (res)
        {
            return -1;
        }

        res = authorize_device(db, &_universal_id_device, "Other", 1, 1, NULL, 0, 0, pubkey_other, &oDeviceID_Ack);
        if (res)
        {
            return -1;
        }

        res = authorize_device(db_other, &_universal_id_device_other, "Self", 1, 1, NULL, 0, 0, pubkey, &mDeviceID_Ack);
        if (res)
        {
            return -1;
        }

        error = NULL;

        //Fork bt process
        if ((child=fork()) < 0)
        {//failed fork
            return -1;
        }
        if (child)
        {//parent
            res = run_i2p(db, mDeviceID, &error);
            if (res)
            {
                res = test_close(CONFIG_FILENAME_I2P_SELF, DB_FILENAME_I2P_SELF, db, mDeviceID);

                return -1;
            }

            res = test_close(CONFIG_FILENAME_I2P_SELF, DB_FILENAME_I2P_SELF, db, mDeviceID);
        }
        else
        {//child
            res = run_i2p(db_other, mDeviceID, &error);
            if (res)
            {
                res = test_close(CONFIG_FILENAME_I2P_OTHER, DB_FILENAME_I2P_OTHER, db_other, mDeviceID);

                exit(-1);
            }

            res = test_close(CONFIG_FILENAME_I2P_OTHER, DB_FILENAME_I2P_OTHER, db_other, mDeviceID);

            exit(res);
        }

        return 0;
#else
        fprintf(stderr, "I2P not compiled!\n");
        return -1;
#endif
    }

    if (strcmp(argv[1], "2") == 0)
    {//BT server
#ifdef WITH_BLUETOOTH
        res = test_prepare(CONFIG_FILENAME_SERVER, DB_FILENAME_SERVER, NULL, &db, &mDeviceID);
        if (res)
        {
            return -1;
        }

        res = test_bt_server(db, mDeviceID);
        if (res)
        {
            res = test_close(CONFIG_FILENAME_SERVER, DB_FILENAME_SERVER, db, mDeviceID);

            return -1;
        }

        res = test_close(CONFIG_FILENAME_SERVER, DB_FILENAME_SERVER, db, mDeviceID);

        return 0;
#else
        fprintf(stderr, "Bluetooth not compiled!\n");
        return -1;
#endif
    }

    if (strcmp(argv[1], "3") == 0)
    {//BT client
#ifdef WITH_BLUETOOTH
        res = test_prepare(CONFIG_FILENAME_CLIENT, DB_FILENAME_CLIENT, NULL, &db, &mDeviceID);
        if (res)
        {
            return -1;
        }

        res = test_bt_client(db, mDeviceID);
        if (res)
        {
            res = test_close(CONFIG_FILENAME_CLIENT, DB_FILENAME_CLIENT, db, mDeviceID);

            return -1;
        }

        res = test_close(CONFIG_FILENAME_CLIENT, DB_FILENAME_CLIENT, db, mDeviceID);

        return 0;
#else
        fprintf(stderr, "Bluetooth not compiled!\n");
        return -1;
#endif
    }

    return -1;
}

#ifdef WITH_BLUETOOTH
int auth_bt_test_devices(sqlite3 *db)
{
    int res;
    sqlite3_int64 oDeviceID;
    char cmbdaddr[19];
    const char* DeviceName_GB01 = "sean-GB01";
    const char* bdaddr_GB01 = "40:9F:38:45:A3:2C";
#ifdef USE_BASE_85
    universal_id_base85 _universal_id_base85_GB01;
#endif
#ifdef USE_BASE_64
    universal_id_base64 _universal_id_base64_GB01;
#endif
    // const char* DeviceName_LIFEBOOK = "sean-LIFEBOOK-T901";
    // const char* bdaddr_LIFEBOOK = "C0:18:85:F2:8F:8D";
    // universal_id_base85 _universal_id_base85_LIFEBOOK;
#ifdef USE_BASE_85
    const char* Universal_id_base85_Galaxy_Note8 = "$MOfN^^D&K53<E3Yl0_m5rRhZi/s]hgPOhg+f/\\+";
#endif
#ifdef USE_BASE_64
    const char* Universal_id_base64_Galaxy_Note8 = CARP;
#endif
    const char* DeviceName_Galaxy_Note8 = "Galaxy Note8";
    const char* bdaddr_Galaxy_Note8 = "94:8B:C1:CB:CB:77";
    universal_id _universal_id_Galaxy_Note8;

#ifdef USE_BASE_85
    res = decode_85(_universal_id_Galaxy_Note8.ID, Universal_id_base85_Galaxy_Note8, strlen(Universal_id_base85_Galaxy_Note8));
#endif
#ifdef USE_BASE_64
    res = decode_64(_universal_id_Galaxy_Note8.ID, Universal_id_base64_Galaxy_Note8, strlen(Universal_id_base64_Galaxy_Note8));
#endif
    if (res != UNIVERSAL_ID_SIZE_BYTES)
    {
        return -1;
    }

    //Add authorized device(s)
    res = get_bt_address(cmbdaddr);
    if (res)
    {
        return -1;
    }
    if (strcmp(cmbdaddr, bdaddr_GB01) == 0)
    {
        // res = authorize_device(db, CARP, DeviceName_LIFEBOOK, 1, 1, bdaddr_LIFEBOOK, 0, 0, NULL, &oDeviceID);
        // if (res)
        // {
        //     fprintf(stderr, "authorize_device\n");
        //     return -1;
        // }
        res = authorize_device(db, &_universal_id_Galaxy_Note8, DeviceName_Galaxy_Note8, 1, 1, bdaddr_Galaxy_Note8, 0, 0, NULL, &oDeviceID);
        if (res)
        {
            fprintf(stderr, "authorize_device\n");
            return -1;
        }
    }
    // else if (strcmp(cmbdaddr, bdaddr_LIFEBOOK) == 0)
    // {
    //     res = authorize_device(db, CARP, DeviceName_GB01, 1, 1, bdaddr_GB01, 0, 0, NULL, &oDeviceID);
    //     if (res)
    //     {
    //         fprintf(stderr, "authorize_device\n");
    //         return -1;
    //     }
    // }
    else
    {
        fprintf(stderr, "authorize_device\n");
        return -1;
    }

    return 0;
}
#endif

#define MIN_TEST_TEXT_LENGTH    1
#define MAX_TEST_TEXT_LENGTH    99

char get_random_char()
{
    const char alpha[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                          'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                          'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
                          'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
                          'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
                          'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                          'Y', 'Z', ' ', ',', '.', '!', '?'};
    return alpha[randombytes_random() % sizeof(alpha)];
}
int generate_test_data(sqlite3 *db, sqlite3_int64 DeviceID, int MinObservations, int MaxObservations)
{
    int res;
    sqlite3_int64 SensorID;
    sqlite3_stmt* statement;
    int testDataCount;
    int testDataLoop;

    int int32Value;

    sqlite3_int64 int64Value;

    double realValue;

    char buffer[MAX_TEST_TEXT_LENGTH + 1];
    int testTextCount;
    int testTextLoop;

    //OBSERVATION_TYPE_INT32
    res = create_db_sensor(db, DeviceID, "TEST_INT32_0", SENSOR_TYPE_TEST_INT32, OBSERVATION_TYPE_INT32, &SensorID);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    res = prepare_log_observationsint32(db, SensorID, &statement);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    testDataCount = MinObservations + (randombytes_random() % (MaxObservations - MinObservations));
    for (testDataLoop = 0; testDataLoop < testDataCount; testDataLoop++)
    {
        //Use randombytes_random() for value and randombytes_random() for sign
        int32Value = randombytes_random();
        if (randombytes_random() % 2)
        {
            int32Value = -int32Value;
        }

        res = log_observationsint32(statement, int32Value);
        if (res)
        {
            //TODO: log error

            return -1;
        }
    }

    res = close_log_observationsint32(statement);
    if (res)
    {
        return -1;
    }

    //OBSERVATION_TYPE_INT64
    res = create_db_sensor(db, DeviceID, "TEST_INT64_0", SENSOR_TYPE_TEST_INT64, OBSERVATION_TYPE_INT64, &SensorID);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    res = prepare_log_observationsint64(db, SensorID, &statement);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    testDataCount = MinObservations + (randombytes_random() % (MaxObservations - MinObservations));
    for (testDataLoop = 0; testDataLoop < testDataCount; testDataLoop++)
    {
        randombytes_buf(&int64Value, sizeof(sqlite3_int64));

        res = log_observationsint64(statement, int64Value);
        if (res)
        {
            //TODO: log error

            return -1;
        }
    }

    res = close_log_observationsint64(statement);
    if (res)
    {
        return -1;
    }

    //OBSERVATION_TYPE_REAL
    res = create_db_sensor(db, DeviceID, "TEST_REAL_0", SENSOR_TYPE_TEST_REAL, OBSERVATION_TYPE_REAL, &SensorID);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    res = prepare_log_observationsreal(db, SensorID, &statement);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    testDataCount = MinObservations + (randombytes_random() % (MaxObservations - MinObservations));
    for (testDataLoop = 0; testDataLoop < testDataCount; testDataLoop++)
    {
        //randombytes_random MAX => 0xffffffff
        realValue = ((double)randombytes_random())/((double)(0xffffffff))*2.0-1.0;//float in range -1 to 1

        res = log_observationsreal(statement, realValue);
        if (res)
        {
            //TODO: log error

            return -1;
        }
    }

    res = close_log_observationsreal(statement);
    if (res)
    {
        return -1;
    }

    //OBSERVATION_TYPE_TEXT
    res = create_db_sensor(db, DeviceID, "TEST_TEXT_0", SENSOR_TYPE_TEST_TEXT, OBSERVATION_TYPE_TEXT, &SensorID);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    res = prepare_log_observationstext(db, SensorID, &statement);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    testDataCount = MinObservations + (randombytes_random() % (MaxObservations - MinObservations));
    for (testDataLoop = 0; testDataLoop < testDataCount; testDataLoop++)
    {
        memset(buffer, 0, MAX_TEST_TEXT_LENGTH + 1);
        testTextCount = MIN_TEST_TEXT_LENGTH + (randombytes_random() % (MAX_TEST_TEXT_LENGTH - MIN_TEST_TEXT_LENGTH));

        for (testTextLoop = 0; testTextLoop < testTextCount; testTextLoop++)
        {
            buffer[testTextLoop] = get_random_char();
        }

        res = log_observationstext(statement, buffer);
        if (res)
        {
            //TODO: log error

            return -1;
        }
    }

    res = close_log_observationstext(statement);
    if (res)
    {
        return -1;
    }

    //OBSERVATION_TYPE_BLOB
    res = create_db_sensor(db, DeviceID, "TEST_BLOB_0", SENSOR_TYPE_TEST_BLOB, OBSERVATION_TYPE_BLOB, &SensorID);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    res = prepare_log_observationsblob(db, SensorID, &statement);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    testDataCount = MinObservations + (randombytes_random() % (MaxObservations - MinObservations));
    for (testDataLoop = 0; testDataLoop < testDataCount; testDataLoop++)
    {
        memset(buffer, 0, MAX_TEST_TEXT_LENGTH + 1);
        testTextCount = MIN_TEST_TEXT_LENGTH + (randombytes_random() % (MAX_TEST_TEXT_LENGTH - MIN_TEST_TEXT_LENGTH));

        randombytes_buf(buffer, testTextCount);

        res = log_observationsblob(statement, buffer, testTextCount);
        if (res)
        {
            //TODO: log error

            return -1;
        }
    }

    res = close_log_observationsblob(statement);
    if (res)
    {
        return -1;
    }

    return 0;
}

#ifdef WITH_BLUETOOTH
int test_bt_server(sqlite3 *db, sqlite3_int64 sDeviceID)
{
    int res;
    const char* error;

    //authorize devices
    res = auth_bt_test_devices(db);
    if (res)
    {
        fprintf(stderr, "auth_bt_test_devices\n");
        return -1;
    }

    error = NULL;

    res = run_bt_server(db, sDeviceID, &error);

    return res;
}

int test_bt_client(sqlite3 *db, sqlite3_int64 cDeviceID)
{
    const char* error;
    int res;

    //authorize devices
    res = auth_bt_test_devices(db);
    if (res)
    {
        fprintf(stderr, "auth_bt_test_devices\n");
        return -1;
    }

    error = NULL;

    res = run_bt_client(db, cDeviceID, &error);
    if (res)
    {
        if (error)
        {
            fprintf(stderr, "run_bt_client() error: %s\n", error);
        }
        else
        {
            fprintf(stderr, "unknown run_bt_client()\n");
        }
    }

    return res;
}
#endif

int test_prepare(const char* ConfigFilename, const char* dbFilename, const char* ForceHostname, sqlite3 **db, sqlite3_int64 *mDeviceID)
{
    sqlite3_stmt* statement;
    const char* error;
    int res;

    //create db
    res = check_configfile(ConfigFilename, dbFilename, mDeviceID);
    if (!res)
    {
        fprintf(stderr, "check_config\n");
        return -1;
    }

    res = create_db(dbFilename, ForceHostname, db, mDeviceID);
    if (res)
    {
        fprintf(stderr, "create_db\n");
        return -1;
    }

    res = create_configfile(ConfigFilename, dbFilename, *mDeviceID);
    if (res)
    {
        fprintf(stderr, "create_config\n");
        return -1;
    }

    res = connect_to_db(dbFilename, *mDeviceID, db);
    if (res)
    {
        fprintf(stderr, "connect_to_db\n");
        return -1;
    }

    //generate test data
    res = generate_test_data(*db, *mDeviceID, 1, 10);
    if (res)
    {
        fprintf(stderr, "generate_test_data\n");
        return -1;
    }

    return 0;
}

int test_close(const char* ConfigFilename, const char* dbFilename, sqlite3 *db, sqlite3_int64 mDeviceID)
{
    int res;

    //Delete db
    res = delete_db(dbFilename, db);
    if (res)
    {
        fprintf(stderr, "delete_db\n");
        return -1;
    }

    res = delete_configfile(ConfigFilename);
    if (res)
    {
        fprintf(stderr, "delete_config\n");
        return -1;
    }

    return 0;
}
