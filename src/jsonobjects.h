#ifndef _JSONOBJECTS_H_
#define _JSONOBJECTS_H_

#define JSON_CONNECTION_EXAMPLE \
"{" \
"    \"connection\":" \
"    {" \
"        \"name\": \"A\"," \
"        \"id\": \"A\"" \
"    }" \
"}"
#define JSON_CONNECTION_CONNECTION      "connection"
#define JSON_CONNECTION_CONNECTION_NAME "name"
#define JSON_CONNECTION_CONNECTION_UUID "uuid"

#define JSON_DEVICES_EXAMPLE \
"{" \
"    \"devices\":" \
"    \"[" \
"       \"A\"" \
"    ]" \
"}"
#define JSON_DEVICES_DEVICES                        "devices"

#define JSON_DEVICEREQ_EXAMPLE \
"{" \
"    \"devicereqs\":" \
"    \"[" \
"       \"A\"" \
"    ]" \
"}"
#define JSON_DEVICEREQ_DEVICEREQS                   "devicereqs"

#define JSON_DEVICE_EXAMPLE \
"{" \
"    \"devices\":" \
"    \"[" \
"       \"device\":" \
"       {" \
"           \"id\": \"A\"," \
"           \"name\": \"A\"," \
"           \"updateds\": \"A\"," \
"           \"type\": \"A\"," \
"           \"storagestyle\": \"A\"," \
"           \"bdaddr\": \"A\"," \
"           \"rfcommchannel\": \"A\"," \
"           \"psm\": \"A\"," \
"           \"i2p\": \"A\"," \
"           \"sensors\":" \
"           \"[" \
"              \"A\"" \
"           ]" \
"       }" \
"    ]" \
"}"
#define JSON_DEVICE_DEVICES                         "devices"
#define JSON_DEVICE_DEVICES_DEVICE                  "device"
#define JSON_DEVICE_DEVICES_DEVICE_ID               "id"
#define JSON_DEVICE_DEVICES_DEVICE_UUID             "uuid"
#define JSON_DEVICE_DEVICES_DEVICE_NAME             "name"
#define JSON_DEVICE_DEVICES_DEVICE_UPDATEDS         "updateds"
#define JSON_DEVICE_DEVICES_DEVICE_TYPE             "type"
#define JSON_DEVICE_DEVICES_DEVICE_STORAGESTYLE     "storagestyle"
#define JSON_DEVICE_DEVICES_DEVICE_BDADDR           "bdaddr"
#define JSON_DEVICE_DEVICES_DEVICE_RFCOMMCHANNEL    "rfcommchannel"
#define JSON_DEVICE_DEVICES_DEVICE_PSM              "psm"
#define JSON_DEVICE_DEVICES_DEVICE_I2P              "i2p"
#define JSON_DEVICE_DEVICES_DEVICE_SENSORS          "sensors"

#define JSON_SENSORREQS_EXAMPLE \
"{" \
"    \"sensorreqs\":" \
"    \"[" \
"       \"A\"" \
"    ]" \
"}"
#define JSON_SENSORREQS_SENSORREQS            "sensorreqs"

#define JSON_SENSOR_EXAMPLE \
"{" \
"    \"sensors\":" \
"    \"[" \
"       \"sensor\":" \
"       {" \
"           \"name\": \"A\"," \
"           \"id\": \"A\"," \
"           \"sensortype\": \"A\"," \
"           \"valuetype\": \"A\"," \
"           \"addeds\": \"A\"" \
"       }" \
"    ]" \
"}"
#define JSON_SENSOR_SENSORS                     "sensors"
#define JSON_SENSOR_SENSORS_SENSOR              "sensor"
#define JSON_SENSOR_SENSORS_SENSOR_NAME         "name"
#define JSON_SENSOR_SENSORS_SENSOR_ID           "id"
#define JSON_SENSOR_SENSORS_SENSOR_SENSORTYPE   "sensortype"
#define JSON_SENSOR_SENSORS_SENSOR_VALUETYPE    "valuetype"
#define JSON_SENSOR_SENSORS_SENSOR_ADDEDS       "addeds"

#define JSON_ALGORITHMINSTANCE_EXAMPLE \
"{" \
"    \"algorithminstance\":" \
"    {" \
"        \"id\": \"A\"," \
"        \"algorithmid\": \"B\"," \
"        \"type\": \"C\"," \
"        \"version\": \"D\"," \
"        \"resulttype\": \"E\"," \
"        \"statusgroups\":" \
"        [" \
"           \"{" \
"               \"label\": \"F\"," \
"               \"statuses\":" \
"               [" \
"                   {\"status\": \"X\"}," \
"               ]" \
"           \"}" \
"        ]," \
"        \"sensors\":" \
"        [" \
"            {\"id\": \"Y\"}" \
"        ]," \
"        \"dependencies\":" \
"        [" \
"            {\"id\": \"Y\"}" \
"        ]" \
"    }" \
"}"
#define JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE                "algorithminstance"
#define JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_ID             "id"
#define JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_ALGORITHMID    "algorithmid"
#define JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_TYPE           "type"
#define JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_VERSION        "version"
#define JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_RESULTTYPE     "resulttype"
#define JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_STATUSGROUPS   "statusgroups"
#define JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_SENSORS        "sensors"
#define JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_DEPENDENCIES   "dependencies"

#define JSON_observationREQS_EXAMPLE \
"{" \
"   \"observationreqs\":" \
"   \"[" \
"       \"observationreq\":" \
"       \"{" \
"           \"id\": \"A\"," \
"           \"LastKnownDateLoggedS\": \"B\"," \
"           \"LastKnownDateLoggedN\": \"C\"" \
"       \"}" \
"   ]" \
"}"
#define JSON_observationREQS_observationreqs                                        "observationreqs"
#define JSON_observationREQS_observationreqs_observationreq                         "observationreq"
#define JSON_observationREQS_observationreqs_observationreq_LastKnownDateLoggedS    "LastKnownDateLoggedS"
#define JSON_observationREQS_observationreqs_observationreq_LastKnownDateLoggedN    "LastKnownDateLoggedN"

#endif
