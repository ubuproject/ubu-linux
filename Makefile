# Copyright (c) 2019, ubu
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

SHELL := $(shell which bash)

cmake_target = all

cmake-ubu-linux  = cmake

cmake-debug      = -D CMAKE_BUILD_TYPE=Debug
cmake-coverage   = -D WITH_COVERAGE=ON
cmake-tests      = -D WITH_TESTS=ON
cmake-net-tests  = -D WITH_NET_TESTS=ON
cmake-daemon     = -D WITH_DAEMON=ON

libconfig = deps/libconfig

build = build/

# cmake builder macro
define CMAKE
	cmake -E make_directory $1
	cmake -E chdir $1 $2 ../
endef

define CLEAN_LIBCONFIG
	cd $(libconfig); \
	rm -rf build/*; \
	make clean;
endef

all: deps daemon tests net-tests

deps:

daemon: deps
	$(eval cmake-ubu-linux += $(cmake-debug) $(cmake-daemon))
	$(call CMAKE,$(build),$(cmake-ubu-linux)) && ${MAKE} -C $(build) $(cmake_target)

tests: deps
	$(eval cmake-ubu-linux += $(cmake-debug) $(cmake-tests))
	$(call CMAKE,$(build),$(cmake-ubu-linux)) && ${MAKE} -C $(build) $(cmake_target)

net-tests: deps
	$(eval cmake-ubu-linux += $(cmake-debug) $(cmake-tests) $(cmake-net-tests))
	$(call CMAKE,$(build),$(cmake-ubu-linux)) && ${MAKE} -C $(build) $(cmake_target)

coverage: tests
	$(eval cmake-ubu-linux += $(cmake-debug) $(cmake-coverage) $(cmake-tests) $(cmake-net-tests))
	$(call CMAKE,$(build),$(cmake-ubu-linux)) && ${MAKE} -C $(build) $(cmake_target)

clean:
	rm -rf $(build)

clean-deps:
	$(call CLEAN_LIBCONFIG)

.PHONY: all daemon tests net-tests coverage clean clean-deps
