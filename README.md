# UbU-linux

Desktop version of UbU project, part of the Strategoi system

# Architectural design

Linux Server:
-DB:
    -MySQL
-Synchronization:
    -Internet:
        -REST/Soap server
        -REST/OAuth 2.0
            -external services
-Report:
    -Apache


Linux/Windows/Mac Desktop:
-Sensors:
    -Windows
    -Keyboard/Mouse (usage)
    -Camera
    -Mic
-DB:
    -SQLite
-Synchronization:
    -P2P:
        -Bluetooth (client/server)
        -WiFi (client/server)?
    -Internet:
        -REST/Soap client
-Report:
    -Apache?


Android Mobile:
-Sensors:
    -All...
    -Network?
-DB:
    -SQLite
-Synchronization:
    -P2P:
        -Bluetooth (client/server)
        -WiFi (client/server)?
        -WiFi Direct?
        -NFC?
    -Internet:
        -REST/Soap client
        -Google Drive?


iOS Mobile:
-Sensors:
    -?
-DB:
    -SQLite
-Synchronization:
    -P2P:
        -Bluetooth (client/server)
        -WiFi (client/server)?
        -WiFi Direct?
        -NFC?
    -Internet:
        -REST/Soap client





DB:
    -User
        -PublicKey
    -Devices
        -DeviceID
        -DeviceType
        -DeviceName
        -StorageStyle (Full, Partial, None)
        -Bluetooth address
        -MAC address
        -IP address
        -TCP port
    -Sensors
        -SensorID
        -DeviceID
        -SensorType
        -SensorName
        -ValueType
    -Synch
        -DeviceIDServer
        -DeviceIDClient
        -DateStarted
        -DateFinished
    -Observations
        (-DeviceID)
        -SensorID
        -DateLogged
        -Value
    -Algorithms
        -AlgorithmID
           -DeviceID
     -AlgorithmType
     -AlgorithmVersion
        -ResultType
        -LatestDateStarted
        -LatestDateFinished
    -AlgorithmSensors
        -AlgorithmID
        -SensorID
    -Results
        -AlgorithmID
        -LatestDateStarted
        -LatestDateFinished
        -Value


Synchronization protocol:
-Client connects to server
-If dirty, Client/Server upserts Devices/Sensors/Algorithms/AlgorithmSensors list
-Client/Server upserts Results
-Client/Server upserts Observations: if (StorageStyle), for each SensorID, query MAX(Observations.DateLogged) and pull missing

