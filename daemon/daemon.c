#include <stdio.h>    //printf(3)
#include <stdlib.h>   //exit(3)
#include <unistd.h>   //fork(3), chdir(3), sysconf(3)
#include <signal.h>   //signal(3)
#include <sys/stat.h> //umask(3)
#include <sys/types.h>
#include <sys/wait.h>
#include <syslog.h>   //syslog(3), openlog(3), closelog(3)
#include <sodium.h>
#include "core.h"
#include "xwindows.h"
#include "corelinux.h"
#ifdef WITH_BLUETOOTH
#include "bluetooth.h"
#endif
#ifdef WITH_I2P
#include "i2pjava.h"
#endif

#define CHILD_XWINDOWS              1
#define CHILD_BT                    2

int daemonize(char* name, char* path, char* outfile, char* errfile, char* infile)
{
    pid_t child;

    if(!path) { path="/tmp"; }
    if(!name) { name="ubu-linux-daemon"; }
    if(!infile) { infile="/dev/null"; }
    if(!outfile) { outfile="/dev/null"; }
    if(!errfile) { errfile="/dev/null"; }

    //fork, detach from process group leader
    if((child=fork()) < 0)
    { //failed fork
        fprintf(stderr,"error: failed fork\n");
        exit(EXIT_FAILURE);
    }
    if (child > 0)
    { //parent
        exit(EXIT_SUCCESS);
    }
    if(setsid() < 0)
    { //failed to become session leader
        //TODO: log error
        fprintf(stderr,"error: failed setsid\n");
        exit(EXIT_FAILURE);
    }

    //catch/ignore signals
    signal(SIGCHLD,SIG_IGN);
    signal(SIGHUP,SIG_IGN);

    //fork second time
    if ((child=fork()) < 0)
    { //failed fork
        //TODO: log error
        fprintf(stderr,"error: failed fork\n");
        exit(EXIT_FAILURE);
    }
    if( child>0 )
    { //parent
        exit(EXIT_SUCCESS);
    }

    //new file permissions
    umask(0);
    //change to path directory
    chdir(path);

    //Close all open file descriptors
    int fd;
    for( fd=sysconf(_SC_OPEN_MAX); fd>0; --fd )
    {
        close(fd);
    }

    //reopen stdin, stdout, stderr
    stdin=fopen(infile,"r");   //fd=0
    stdout=fopen(outfile,"w+");  //fd=1
    stderr=fopen(errfile,"w+");  //fd=2

    //open syslog
    openlog(name,LOG_PID,LOG_DAEMON);
    return 0;
}

#define STORAGE_PATH                "/usr/share/ubu"
#define MAX_CHILD_PROCESSES         2

int main()
{
    int res;
    char dbFilename[PATH_MAX];
    sqlite3 *db;
    sqlite3_int64 mDeviceID;
    int first_run;
    int child_type[MAX_CHILD_PROCESSES];
    int child_pid[MAX_CHILD_PROCESSES];
    int total_children = 0;
    int child;
    int loop;
    int status;
    pid_t pid;
    const char* error;

    if (sodium_init() == -1)
    {
        return 1;
    }

    const char* path = STORAGE_PATH;
    struct stat st = {0};
    if (stat(path, &st) == -1)
    {
        mkdir(path, 0600);
    }

    //Start daemon
    res = daemonize("ubu-linux-daemon", (char*)path, NULL, NULL, NULL);
    if(res)
    {
        //TODO: log error
        fprintf(stderr,"error: daemonize failed\n");
        exit(EXIT_FAILURE);
    }

    //Check config
    res = check_configfile(NULL, dbFilename, &mDeviceID);

    if (res)
    {//First time running, so create db
        first_run = 1;

        res = create_db(NULL, NULL, &db, &mDeviceID);
        if (res)
        {
            fprintf(stderr,"error: daemonize failed\n");
            exit(EXIT_FAILURE);
        }

        res = create_configfile(NULL, NULL, mDeviceID);
        if (res)
        {
            fprintf(stderr,"error: daemonize failed\n");
            exit(EXIT_FAILURE);
        }
    }
    else
    {
        first_run = 0;

        //Connect to db
        res = connect_to_db(dbFilename, mDeviceID, &db);
        if (res)
        {
            fprintf(stderr,"error: daemonize failed\n");
            exit(EXIT_FAILURE);
        }
    }

#ifdef WITH_BLUETOOTH
    //Bluetooth
    res = has_bt(&error);
    if (!res)
    {//Bluetooth exists

        //Bluetooth server
        //Fork bt process
        if ((child=fork()) < 0)
        {//failed fork
            syslog(LOG_ERR, "fork for run_bt() failed");
            exit(EXIT_FAILURE);
        }
        if (child)
        {//parent
            child_type[total_children] = CHILD_BT;
            child_pid[total_children++] = child;
        }
        else
        {//child
            res = run_bt_server(db, mDeviceID, &error);
            if (res)
            {
                syslog(LOG_ERR, "run_bt_server() failed due to reason: %s", error);
                exit(EXIT_FAILURE);
            }
            else
            {
                syslog(LOG_INFO, "run_bt_server() succeeded");
                exit(EXIT_SUCCESS);
            }
        }

        //Bluetooth client
        if ((child=fork()) < 0)
        {//failed fork
            syslog(LOG_ERR, "fork for run_bt() failed");
            exit(EXIT_FAILURE);
        }
        if (child)
        {//parent
            child_type[total_children] = CHILD_BT;
            child_pid[total_children++] = child;
        }
        else
        {//child
            res = run_bt_client(db, mDeviceID, &error);
            if (res)
            {
                syslog(LOG_ERR, "run_bt_client() failed due to reason: %s", error);
                exit(EXIT_FAILURE);
            }
            else
            {
                syslog(LOG_INFO, "run_bt_client() succeeded");
                exit(EXIT_SUCCESS);
            }
        }
    }
#endif

    //XWindows
    res = has_xwindows();
    if (!res)
    {//XWindows exists
        //Fork XWindows process
        if ((child=fork()) < 0)
        {//failed fork
            syslog(LOG_ERR, "fork for run_xwindows() failed");
            exit(EXIT_FAILURE);
        }
        if (child)
        {//parent
            child_type[total_children] = CHILD_XWINDOWS;
            child_pid[total_children++] = child;
        }
        else
        {//child
            res = run_xwindows(db, mDeviceID, first_run);
            if (res)
            {
                syslog(LOG_ERR, "run_xwindows() failed");
                exit(EXIT_FAILURE);
            }
            else
            {
                syslog(LOG_INFO, "run_xwindows() succeeded");
                exit(EXIT_SUCCESS);
            }
        }
    }

    if (!total_children)
    {
        syslog(LOG_ERR, "daemon has nothing configured to run");
        exit(EXIT_FAILURE);
    }

    while (1)
    {
        pid = waitpid(-1, &status, WNOHANG);
        if (pid < 0)
        {
            syslog(LOG_ERR, "daemon failed to wait for children");
            exit(EXIT_FAILURE);
        }

        if (pid)
        {
            // something happened with child 'pid', do something about it...
            // Details are in 'status', see waitpid() manpage
            for (loop = 0; loop < total_children; loop++)
            {
                if (pid == child_pid[loop])
                {
                    if (!WIFSIGNALED(status))
                    {//Bizarre error
                        syslog(LOG_NOTICE, "child process %d of type %d closed by signal", pid, child_type[loop]);
                        child_pid[loop] = 0;
                        child_type[loop] = 0;
                    }
                    else if (!WIFEXITED(status))
                    {//Bizarre error
                        syslog(LOG_ERR, "child process %d of type %d failed for unknown reasons", pid, child_type[loop]);
                        child_pid[loop] = 0;
                        child_type[loop] = 0;
                    }
                    else
                    {
                        res = WEXITSTATUS(status);
                        if (res)
                        {
                            syslog(LOG_ERR, "child process %d of type %d failed with error %d", pid, child_type[loop], res);
                            child_pid[loop] = 0;
                            child_type[loop] = 0;
                        }
                        else
                        {
                            syslog(LOG_ERR, "child process %d of type %d incorrectly closed successfully", pid, child_type[loop]);
                            child_pid[loop] = 0;
                            child_type[loop] = 0;
                        }
                    }
                    break;
                }
            }
        }
        else
        {
            //TODO: check for interrupts and kill children
        }
    }

    syslog(LOG_NOTICE,"daemon closing");
    closelog();
    return(EXIT_SUCCESS);
}