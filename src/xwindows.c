#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <unistd.h>
#include "core.h"
#include "xwindows.h"

#define BUFFER_SIZE                     256

#define CONFIG_DELAY_TIME_SECONDS           "XWND_DELAY_TIME_SECONDS"
#define CONFIG_DELAY_TIME_SECONDS_DEFAULT   "1"
#define CONFIG_DELAY_TIME_NANO              "XWND_DELAY_TIME_NANO"
#define CONFIG_DELAY_TIME_NANO_DEFAULT      "0"

const char* ATOM_NET_ACTIVE_WINDOW = "_NET_ACTIVE_WINDOW";
const char* ATOM_WM_NAME = "WM_NAME";
const char* ATOM_WM_CLASS = "WM_CLASS";

int get_top_window(char* TopWindowName, char* TopWindowClass);

int has_xwindows()
{
    Display* display;
    int screenCount;
    int res;

    display = XOpenDisplay(NULL);
    if (!display)
    {
        return -1;
    }

    screenCount = XScreenCount(display);
    if (screenCount < 1)
    {
        res = XCloseDisplay(display);

        return -1;
    }

    res = XCloseDisplay(display);
    if (res)
    {
        return -1;
    }

    return 0;
}

int get_top_window(char* TopWindowName, char* TopWindowClass)
{
    Display* display;
    Atom atomActiveWindow;
    int screenCount;
    int loop;
    Screen* screen;
    Window window;
    long length;
    Atom atomActual;
    Atom atomWMName;
    Atom atomWMClass;
    int actual_format_return;
    unsigned long nitems_return;
    unsigned long bytes_after_return;
    unsigned char* prop_return;
    Window activeWindow;
    int res;

    display = XOpenDisplay(NULL);
    if (!display)
    {
        return -1;
    }

    atomActiveWindow = XInternAtom(display, ATOM_NET_ACTIVE_WINDOW, True);
    if (atomActiveWindow == None)
    {
        goto ERROR;
    }

    atomWMName = XInternAtom(display, ATOM_WM_NAME, True);
    if (atomWMName == None)
    {
        goto ERROR;
    }

    atomWMClass = XInternAtom(display, ATOM_WM_CLASS, True);
    if (atomWMClass == None)
    {
        goto ERROR;
    }

    screenCount = XScreenCount(display);

    activeWindow = 0;
    for (loop = 0; loop < screenCount; loop++)
    {
        screen = XScreenOfDisplay(display, loop);

        window = XRootWindow(display, loop);
        if (!window)
        {
            goto ERROR;
        }

        length = sizeof(Window)/sizeof(int);
        actual_format_return = None;
        res = XGetWindowProperty(display, window, atomActiveWindow, 0, length, False, AnyPropertyType,
            &atomActual, &actual_format_return, &nitems_return, &bytes_after_return, &prop_return);
        if (actual_format_return != None)
        {
            activeWindow = *(Window*)prop_return;
            break;
        }
    }

    if (!activeWindow)
    {
        goto ERROR;
    }

    length = TOP_WINDOW_NAME_SIZE;
    actual_format_return = None;
    res = XGetWindowProperty(display, activeWindow, atomWMName, 0, length, False, AnyPropertyType,
        &atomActual, &actual_format_return, &nitems_return, &bytes_after_return, &prop_return);
    if (res || (actual_format_return == None))
    {
        goto ERROR;
    }
    strncpy(TopWindowName, (const char*)prop_return, nitems_return);

    length = TOP_WINDOW_NAME_SIZE;
    actual_format_return = None;
    res = XGetWindowProperty(display, activeWindow, atomWMClass, 0, length, False, AnyPropertyType,
        &atomActual, &actual_format_return, &nitems_return, &bytes_after_return, &prop_return);
    if (res || (actual_format_return == None))
    {
        goto ERROR;
    }
    strncpy(TopWindowClass, (const char*)prop_return, nitems_return);

    res = XCloseDisplay(display);
    if (res)
    {
        return -1;
    }

    return 0;

ERROR:
    res = XCloseDisplay(display);
    return -1;
}

int run_xwindows(sqlite3 *db, sqlite3_int64 DeviceID, int first_run)
{
    int res;
    sqlite3_int64 SensorID;
    sqlite3_stmt* statement;
    char buffer[BUFFER_SIZE];
    char topWindowName[TOP_WINDOW_NAME_SIZE + 1] = { 0 };
    char topWindowClass[TOP_WINDOW_NAME_SIZE + 1] = { 0 };
    char topWindowClassName[TOP_WINDOW_NAME_SIZE + TOP_WINDOW_NAME_SIZE + 1] = { 0 };
    int delayS;
    int delayN;

    if (first_run)
    {//First time running, so create db
        res = insert_config(db, CONFIG_DELAY_TIME_SECONDS, CONFIG_DELAY_TIME_SECONDS_DEFAULT);
        if (res)
        {
            //TODO: log error
            return -1;
        }
        delayS = atoi(CONFIG_DELAY_TIME_SECONDS_DEFAULT);

        res = insert_config(db, CONFIG_DELAY_TIME_NANO, CONFIG_DELAY_TIME_NANO_DEFAULT);
        if (res)
        {
            //TODO: log error
            return -1;
        }
        delayN = atoi(CONFIG_DELAY_TIME_NANO_DEFAULT);

        res = create_db_sensor(db, DeviceID, XWINDOWS_SENSOR, XWINDOWS_SENSOR_TYPE, XWINDOWS_OBSERVATION_TYPE, &SensorID);
        if (res)
        {
            //TODO: log error
            return -1;
        }
    }
    else
    {//Get information
        memset(buffer, 0, BUFFER_SIZE);
        res = get_config(db, CONFIG_DELAY_TIME_SECONDS, buffer, BUFFER_SIZE, CONFIG_DELAY_TIME_SECONDS_DEFAULT);
        if (res)
        {
            //TODO: log error
            return -1;
        }
        delayS = atoi(buffer);
        res = get_config(db, CONFIG_DELAY_TIME_SECONDS, buffer, BUFFER_SIZE, CONFIG_DELAY_TIME_NANO_DEFAULT);
        if (res)
        {
            //TODO: log error
            return -1;
        }
        delayN = atoi(buffer);

        res = get_db_sensor(db, DeviceID, XWINDOWS_SENSOR, &SensorID);
        if (res)
        {
            //TODO: log error
            return -1;
        }
    }

    res = prepare_log_observationstext(db, SensorID, &statement);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    while (1)
    {
        //XWindow
        res = get_top_window(topWindowName, topWindowClass);
        if (!res)
        {
            strncpy(topWindowClassName, topWindowClass, TOP_WINDOW_NAME_SIZE + TOP_WINDOW_NAME_SIZE);
            strncat(topWindowClassName, "|", TOP_WINDOW_NAME_SIZE + TOP_WINDOW_NAME_SIZE);
            strncat(topWindowClassName, topWindowName, TOP_WINDOW_NAME_SIZE + TOP_WINDOW_NAME_SIZE);

            res = log_observationstext(statement, topWindowClassName);
            if (res)
            {
                //TODO: log error
                return -1;
            }
        }

        //TODO: include delayN
        sleep(delayS);
    }

    res = close_log_observationstext(statement);
    if (res)
    {
        return -1;
    }

    return 0;
}