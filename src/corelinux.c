#include <string.h>
#include <limits.h>
#include <libconfig.h>
#include <sodium.h>
#include "corelinux.h"
#include "db.h"
#include "bluetooth.h"

//External undefined
int get_device_name_OS(char* DeviceName)
{
    return gethostname(DeviceName, HOST_NAME_MAX);
}

int create_random_universal_id_OS(universal_id* puniversal_id)
{
    randombytes_buf(puniversal_id->ID, sizeof(universal_id));

    return 0;
}

//External Bluetooth
int writebt(BluetoothSocket sock, int cbuffer_out, const char* cbuffer)
{
    size_t cbuffer_syncd;
    size_t nstreamed;

    //TODO: #31: handle mtu

    cbuffer_syncd = 0;
    while (cbuffer_syncd < cbuffer_out)
    {
        nstreamed = write(sock, cbuffer + cbuffer_syncd, cbuffer_out - cbuffer_syncd);
        if (nstreamed < 0)
        {
            return -1;
        }
        cbuffer_syncd += nstreamed;
    }

    return 0;
}

int readbt(BluetoothSocket sock, const char* cbuffer, int cbuffer_size, size_t cbuffer_needed, char tolerate_overflow, size_t* cbuffer_syncd)
{
    size_t nstreamed;

    //TODO: #31: handle mtu

    while (*cbuffer_syncd < cbuffer_needed)
    {
        nstreamed = read(sock, (void*)(cbuffer + (*cbuffer_syncd)), cbuffer_size - *cbuffer_syncd);
        if (nstreamed < 0)
        {
            return -1;
        }
        *cbuffer_syncd += nstreamed;
    }

    if ((*cbuffer_syncd > cbuffer_needed) && !tolerate_overflow)
    {
        return -1;
    }

    return 0;
}

//OS
const char* CONFIG_FILENAME_DEFAULT = "main.conf";
const char* DB_FILENAME_DEFAULT = "ubu.db";

const char* CONFIG_DBFILENAME = "dbfilename";
const char* CONFIG_DEVICEID = "deviceid";

int check_file_exists_OS(const char* filename)
{
    if(access(filename, F_OK) < 0)
    {// file doesn't exist
        return -1;
    }

    return 0;
}


int create_configfile(const char* ConfigFilename, const char* dbFilename, sqlite3_int64 DeviceID)
{
    config_t cfg;
    config_setting_t *root, *settingdbFilename, *settingDeviceID;
    int res;

    if (!ConfigFilename)
    {
        ConfigFilename = CONFIG_FILENAME_DEFAULT;
    }

    if(access(ConfigFilename, F_OK) >= 0)
    {// file exists
        return -1;
    }

    config_init(&cfg);
    root = config_root_setting(&cfg);

    settingdbFilename = config_setting_add(root, CONFIG_DBFILENAME, CONFIG_TYPE_STRING);
    if(!settingdbFilename)
    {
        config_destroy(&cfg);
        return -1;
    }

    if (!dbFilename)
    {
        dbFilename = DB_FILENAME_DEFAULT;
    }

    res = config_setting_set_string(settingdbFilename, dbFilename);
    if(res != CONFIG_TRUE)
    {
        config_destroy(&cfg);
        return -1;
    }

    settingDeviceID = config_setting_add(root, CONFIG_DEVICEID, CONFIG_TYPE_INT);
    if(!settingDeviceID)
    {
        config_destroy(&cfg);
        return -1;
    }

    res = config_setting_set_int64(settingDeviceID, DeviceID);
    if(res != CONFIG_TRUE)
    {
        config_destroy(&cfg);
        return -1;
    }

    /* Write out the new configuration. */
    res = config_write_file(&cfg, ConfigFilename);
    if(!res)
    {
        config_destroy(&cfg);
        return -1;
    }

    config_destroy(&cfg);
    return 0;
}

int check_configfile(const char* ConfigFilename, const char* dbFilename, sqlite3_int64* DeviceID)
{
    config_t cfg;
    int res;
    const char *value;

    config_init(&cfg);

    if (!ConfigFilename)
    {
        ConfigFilename = CONFIG_FILENAME_DEFAULT;
    }

    /* Read the file. If there is an error, report it and exit. */
    res = config_read_file(&cfg, ConfigFilename);
    if(!res)
    {
        config_destroy(&cfg);
        return -1;
    }

    /* Get the db filename. */
    res = config_lookup_string(&cfg, CONFIG_DEVICEID, &value);
    if (res == CONFIG_FALSE)
    {
        config_destroy(&cfg);
        return -1;
    }
    if (!dbFilename)
    {//Must be default
        res = strncmp(DB_FILENAME_DEFAULT, dbFilename, sizeof(DB_FILENAME_DEFAULT) + 1);//Compare the NULL terminator too

        if (res)
        {
            config_destroy(&cfg);
            return -1;
        }
    }
    else
    {//Can't copy to storage
        config_destroy(&cfg);
        return -1;
    }

    /* Get the DeviceID. */
    res = config_lookup_int64(&cfg, CONFIG_DEVICEID, DeviceID);
    if (res == CONFIG_FALSE)
    {
        config_destroy(&cfg);
        return -1;
    }

    config_destroy(&cfg);
    return 0;
}

//TODO: Set as compile for test only, since a running DB should not be deleted
int delete_configfile(const char* ConfigFilename)
{
    int res;

    if (!ConfigFilename)
    {
        ConfigFilename = CONFIG_FILENAME_DEFAULT;
    }

    if(access(ConfigFilename, F_OK) < 0)
    {// file doesn't exist
        return -1;
    }

    res = remove(ConfigFilename);

    return res;
}
