#ifndef _DB_H_
#define _DB_H_

#include "core.h"

#define SQL_CREATE_CONFIG \
        "CREATE TABLE Config("  \
        "Name   TEXT    NOT NULL," \
        "Value  TEXT    NOT NULL);"

#define SQL_INSERT_CONFIG_DEFAULTS \
        "INSERT INTO Config ("  \
        "Name," \
        "Value" \
        ") VALUES (" \
        "'DBVER'," \
        "'1');" \
        "INSERT INTO Config ("  \
        "Name," \
        "Value" \
        ") VALUES (" \
        "'DBCREATED'," \
        "datetime('now'));"

// #define SQL_CREATE_USER \
//         "CREATE TABLE User("  \
//         "ID             INTEGER PRIMARY KEY AUTOINCREMENT," \
//         "Name           TEXT    NOT NULL," \
//         "PublicKey      TEXT);"

#define SQL_CREATE_DEVICES \
        "CREATE TABLE Devices("  \
        "DeviceID       INTEGER PRIMARY KEY AUTOINCREMENT," \
        "UniversalID    TEXT    NOT NULL," \
        "Updated        DATETIME," \
        "DeviceType     INTEGER NOT NULL," \
        "StorageStyle   INTEGER NOT NULL," \
        "DeviceName     TEXT    NOT NULL," \
        "BtAddress      TEXT," \
        "RFCOMMChannel  INTEGER," \
        "PSM            INTEGER," \
        "I2PPublicKey   TEXT," \
        "I2PPrivateKey  TEXT);"

#define SQL_CREATE_FOREIGNDEVICES \
        "CREATE TABLE ForeignDevices("  \
        "DeviceID           INTEGER NOT NULL," \
        "ForeignDeviceID    INTEGER NOT NULL," \
        "DomesticDeviceID   INTEGER);"

#define SQL_CREATE_SENSORS \
        "CREATE TABLE Sensors("  \
        "SensorID       INTEGER     PRIMARY KEY AUTOINCREMENT," \
        "DeviceID       INTEGER     NOT NULL," \
        "SensorType     INTEGER     NOT NULL," \
        "SensorName     TEXT," \
        "ValueType      INTEGER     NOT NULL," \
        "Added          DATETIME    NOT NULL);"

#define SQL_CREATE_FOREIGNSENSORS \
        "CREATE TABLE ForeignSensors("  \
        "DeviceID           INTEGER NOT NULL," \
        "ForeignDeviceID    INTEGER NOT NULL," \
        "ForeignSensorID    INTEGER NOT NULL," \
        "DomesticSensorID   INTEGER);"

#define SQL_CREATE_OBSERVATIONS_INT \
        "CREATE TABLE ObservationsINT("  \
        "SensorID       INTEGER     NOT NULL," \
        "DateLoggedS    INTEGER     NOT NULL," \
        "DateLoggedN    INTEGER     NOT NULL," \
        "Value          INTEGER     NOT NULL);"

#define SQL_CREATE_OBSERVATIONS_REAL \
        "CREATE TABLE ObservationsREAL("  \
        "SensorID       INTEGER     NOT NULL," \
        "DateLoggedS    INTEGER     NOT NULL," \
        "DateLoggedN    INTEGER     NOT NULL," \
        "Value          REAL        NOT NULL);"

#define SQL_CREATE_OBSERVATIONS_TEXT \
        "CREATE TABLE ObservationsTEXT("  \
        "SensorID       INTEGER     NOT NULL," \
        "DateLoggedS    INTEGER     NOT NULL," \
        "DateLoggedN    INTEGER     NOT NULL," \
        "Value          TEXT        NOT NULL);"

#define SQL_CREATE_OBSERVATIONS_BLOB \
        "CREATE TABLE ObservationsBLOB("  \
        "SensorID       INTEGER     NOT NULL," \
        "DateLoggedS    INTEGER     NOT NULL," \
        "DateLoggedN    INTEGER     NOT NULL," \
        "Value          BLOB        NOT NULL);"

#define SQL_CREATE_STATUS \
        "CREATE TABLE Statuses("  \
        "StatusType         INTEGER     NOT NULL," \
        "RangeStart         DATETIME    NOT NULL," \
        "RangeEnd           DATETIME    NOT NULL," \
        "Value              TEXT        NOT NULL);"

#define SQL_CREATE_ALGORITHMINSTANCES \
        "CREATE TABLE AlgorithmInstances("  \
        "AlgorithmInstanceID    INTEGER     BLOB," \
        "AlgorithmID            INTEGER     BLOB," \
        "AlgorithmType          INTEGER," \
        "AlgorithmVersion       INTEGER," \
        "ResultType             INTEGER);"

#define SQL_CREATE_ALGORITHMINSTANCEALGORITHMS \
        "CREATE TABLE AlgorithmInstanceAlgorithms("  \
        "IndependentAlgorithmInstanceID BLOB    NOT NULL," \
        "DependentAlgorithmInstanceID   BLOB    NOT NULL);"

#define SQL_CREATE_ALGORITHMINSTANCESENSORS \
        "CREATE TABLE AlgorithmInstanceSensors("  \
        "AlgorithmInstanceID    BLOB    NOT NULL," \
        "SensorID               INTEGER NOT NULL);"

#define SQL_CREATE_RESULTS \
        "CREATE TABLE Results("  \
        "AlgorithmInstanceID    BLOB        NOT NULL," \
        "RangeStart             DATETIME," \
        "RangeEnd               DATETIME," \
        "Value                  BLOB);"

#define SQL_CREATE_SYNCH \
        "CREATE TABLE Synch("  \
        "DeviceIDOther  INTEGER     NOT NULL," \
        "DateStarted    DATETIME    NOT NULL," \
        "SynchType      INTEGER     NOT NULL," \
        "I2PState       INTEGER," \
        "Cache          BLOB);"
        //"DeviceIDSelf   INTEGER     NOT NULL," \

#define SQL_INDEX_DEVICES_UNIVERSALID \
        "CREATE UNIQUE INDEX IDX_Devices_UniversalID ON Devices (UniversalID);"

#define SQL_INDEX_DEVICES_DEVICENAME \
        "CREATE UNIQUE INDEX IDX_Devices_DeviceName ON Devices (DeviceName);"

// #define SQL_INDEX_DEVICES_I2PPUBLICKEY \
//         "CREATE UNIQUE INDEX IDX_Devices_I2PPublicKey ON Devices (I2PPublicKey);"

#define SQL_INDEX_SENSORS_DEVICEID \
        "CREATE INDEX IDX_Sensors_DeviceID ON Sensors (DeviceID);"

#define SQL_INDEX_SENSORS_SENSORID_DEVICEID \
        "CREATE UNIQUE INDEX IDX_Sensors_SensorID_DeviceID ON Sensors (SensorID, DeviceID);"

#define SQL_INDEX_FOREIGNDEVICES_DEVICEID_FOREIGNDEVICEID \
        "CREATE UNIQUE INDEX IDX_ForeignDevices_DeviceID_ForeignDeviceID ON ForeignDevices (DeviceID, ForeignDeviceID);"

#define SQL_INDEX_FOREIGNDEVICES_DEVICEID_DOMESTICDEVICEID \
        "CREATE UNIQUE INDEX IDX_ForeignDevices_DeviceID_DomesticDeviceID ON ForeignDevices (DeviceID, DomesticDeviceID);"

#define SQL_INDEX_FOREIGNSENSORS_DEVICEID_FOREIGNDEVICEID_FOREIGNSENSORID \
        "CREATE UNIQUE INDEX IDX_ForeignSensors_DeviceID_ForeignDeviceID_ForeignSensorID ON ForeignSensors (DeviceID, ForeignDeviceID, ForeignSensorID);"

#define SQL_INDEX_FOREIGNSENSORS_DEVICEID_FOREIGNDEVICEID_DOMESTICSENSORID \
        "CREATE UNIQUE INDEX IDX_ForeignSensors_DeviceID_ForeignDeviceID_DomesticDeviceID ON ForeignSensors (DeviceID, ForeignDeviceID, DomesticSensorID);"

#define SQL_INDEX_FOREIGNSENSORS_DEVICEID_FOREIGNSENSORID \
        "CREATE UNIQUE INDEX IDX_ForeignSensors_DeviceID_ForeignSensorID ON ForeignSensors (DeviceID, ForeignSensorID);"

#define SQL_INDEX_SYNCH_DEVICEIDOTHER \
        "CREATE UNIQUE INDEX IDX_Synch_DeviceIDOther ON Synch (DeviceIDOther);"

#define SQL_INDEX_OBSERVATIONSINT_SENSORID_DEVICEID \
        "CREATE UNIQUE INDEX IDX_ObservationsINT_SensorID_DeviceID ON ObservationsINT (SensorID, DateLoggedS, DateLoggedN);"

#define SQL_INDEX_OBSERVATIONSREAL_SENSORID_DEVICEID \
        "CREATE UNIQUE INDEX IDX_ObservationsREAL_SensorID_DeviceID ON ObservationsREAL (SensorID, DateLoggedS, DateLoggedN);"

#define SQL_INDEX_OBSERVATIONSTEXT_SENSORID_DEVICEID \
        "CREATE UNIQUE INDEX IDX_ObservationsTEXT_SensorID_DeviceID ON ObservationsTEXT (SensorID, DateLoggedS, DateLoggedN);"

#define SQL_INDEX_OBSERVATIONSBLOB_SENSORID_DEVICEID \
        "CREATE UNIQUE INDEX IDX_ObservationsBLOB_SensorID_DeviceID ON ObservationsBLOB (SensorID, DateLoggedS, DateLoggedN);"

#define SQL_INDEX_ALGORITHMINSTANCEALGORITHMS \
        "CREATE UNIQUE INDEX IDX_AlgorithmInstanceAlgorithms ON AlgorithmInstanceAlgorithms (IndependentAlgorithmInstanceID, DependentAlgorithmInstanceID);"

#define SQL_INDEX_ALGORITHMINSTANCESENSORS_ALGORITHMINSTANCEID_SENSORID \
        "CREATE UNIQUE INDEX IDX_AlgorithmInstanceSensors_AlgorithmInstanceID_SensorID ON AlgorithmInstanceSensors (AlgorithmInstanceID, SensorID);"

#define SQL_INDEX_RESULTS_RANGESTART \
        "CREATE INDEX IDX_Results_RangeStart ON Results (RangeStart);"

#define SQL_INDEX_RESULTS_RANGEEND \
        "CREATE INDEX IDX_Results_RangeEnd ON Results (RangeEnd);"

#define SQL_INDEX_RESULTS_RANGESTART_RANGEEND \
        "CREATE INDEX IDX_Results_RangeStart_RangeEnd ON Results (RangeStart, RangeEnd);"


static const char* SQL_CREATE_TABLES[] = {
    SQL_CREATE_CONFIG,
    SQL_INSERT_CONFIG_DEFAULTS,
    // SQL_CREATE_USER,
    SQL_CREATE_DEVICES,
    SQL_CREATE_FOREIGNDEVICES,
    SQL_CREATE_SENSORS,
    SQL_CREATE_FOREIGNSENSORS,
    SQL_CREATE_OBSERVATIONS_INT,
    SQL_CREATE_OBSERVATIONS_REAL,
    SQL_CREATE_OBSERVATIONS_TEXT,
    SQL_CREATE_OBSERVATIONS_BLOB,
    SQL_CREATE_STATUS,
    SQL_CREATE_ALGORITHMINSTANCES,
    SQL_CREATE_ALGORITHMINSTANCEALGORITHMS,
    SQL_CREATE_ALGORITHMINSTANCESENSORS,
    SQL_CREATE_RESULTS,
    SQL_CREATE_SYNCH,
    SQL_INDEX_DEVICES_DEVICENAME,
    SQL_INDEX_DEVICES_UNIVERSALID,
    // SQL_INDEX_DEVICES_I2PPUBLICKEY,
    SQL_INDEX_SENSORS_DEVICEID,
    SQL_INDEX_SENSORS_SENSORID_DEVICEID,
    SQL_INDEX_FOREIGNDEVICES_DEVICEID_FOREIGNDEVICEID,
    SQL_INDEX_FOREIGNDEVICES_DEVICEID_DOMESTICDEVICEID,
    SQL_INDEX_FOREIGNSENSORS_DEVICEID_FOREIGNDEVICEID_FOREIGNSENSORID,
    SQL_INDEX_FOREIGNSENSORS_DEVICEID_FOREIGNDEVICEID_DOMESTICSENSORID,
    SQL_INDEX_FOREIGNSENSORS_DEVICEID_FOREIGNSENSORID,
    SQL_INDEX_SYNCH_DEVICEIDOTHER,
    SQL_INDEX_OBSERVATIONSINT_SENSORID_DEVICEID,
    SQL_INDEX_OBSERVATIONSREAL_SENSORID_DEVICEID,
    SQL_INDEX_OBSERVATIONSTEXT_SENSORID_DEVICEID,
    SQL_INDEX_OBSERVATIONSBLOB_SENSORID_DEVICEID,
    SQL_INDEX_ALGORITHMINSTANCEALGORITHMS,
    SQL_INDEX_ALGORITHMINSTANCESENSORS_ALGORITHMINSTANCEID_SENSORID,
    SQL_INDEX_RESULTS_RANGESTART,
    SQL_INDEX_RESULTS_RANGEEND,
    SQL_INDEX_RESULTS_RANGESTART_RANGEEND,
    NULL };

#define SQL_INSERT_CONFIG \
        "INSERT INTO Config ("  \
        "Name," \
        "Value" \
        ") VALUES (" \
        "?," \
        "?);"

#define SQL_QUERY_CONFIG_BY_NAME \
        "SELECT Value FROM Config WHERE Name=?"

#define SQL_INSERT_DEVICES_NEW \
        "INSERT INTO Devices ("  \
        "UniversalID," \
        "Updated," \
        "DeviceType," \
        "StorageStyle," \
        "DeviceName," \
        "BtAddress," \
        "RFCOMMChannel," \
        "PSM," \
        "I2PPublicKey," \
        "I2PPrivateKey" \
        ") VALUES (" \
        "?," \
        "datetime('now')," \
        "?," \
        "?," \
        "?," \
        "?," \
        "?," \
        "?," \
        "?," \
        "?);"

#define SQL_INSERT_DEVICES_FULL \
        "INSERT INTO Devices ("  \
        "UniversalID," \
        "Updated," \
        "DeviceType," \
        "StorageStyle," \
        "DeviceName," \
        "BtAddress," \
        "RFCOMMChannel," \
        "PSM," \
        "I2PPublicKey," \
        "I2PPrivateKey" \
        ") VALUES (" \
        "?," \
        "?," \
        "?," \
        "?," \
        "?," \
        "?," \
        "?," \
        "?," \
        "?," \
        "?);"

#define SQL_INSERT_DEVICES_AUTHORIZED \
        "INSERT INTO Devices ("  \
        "UniversalID," \
        "Updated," \
        "DeviceType," \
        "StorageStyle," \
        "DeviceName," \
        "BtAddress," \
        "RFCOMMChannel," \
        "PSM," \
        "I2PPublicKey," \
        "I2PPrivateKey" \
        ") VALUES (" \
        "?," \
        "NULL," \
        "?," \
        "?," \
        "?," \
        "?," \
        "?," \
        "?," \
        "?," \
        "?);"

#define SQL_UPDATE_DEVICES_FOR_I2P \
        "UPDATE Devices SET I2PPublicKey=?,I2PPrivateKey=? WHERE DeviceID=?;"

#define SQL_UPDATE_DEVICES \
        "UPDATE Devices SET UniversalID=?,Updated=?,DeviceType=?,StorageStyle=?,DeviceName=?,BtAddress=?,RFCOMMChannel=?,PSM=?,I2PPublicKey=?,I2PPrivateKey=? WHERE DeviceID=?;"

#define SQL_QUERY_DEVICEID_BY_DEVICENAME \
        "SELECT DeviceID,Updated FROM Devices WHERE DeviceName=?"

#define SQL_QUERY_UNIVERSALID_BY_DEVICEID \
        "SELECT UniversalID FROM Devices WHERE DeviceID=?"

#define SQL_QUERY_DEVICENAME_BY_DEVICEID \
        "SELECT DeviceName FROM Devices WHERE DeviceID=?"

#define SQL_QUERY_I2PPUBLICKEY_BY_DEVICEID \
        "SELECT I2PPublicKey FROM Devices WHERE DeviceID=?"

#define SQL_QUERY_I2PPRIVATEKEY_BY_DEVICEID \
        "SELECT I2PPrivateKey FROM Devices WHERE DeviceID=?"

#define SQL_QUERY_DEVICEID_BY_DEVICENAME_BTADDRESS \
        "SELECT DeviceID FROM Devices WHERE DeviceName=? AND BtAddress=?"

#define SQL_QUERY_DEVICEID_I2PPUBLICKEY \
        "SELECT DeviceID,I2PPublicKey FROM Devices WHERE DeviceID!=?"

#define SQL_QUERY_DEVICECOUNT_BY_DEVICEID \
        "SELECT COUNT(*) FROM Devices WHERE DeviceID=?"

#define SQL_QUERY_DEVICEID \
        "SELECT DeviceID FROM Devices"

#define SQL_QUERY_DEVICES_BY_DEVICEID \
        "SELECT UniversalID,DeviceType,StorageStyle,DeviceName,BtAddress,RFCOMMChannel,PSM,I2PPublicKey,Updated FROM Devices WHERE DeviceID=?"

#define SQL_QUERY_DEVICES_FOR_BTADDRESS \
        "SELECT DeviceID,BtAddress,RFCOMMChannel,PSM FROM Devices WHERE DeviceID!=? AND BtAddress IS NOT NULL"

// #define SQL_QUERY_DEVICES_FOR_I2PPUBLICKEY \
//         "SELECT DeviceID,I2PPublicKey FROM Devices WHERE DeviceName!=? AND I2PPublicKey IS NOT NULL"

#define SQL_INSERT_SENSORS \
        "INSERT INTO Sensors ("  \
        "DeviceID," \
        "SensorType," \
        "SensorName," \
        "ValueType," \
        "Added" \
        ") VALUES (" \
        "?," \
        "?," \
        "?," \
        "?," \
        "datetime('now'));"

#define SQL_QUERY_SENSORID_BY_DEVICEID \
        "SELECT SensorID FROM Sensors WHERE DeviceID=?"

#define SQL_QUERY_SENSORS_BY_DEVICEID \
        "SELECT SensorID,SensorType,SensorName,ValueType,Added FROM Sensors WHERE DeviceID=?"

#define SQL_QUERY_SENSORS_BY_SENSORID \
        "SELECT SensorType,SensorName,ValueType,Added FROM Sensors WHERE SensorID=?"

#define SQL_QUERY_SENSORID_BY_DEVICEID_SENSORNAME \
        "SELECT SensorID FROM Sensors WHERE DeviceID=? AND SensorName=?"

#define SQL_INSERT_ALGORITHMINSTANCES \
        "INSERT INTO AlgorithmInstances ("  \
        "AlgorithmInstanceID," \
        "AlgorithmID," \
        "AlgorithmType," \
        "AlgorithmVersion," \
        "ResultType" \
        ") VALUES (" \
        "?," \
        "?," \
        "?," \
        "?," \
        "?);"

#define SQL_INSERT_UNMAPPED_ALGORITHMINSTANCES \
        "INSERT INTO AlgorithmInstances ("  \
        "AlgorithmInstanceID" \
        ") VALUES (" \
        "?);"

#define SQL_QUERY_ALGORITHMINSTANCEID \
        "SELECT AlgorithmInstanceID FROM AlgorithmInstances"

#define SQL_QUERY_ALGORITHMINSTANCES_BY_ALGORITHMINSTANCEID \
        "SELECT AlgorithmID,AlgorithmType,AlgorithmVersion,ResultType FROM AlgorithmInstances WHERE AlgorithmInstanceID=?"

#define SQL_QUERY_ALGORITHMINSTANCESENSORS_BY_ALGORITHMINSTANCEID \
        "SELECT SensorID FROM AlgorithmInstanceSensors WHERE AlgorithmInstanceID=?"

#define SQL_QUERY_FOREIGNDEVICES_BY_DEVICEID_FOREIGNDEVICEID \
        "SELECT DomesticDeviceID FROM ForeignDevices WHERE DeviceID=? AND ForeignDeviceID=?"

#define SQL_QUERY_UNMAPPED_FOREIGNDEVICES_BY_DEVICEID \
        "SELECT ForeignDeviceID FROM ForeignDevices WHERE DeviceID=? AND DomesticDeviceID IS NULL"

#define SQL_INSERT_FOREIGNDEVICES \
        "INSERT INTO ForeignDevices ("  \
        "DeviceID," \
        "ForeignDeviceID," \
        "DomesticDeviceID" \
        ") VALUES (" \
        "?," \
        "?," \
        "?);"

#define SQL_UPDATE_FOREIGNDEVICES \
        "UPDATE ForeignDevices SET DomesticDeviceID=? WHERE DeviceID=? AND ForeignDeviceID=?;"

#define SQL_INSERT_UNMAPPED_FOREIGNDEVICES_BY_DEVICEID_FOREIGNDEVICEID \
        "INSERT INTO ForeignDevices ("  \
        "DeviceID," \
        "ForeignDeviceID," \
        "DomesticDeviceID" \
        ") VALUES (" \
        "?," \
        "?," \
        "NULL);"

#define SQL_QUERY_FOREIGNSENSORS_BY_DEVICEID_FOREIGNDEVICEID_FOREIGNSENSORID \
        "SELECT DomesticSensorID FROM ForeignSensors WHERE DeviceID=? AND ForeignDeviceID=? AND ForeignSensorID=?"

#define SQL_QUERY_FOREIGNSENSORS_BY_DEVICEID_FOREIGNSENSORID \
        "SELECT DomesticSensorID FROM ForeignSensors WHERE DeviceID=? AND ForeignSensorID=?"

#define SQL_QUERY_UNMAPPED_FOREIGNSENSORS_BY_DEVICEID \
        "SELECT ForeignDeviceID,ForeignSensorID FROM ForeignSensors WHERE DeviceID=? AND DomesticSensorID IS NULL"

#define SQL_INSERT_FOREIGNSENSORS \
        "INSERT INTO ForeignSensors ("  \
        "DeviceID," \
        "ForeignDeviceID," \
        "ForeignSensorID," \
        "DomesticSensorID" \
        ") VALUES (" \
        "?," \
        "?," \
        "?," \
        "?);"

#define SQL_INSERT_UNMAPPED_FOREIGNSENSORS_BY_DEVICEID_FOREIGNDEVICEID_FOREIGNSENSORID \
        "INSERT INTO ForeignSensors ("  \
        "DeviceID," \
        "ForeignDeviceID," \
        "ForeignSensorID," \
        "DomesticSensorID" \
        ") VALUES (" \
        "?," \
        "?," \
        "?," \
        "NULL);"

#define SQL_UPDATE_FOREIGNSENSORS \
        "UPDATE ForeignSensors SET DomesticSensorID=? WHERE DeviceID=? AND ForeignDeviceID=? AND ForeignSensorID=?;"

#define SQL_QUERY_VALUETYPE_BY_SENSORID \
        "SELECT ValueType FROM Sensors WHERE SensorID=?"

#define SQL_INSERT_OBSERVATIONSINT \
        "INSERT INTO ObservationsINT ("  \
        "SensorID," \
        "DateLoggedS," \
        "DateLoggedN," \
        "Value" \
        ") VALUES (" \
        "?," \
        "?," \
        "?," \
        "?);"

#define SQL_QUERY_OBSERVATIONSINT_BY_SENSORID_DATELOGGED \
        "SELECT DateLoggedS,DateLoggedN,Value FROM ObservationsINT WHERE SensorID=? AND (DateLoggedS>? OR (DateLoggedS=? AND DateLoggedN>?)) ORDER BY DateLoggedS,DateLoggedN"

#define SQL_QUERY_LAST_OBSERVATIONSINT_BY_SENSORID \
        "SELECT DateLoggedS,DateLoggedN FROM ObservationsINT WHERE SensorID=? AND DateLoggedS=(SELECT MAX(DateLoggedS) FROM ObservationsINT WHERE SensorID=?) ORDER BY DateLoggedN DESC"

#define SQL_INSERT_OBSERVATIONSREAL \
        "INSERT INTO ObservationsREAL ("  \
        "SensorID," \
        "DateLoggedS," \
        "DateLoggedN," \
        "Value" \
        ") VALUES (" \
        "?," \
        "?," \
        "?," \
        "?);"

#define SQL_QUERY_OBSERVATIONSREAL_BY_SENSORID_DATELOGGED \
        "SELECT DateLoggedS,DateLoggedN,Value FROM ObservationsREAL WHERE SensorID=? AND (DateLoggedS>? OR (DateLoggedS=? AND DateLoggedN>?)) ORDER BY DateLoggedS,DateLoggedN"

#define SQL_QUERY_LAST_OBSERVATIONSREAL_BY_SENSORID \
        "SELECT DateLoggedS,DateLoggedN FROM ObservationsREAL WHERE SensorID=? AND DateLoggedS=(SELECT MAX(DateLoggedS) FROM ObservationsREAL WHERE SensorID=?) ORDER BY DateLoggedN DESC"

#define SQL_INSERT_OBSERVATIONSTEXT \
        "INSERT INTO ObservationsTEXT ("  \
        "SensorID," \
        "DateLoggedS," \
        "DateLoggedN," \
        "Value" \
        ") VALUES (" \
        "?," \
        "?," \
        "?," \
        "?);"

#define SQL_QUERY_OBSERVATIONSTEXT_BY_SENSORID_DATELOGGED \
        "SELECT DateLoggedS,DateLoggedN,Value FROM ObservationsTEXT WHERE SensorID=? AND (DateLoggedS>? OR (DateLoggedS=? AND DateLoggedN>?)) ORDER BY DateLoggedS,DateLoggedN"

#define SQL_QUERY_LAST_OBSERVATIONSTEXT_BY_SENSORID \
        "SELECT DateLoggedS,DateLoggedN FROM ObservationsTEXT WHERE SensorID=? AND DateLoggedS=(SELECT MAX(DateLoggedS) FROM ObservationsTEXT WHERE SensorID=?) ORDER BY DateLoggedN DESC"

#define SQL_QUERY_FOREIGN_SENSORS_FOR_UPDATES \
        "SELECT Sensors.SensorType,Sensors.ValueType,ForeignSensors.ForeignSensorID,ForeignSensors.DomesticSensorID FROM ForeignSensors INNER JOIN Sensors ON Sensors.SensorID=ForeignSensors.DomesticSensorID WHERE (ForeignSensors.DeviceID=?) AND (Sensors.DeviceID!=?)"

#define SQL_INSERT_OBSERVATIONSBLOB \
        "INSERT INTO ObservationsBLOB ("  \
        "SensorID," \
        "DateLoggedS," \
        "DateLoggedN," \
        "Value" \
        ") VALUES (" \
        "?," \
        "?," \
        "?," \
        "?);"

#define SQL_QUERY_OBSERVATIONSBLOB_BY_SENSORID_DATELOGGED \
        "SELECT DateLoggedS,DateLoggedN,Value FROM ObservationsBLOB WHERE SensorID=? AND (DateLoggedS>? OR (DateLoggedS=? AND DateLoggedN>?)) ORDER BY DateLoggedS,DateLoggedN"

#define SQL_QUERY_LAST_OBSERVATIONSBLOB_BY_SENSORID \
        "SELECT DateLoggedS,DateLoggedN FROM ObservationsBLOB WHERE SensorID=? AND DateLoggedS=(SELECT MAX(DateLoggedS) FROM ObservationsBLOB WHERE SensorID=?) ORDER BY DateLoggedN DESC"

#define SQL_QUERY_UNMAPPED_ALGORITHMINSTANCES_BY_ALGORITHMINSTANCEID \
        "SELECT AlgorithmType FROM AlgorithmInstances WHERE AlgorithmInstanceID=?"

#define SQL_QUERY_UNMAPPED_ALGORITHMINSTANCES \
        "SELECT AlgorithmInstanceID FROM AlgorithmInstances WHERE AlgorithmType IS NULL"

#define SQL_INSERT_ALGORITHMINSTANCESENSORS \
        "INSERT INTO AlgorithmInstanceSensors ("  \
        "AlgorithmInstanceID,"  \
        "SensorID" \
        ") VALUES (" \
        "?," \
        "?);"

#endif
