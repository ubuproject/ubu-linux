#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <json.h>
#include "core.h"
#include "db.h"
#include "jsonobjects.h"

#ifdef USE_BASE_85
#include "ascii85.h"
#endif

//Helper functions
int upsert_domesticdevice_by_json_device(sqlite3 *db, json_object *device_devices_device, sqlite3_int64 oDeviceID, sqlite3_int64 ForeignDeviceID, sqlite3_int64* DomesticDeviceID);
int insert_device_by_json_device(sqlite3 *db, sqlite3_int64 oDeviceID, json_object *device_devices_device, sqlite3_int64* DeviceID);
int update_device_by_json_device(sqlite3 *db, sqlite3_int64 DeviceID, json_object *device_devices_device);


int NOW(struct timespec *tp)
{
    int res;

    res = clock_gettime(CLOCK_REALTIME, tp);
    if (res)
    {
        return -1;
    }

    return 0;
}
int NOW_plus_seconds(unsigned int seconds, struct timespec *tp)
{
    int res;

    res = NOW(tp);
    if (res)
    {
        return -1;
    }

    tp->tv_sec += seconds;

    return 0;
}
int tmcompare(struct timespec *tpleft, struct timespec *tpright)
{
    if ((tpleft == NULL) && (tpright == NULL))
    {
        return 0;
    }
    else if ((tpleft) && (tpright == NULL))
    {
        return -1;
    }
    else if ((tpleft == NULL) && (tpright))
    {
        return 1;
    }
    else if (tpleft->tv_sec > tpright->tv_sec)
    {
        return -1;
    }
    else if (tpleft->tv_sec < tpright->tv_sec)
    {
        return 1;
    }
    else if (tpleft->tv_nsec > tpright->tv_nsec)
    {
        return -1;
    }
    else if (tpleft->tv_nsec < tpright->tv_nsec)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int connect_to_db(const char* dbFilename, sqlite3_int64 DeviceID, sqlite3 **db)
{
    char *zErrMsg = 0;
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    sqlite3_int64 devicecount;

    if (!dbFilename)
    {
        dbFilename = DB_FILENAME_DEFAULT;
    }

    res = check_file_exists_OS(dbFilename);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    res = sqlite3_open(dbFilename, db);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    res = sqlite3_prepare_v3(*db, SQL_QUERY_DEVICECOUNT_BY_DEVICEID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }
    //DeviceID = DeviceID
    res = sqlite3_bind_int64(statement, 1, DeviceID);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_ROW)//if (res == SQLITE_DONE)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    devicecount = sqlite3_column_int64(statement, 0);
    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    return (devicecount != 1);

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    res = sqlite3_close(*db);
    return -1;
}

int close_db(sqlite3* db)
{
    int res;

    res = sqlite3_close(db);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    return 0;
}

static int sqlite3_exec_error_callback(void *NotUsed, int argc, char **argv, char **azColName)
{
    int i;
    for(i = 0; i<argc; i++)
    {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    return 0;
}

int create_db(const char* dbFilename, const char* ForceDevicename, sqlite3 **db, sqlite3_int64* DeviceID)
{
    int res;
    char devicename[DEVICE_NAME_SIZE + 1];
    universal_id _universal_id_device;
#ifdef USE_BASE_85
    universal_id_base85 _universal_id_base85_device;
#endif
#ifdef USE_BASE_64
    universal_id_base64 _universal_id_base64_device;
#endif

    char bt_address[1024] = { 0 };
    char *zErrMsg = 0;
    int loop;
    const char *sql;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;

    if (!dbFilename)
    {
        dbFilename = DB_FILENAME_DEFAULT;
    }

    //Check that file doesn't exist
    res = check_file_exists_OS(dbFilename);
    if (!res)
    {
        //TODO: log error
        return -1;
    }

    if (ForceDevicename)
    {
        strncpy(devicename, ForceDevicename, DEVICE_NAME_SIZE);
    }
    else
    {
        //Get device information
        res = get_device_name_OS(devicename);
        if (res)
        {
            //TODO: log error
            return -1;
        }
    }

    //Create random Universal ID
    res = create_random_universal_id_OS(&_universal_id_device);
    if (res)
    {
        //TODO: log error
        return -1;
    }
#ifdef USE_BASE_85
    encode_85(_universal_id_base85_device.ID_base85, (const unsigned char *)_universal_id_device.ID, sizeof(_universal_id_device.ID));
#endif
#ifdef USE_BASE_64
    encode_64(_universal_id_base64_device.ID_base64, _universal_id_device.ID, sizeof(_universal_id_device.ID));
#endif

    //Create db
    res = sqlite3_open(dbFilename, db);
    if (res)
    {
        //TODO: log error
        fprintf(stderr, "Can't create database: %s\n", sqlite3_errmsg(*db));
        return -1;
    }

    //Create tables and indeces
    for (loop = 0; ; loop++)
    {
        /* Create SQL statement */
        sql = SQL_CREATE_TABLES[loop];

        if (sql == NULL)
        {
            break;
        }

        /* Execute SQL statement */
        res = sqlite3_exec(*db, sql, sqlite3_exec_error_callback, 0, &zErrMsg);
        if (res != SQLITE_OK)
        {
            //TODO: log error
            fprintf(stderr, "SQL error: %s\n", zErrMsg);
            sqlite3_free(zErrMsg);
            goto ERROR;
        }
    }

    //Insert self information: Devices
    const unsigned short DeviceType = 1;
    const unsigned short StorageStyle = 1;
    char BtAddress[19];
    int hasNoBT;

#ifdef WITH_BLUETOOTH
    hasNoBT = get_bt_address(BtAddress);
#else
    hasNoBT = 1;
#endif

    res = sqlite3_prepare_v3(*db, SQL_INSERT_DEVICES_NEW, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }
    //UniversalID
#ifdef USE_BASE_85
    res = sqlite3_bind_text(statement, 1, _universal_id_base85_device.ID_base85, UNIVERSAL_ID_BASE_85_SIZE_BYTES, SQLITE_STATIC);
#endif
#ifdef USE_BASE_64
    res = sqlite3_bind_text(statement, 1, _universal_id_base64_device.ID_base64, UNIVERSAL_ID_BASE_64_SIZE_BYTES, SQLITE_STATIC);
#endif
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    //DeviceType = 1
    res = sqlite3_bind_int(statement, 2, DeviceType);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    //StorageStyle = 1
    res = sqlite3_bind_int(statement, 3, StorageStyle);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    //DeviceName
    res = sqlite3_bind_text(statement, 4, devicename, -1, SQLITE_STATIC);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    //BtAddress
    if (!hasNoBT)
    {
        res = sqlite3_bind_text(statement, 5, BtAddress, -1, SQLITE_STATIC);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
        //RFCOMMChannel
        res = sqlite3_bind_null(statement, 6);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
        //PSM
        res = sqlite3_bind_null(statement, 7);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
    }
    else
    {
        res = sqlite3_bind_null(statement, 5);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
        //RFCOMMChannel
        res = sqlite3_bind_null(statement, 6);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
        //PSM
        res = sqlite3_bind_null(statement, 7);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
    }
    //I2PPublicAddress=NULL
    res = sqlite3_bind_null(statement, 8);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    //I2PPrivateAddress=NULL
    res = sqlite3_bind_null(statement, 9);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    *DeviceID = sqlite3_last_insert_rowid(*db);
    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

#ifdef WITH_I2P
    //I2PPublicAddress,I2PPrivateAddress
    res = create_i2p_keys(*db, *DeviceID);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }
#else
    //Do nothing
#endif

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    res = sqlite3_close(*db);
    res = remove(dbFilename);
    return -1;
}

//TODO: Set as compile for test only, since a running DB should not be deleted
int delete_db(const char* dbFilename, sqlite3 *db)
{
    int res;

    res = sqlite3_close(db);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    if (!dbFilename)
    {
        dbFilename = DB_FILENAME_DEFAULT;
    }

    res = remove(dbFilename);
    if (res)
    {
        //TODO: log error
        return -1;
    }

    return 0;
}

int create_db_sensor(sqlite3 *db, sqlite3_int64 DeviceID, const char* SensorName, int SensorType, int ValueType, sqlite3_int64* SensorID)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    struct timespec* tp;

    //Insert self information: Sensors
    res = sqlite3_prepare_v3(db, SQL_INSERT_SENSORS, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //DeviceID = DeviceID
    res = sqlite3_bind_int64(statement, 1, DeviceID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //SensorType = SensorType
    res = sqlite3_bind_int(statement, 2, SensorType);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //SensorName = SensorName
    res = sqlite3_bind_text(statement, 3, SensorName, -1, SQLITE_STATIC);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //ValueType = ValueType
    res = sqlite3_bind_int(statement, 4, ValueType);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        goto ERROR_STATEMENT;
    }
    *SensorID = sqlite3_last_insert_rowid(db);
    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int get_db_sensor(sqlite3 *db, sqlite3_int64 DeviceID, const char* SensorName, sqlite3_int64* SensorID)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;

    res = sqlite3_prepare_v3(db, SQL_QUERY_SENSORID_BY_DEVICEID_SENSORNAME, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //DeviceID = DeviceID
    res = sqlite3_bind_int64(statement, 1, DeviceID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //SensorName = SensorName
    res = sqlite3_bind_text(statement, 2, SensorName, -1, SQLITE_STATIC);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_ROW)//if (res == SQLITE_DONE)
    {
        goto ERROR_STATEMENT;
    }
    *SensorID = sqlite3_column_int64(statement, 0);
    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int prepare_log_observationsint32(sqlite3 *db, sqlite3_int64 SensorID, sqlite3_stmt** statement)
{
    int res;
    const char* sqlBufferUnused;

    res = sqlite3_prepare_v3(db, SQL_INSERT_OBSERVATIONSINT, -1, 0, statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //SensorID = SensorID
    res = sqlite3_bind_int64(*statement, 1, SensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(*statement);
ERROR:
    return -1;
}

int close_log_observationsint32(sqlite3_stmt* statement)
{
    int res;

    res = sqlite3_finalize(statement);

    return res;
}

int prepare_log_observationsint64(sqlite3 *db, sqlite3_int64 SensorID, sqlite3_stmt** statement)
{
    int res;
    const char* sqlBufferUnused;

    res = sqlite3_prepare_v3(db, SQL_INSERT_OBSERVATIONSINT, -1, 0, statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //SensorID = SensorID
    res = sqlite3_bind_int64(*statement, 1, SensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(*statement);
ERROR:
    return -1;
}

int close_log_observationsint64(sqlite3_stmt* statement)
{
    int res;

    res = sqlite3_finalize(statement);

    return res;
}

int prepare_log_observationsreal(sqlite3 *db, sqlite3_int64 SensorID, sqlite3_stmt** statement)
{
    int res;
    const char* sqlBufferUnused;

    res = sqlite3_prepare_v3(db, SQL_INSERT_OBSERVATIONSREAL, -1, 0, statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //SensorID = SensorID
    res = sqlite3_bind_int64(*statement, 1, SensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(*statement);
ERROR:
    return -1;
}

int close_log_observationsreal(sqlite3_stmt* statement)
{
    int res;

    res = sqlite3_finalize(statement);

    return res;
}

int prepare_log_observationstext(sqlite3 *db, sqlite3_int64 SensorID, sqlite3_stmt** statement)
{
    int res;
    const char* sqlBufferUnused;

    res = sqlite3_prepare_v3(db, SQL_INSERT_OBSERVATIONSTEXT, -1, 0, statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //SensorID = SensorID
    res = sqlite3_bind_int64(*statement, 1, SensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(*statement);
ERROR:
    return -1;
}

int close_log_observationstext(sqlite3_stmt* statement)
{
    int res;

    res = sqlite3_finalize(statement);

    return res;
}

int prepare_log_observationsblob(sqlite3 *db, sqlite3_int64 SensorID, sqlite3_stmt** statement)
{
    int res;
    const char* sqlBufferUnused;

    res = sqlite3_prepare_v3(db, SQL_INSERT_OBSERVATIONSBLOB, -1, 0, statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //SensorID = SensorID
    res = sqlite3_bind_int64(*statement, 1, SensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(*statement);
ERROR:
    return -1;
}

int close_log_observationsblob(sqlite3_stmt* statement)
{
    int res;

    res = sqlite3_finalize(statement);

    return res;
}

int get_device_name(sqlite3 *db, sqlite3_int64 DeviceID, char* DeviceName)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    const unsigned char* pDeviceName;

    res = sqlite3_prepare_v3(db, SQL_QUERY_DEVICENAME_BY_DEVICEID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //DeviceID = DeviceID
    res = sqlite3_bind_int64(statement, 1, DeviceID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_ROW)//if (res == SQLITE_DONE)
    {
        goto ERROR_STATEMENT;
    }
    pDeviceName = sqlite3_column_text(statement, 0);
    if (!pDeviceName)
    {
        goto ERROR_STATEMENT;
    }
    memset(DeviceName, 0, sqlite3_column_bytes(statement, 0) + 1);
    strncpy(DeviceName, (const char*)pDeviceName, sqlite3_column_bytes(statement, 0));
    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int get_device_universal_id(sqlite3 *db, sqlite3_int64 DeviceID, universal_id* puniversal_id)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
#ifdef USE_BASE_85
    universal_id_base85 _universal_id_base85_device;
#endif
#ifdef USE_BASE_64
    universal_id_base64 _universal_id_base64_device;
#endif

    res = sqlite3_prepare_v3(db, SQL_QUERY_UNIVERSALID_BY_DEVICEID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //DeviceID = DeviceID
    res = sqlite3_bind_int64(statement, 1, DeviceID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_ROW)//if (res == SQLITE_DONE)
    {
        goto ERROR_STATEMENT;
    }
#ifdef USE_BASE_85
    if (sqlite3_column_bytes(statement, 0) != UNIVERSAL_ID_BASE_85_SIZE_BYTES)
    {
        goto ERROR_STATEMENT;
    }
    strncpy(_universal_id_base85_device.ID_base85, (const char*)sqlite3_column_text(statement, 0), sqlite3_column_bytes(statement, 0));
#endif
#ifdef USE_BASE_64
    if (sqlite3_column_bytes(statement, 0) != UNIVERSAL_ID_BASE_64_SIZE_BYTES)
    {
        goto ERROR_STATEMENT;
    }
    strncpy(_universal_id_base64_device.ID_base64, (const char*)sqlite3_column_text(statement, 0), sqlite3_column_bytes(statement, 0));
#endif
    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

#ifdef USE_BASE_85
    res = decode_85(puniversal_id->ID, _universal_id_base85_device.ID_base85, UNIVERSAL_ID_BASE_85_SIZE_BYTES);
#endif
#ifdef USE_BASE_64
    res = decode_64(puniversal_id->ID, _universal_id_base64_device.ID_base64, UNIVERSAL_ID_BASE_64_SIZE_BYTES);
#endif
    if (res != UNIVERSAL_ID_SIZE_BYTES)
    {
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int insert_config(sqlite3 *db, const char* Name, const char* Value)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;

    res = sqlite3_prepare_v3(db, SQL_INSERT_CONFIG, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }
    //Name = Name
    res = sqlite3_bind_text(statement, 1, Name, -1, SQLITE_STATIC);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    //StorageStyle = 1
    res = sqlite3_bind_text(statement, 2, Value, -1, SQLITE_STATIC);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int get_config(sqlite3 *db, const char* Name, char* Value, size_t ValueLength, const char* DefaultValue)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    const unsigned char* pValue;

    res = sqlite3_prepare_v3(db, SQL_QUERY_CONFIG_BY_NAME, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //Name = Name
    //SensorName = SensorName
    res = sqlite3_bind_text(statement, 1, Name, -1, SQLITE_STATIC);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res == SQLITE_DONE)
    {
        memset(Value, 0, ValueLength);
        strncpy(Value, DefaultValue, ValueLength - 1);
    }
    else if (res == SQLITE_ROW)
    {
        pValue = sqlite3_column_text(statement, 0);
        if (!pValue)
        {
            goto ERROR_STATEMENT;
        }
        if (sqlite3_column_bytes(statement, 0) > ValueLength)
        {
            goto ERROR_STATEMENT;
        }
        memset(Value, 0, sqlite3_column_bytes(statement, 0) + 1);
        strncpy(Value, (const char*)pValue, sqlite3_column_bytes(statement, 0));
    }
    else
    {
        goto ERROR_STATEMENT;
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int get_device_by_name(sqlite3 *db, const char* DeviceName, sqlite3_int64* DeviceID)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    const unsigned char* pValue;
    __time_t updated;

    res = sqlite3_prepare_v3(db, SQL_QUERY_DEVICEID_BY_DEVICENAME, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //DeviceName = DeviceName
    res = sqlite3_bind_text(statement, 1, DeviceName, -1, SQLITE_STATIC);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_ROW)//if (res == SQLITE_DONE)
    {
        goto ERROR_STATEMENT;
    }

    //DeviceID
    *DeviceID = sqlite3_column_int64(statement, 0);

    //Updated
    if (sqlite3_column_type(statement, 1) == SQLITE_NULL)
    {//Pre authorized
    }
    else
    {
        pValue = sqlite3_column_text(statement, 1);

        res = sqlite_datetime_to_time_t(pValue, &updated);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int insert_foreign_device(sqlite3 *db, sqlite3_int64 oDeviceID, sqlite3_int64 ForeignDeviceID, sqlite3_int64 DomesticDeviceID)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;

    //Insert devices
    res = sqlite3_prepare_v3(db, SQL_INSERT_FOREIGNDEVICES, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }
    //DeviceID
    res = sqlite3_bind_int64(statement, 1, oDeviceID);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    //ForeignDeviceID = ForeignDeviceID
    res = sqlite3_bind_int(statement, 2, ForeignDeviceID);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    //DomesticDeviceID = DomesticDeviceID
    res = sqlite3_bind_int(statement, 3, DomesticDeviceID);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int update_foreign_device(sqlite3 *db, sqlite3_int64 oDeviceID, sqlite3_int64 ForeignDeviceID, sqlite3_int64 DomesticDeviceID)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;

    //Update devices
    res = sqlite3_prepare_v3(db, SQL_UPDATE_FOREIGNDEVICES, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }
    //DomesticDeviceID
    res = sqlite3_bind_int(statement, 1, DomesticDeviceID);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    //DeviceID
    res = sqlite3_bind_int64(statement, 2, oDeviceID);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    //ForeignDeviceID
    res = sqlite3_bind_int(statement, 3, ForeignDeviceID);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int get_foreign_device(sqlite3 *db, sqlite3_int64 oDeviceID, sqlite3_int64 ForeignDeviceID, sqlite3_int64* DomesticDeviceID)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;

    res = sqlite3_prepare_v3(db, SQL_QUERY_FOREIGNDEVICES_BY_DEVICEID_FOREIGNDEVICEID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //DeviceID
    res = sqlite3_bind_int64(statement, 1, oDeviceID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //ForeignDeviceID = ForeignDeviceID
    res = sqlite3_bind_int64(statement, 2, ForeignDeviceID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_ROW)//if (res == SQLITE_DONE)
    {
        goto ERROR_STATEMENT;
    }
    *DomesticDeviceID = sqlite3_column_int64(statement, 0);

    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int upsert_foreign_sensor(sqlite3 *db, sqlite3_int64 oDeviceID, unsigned short sensorcount, sqlite3_int64* ForeignDeviceIDs, sqlite3_int64* ForeignSensorIDs, sqlite3_int64* DomesticSensorIDs)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement_query;
    sqlite3_stmt* statement_insert;
    sqlite3_stmt* statement_update;
    int loop;

    res = sqlite3_prepare_v3(db, SQL_QUERY_FOREIGNSENSORS_BY_DEVICEID_FOREIGNDEVICEID_FOREIGNSENSORID, -1, 0, &statement_query, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    for (loop = 0; loop < sensorcount; loop++)
    {
        //DeviceID
        res = sqlite3_bind_int64(statement_query, 1, oDeviceID);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        //ForeignDeviceID
        res = sqlite3_bind_int64(statement_query, 2, ForeignDeviceIDs[loop]);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        //ForeignSensorID
        res = sqlite3_bind_int64(statement_query, 3, ForeignSensorIDs[loop]);
        if (res)
        {
            goto ERROR_STATEMENT;
        }

        res = sqlite3_step(statement_query);
        if (res == SQLITE_DONE)
        {//Insert
            //Insert foreign sensor
            res = sqlite3_prepare_v3(db, SQL_INSERT_FOREIGNSENSORS, -1, 0, &statement_insert, &sqlBufferUnused);
            if (res)
            {
                //TODO: log error
                goto ERROR_STATEMENT;
            }
            //DeviceID
            res = sqlite3_bind_int64(statement_insert, 1, oDeviceID);
            if (res)
            {
                res = sqlite3_finalize(statement_insert);
                goto ERROR_STATEMENT;
            }
            //ForeignDeviceID = ForeignDeviceID
            res = sqlite3_bind_int(statement_insert, 2, ForeignDeviceIDs[loop]);
            if (res)
            {
                res = sqlite3_finalize(statement_insert);
                goto ERROR_STATEMENT;
            }
            //ForeignSensorID = ForeignSensorID
            res = sqlite3_bind_int(statement_insert, 3, ForeignSensorIDs[loop]);
            if (res)
            {
                res = sqlite3_finalize(statement_insert);
                goto ERROR_STATEMENT;
            }
            //DomesticSensorID = DomesticSensorID
            res = sqlite3_bind_int(statement_insert, 4, DomesticSensorIDs[loop]);
            if (res)
            {
                res = sqlite3_finalize(statement_insert);
                goto ERROR_STATEMENT;
            }
            res = sqlite3_step(statement_insert);
            if (res != SQLITE_DONE)
            {
                res = sqlite3_finalize(statement_insert);
                goto ERROR_STATEMENT;
            }
            res = sqlite3_finalize(statement_insert);
            if (res)
            {
                //TODO: log error
                goto ERROR_STATEMENT;
            }
        }
        else if (res == SQLITE_ROW)
        {//Update
            //Update foreign sensor
            res = sqlite3_prepare_v3(db, SQL_UPDATE_FOREIGNSENSORS, -1, 0, &statement_update, &sqlBufferUnused);
            if (res)
            {
                //TODO: log error
                goto ERROR;
            }
            //DomesticSensorID
            res = sqlite3_bind_int(statement_update, 1, DomesticSensorIDs[loop]);
            if (res)
            {
                res = sqlite3_finalize(statement_update);
                goto ERROR_STATEMENT;
            }
            //DeviceID
            res = sqlite3_bind_int64(statement_update, 2, oDeviceID);
            if (res)
            {
                res = sqlite3_finalize(statement_update);
                goto ERROR_STATEMENT;
            }
            //ForeignDeviceID
            res = sqlite3_bind_int(statement_update, 3, ForeignDeviceIDs[loop]);
            if (res)
            {
                res = sqlite3_finalize(statement_update);
                goto ERROR_STATEMENT;
            }
            //ForeignSensorID
            res = sqlite3_bind_int(statement_update, 4, ForeignSensorIDs[loop]);
            if (res)
            {
                res = sqlite3_finalize(statement_update);
                goto ERROR_STATEMENT;
            }
            res = sqlite3_step(statement_update);
            if (res != SQLITE_DONE)
            {
                res = sqlite3_finalize(statement_update);
                goto ERROR_STATEMENT;
            }
            res = sqlite3_finalize(statement_update);
            if (res)
            {
                //TODO: log error
                goto ERROR_STATEMENT;
            }
        }
        else
        {
            goto ERROR_STATEMENT;
        }

        res = sqlite3_reset(statement_query);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement_query);
ERROR:
    return -1;
}

int insert_unmapped_foreign_sensor(sqlite3 *db, sqlite3_int64 oDeviceID, sqlite3_int64 ForeignDeviceID, sqlite3_int64 ForeignSensorID)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;

    //Insert devices
    res = sqlite3_prepare_v3(db, SQL_INSERT_UNMAPPED_FOREIGNSENSORS_BY_DEVICEID_FOREIGNDEVICEID_FOREIGNSENSORID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }
    //DeviceID
    res = sqlite3_bind_int64(statement, 1, oDeviceID);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    //ForeignDeviceID = ForeignDeviceID
    res = sqlite3_bind_int(statement, 2, ForeignDeviceID);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    //ForeignSensorID = ForeignSensorID
    res = sqlite3_bind_int(statement, 3, ForeignSensorID);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int authorize_device(sqlite3 *db, const universal_id* puniversal_id, const char* DeviceName, unsigned short DeviceType, unsigned short StorageStyle, const char* BtAddress, unsigned char RFCOMMChannel, unsigned short PSM, const char* I2PPublicKey, sqlite3_int64* DeviceID)
{
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    int res;
#ifdef USE_BASE_85
    universal_id_base85 _universal_id_base85_device;
#endif
#ifdef USE_BASE_64
    universal_id_base64 _universal_id_base64_device;
#endif

    //Insert device
    res = sqlite3_prepare_v3(db, SQL_INSERT_DEVICES_AUTHORIZED, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    //UniversalID
#ifdef USE_BASE_85
    encode_85(_universal_id_base85_device.ID_base85, (const unsigned char *)puniversal_id->ID, sizeof(universal_id));
    res = sqlite3_bind_text(statement, 1, _universal_id_base85_device.ID_base85, UNIVERSAL_ID_BASE_85_SIZE_BYTES, SQLITE_STATIC);
#endif
#ifdef USE_BASE_64
    encode_64(_universal_id_base64_device.ID_base64, puniversal_id->ID, sizeof(_universal_id.ID));
    res = sqlite3_bind_text(statement, 1, _universal_id_base64_device.ID_base64, UNIVERSAL_ID_BASE_64_SIZE_BYTES, SQLITE_STATIC);
#endif
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }

    //DeviceType
    res = sqlite3_bind_int(statement, 2, DeviceType);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }

    //StorageStyle
    res = sqlite3_bind_int(statement, 3, StorageStyle);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }

    //DeviceName
    res = sqlite3_bind_text(statement, 4, DeviceName, -1, SQLITE_STATIC);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }

    //BtAddress
    if (BtAddress)
    {
        res = sqlite3_bind_text(statement, 5, BtAddress, -1, SQLITE_STATIC);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
    }
    else
    {
        res = sqlite3_bind_null(statement, 5);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
    }
    //RFCOMMChannel
    if (RFCOMMChannel)
    {
        res = sqlite3_bind_int(statement, 6, RFCOMMChannel);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
    }
    else
    {
        res = sqlite3_bind_null(statement, 6);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
    }
    //PSM
    if (PSM)
    {
        res = sqlite3_bind_int(statement, 7, PSM);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
    }
    else
    {
        res = sqlite3_bind_null(statement, 7);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
    }

    //I2PPublicKey
    if (I2PPublicKey)
    {
        res = sqlite3_bind_text(statement, 8, I2PPublicKey, -1, SQLITE_STATIC);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
    }
    else
    {
        res = sqlite3_bind_null(statement, 8);
        if (res)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }
    }
    //I2PPrivateKey
    res = sqlite3_bind_null(statement, 9);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    *DeviceID = sqlite3_last_insert_rowid(db);
    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int verify_bt_device(sqlite3 *db, const char* DeviceName, const char* BtAddress, sqlite3_int64* DeviceID)
{
    char *zErrMsg = 0;
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;

    res = sqlite3_prepare_v3(db, SQL_QUERY_DEVICEID_BY_DEVICENAME_BTADDRESS, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //DeviceName
    res = sqlite3_bind_text(statement, 1, DeviceName, -1, SQLITE_STATIC);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //BtAddress
    res = sqlite3_bind_text(statement, 2, BtAddress, -1, SQLITE_STATIC);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_ROW)//if (res == SQLITE_DONE)
    {
        goto ERROR_STATEMENT;
    }
    *DeviceID = sqlite3_column_int64(statement, 0);
    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int synch_device_status(sqlite3 *db, sqlite3_int64 mDeviceID, sqlite3_int64 oDeviceID, unsigned short* SynchType, sqlite3_int64* SynchState)
{
    //TODO: #28: ensure not already connected to a device
    *SynchType = 0;
    *SynchState = 0;
    return 0;
}

int synch_device_begin(sqlite3 *db, sqlite3_int64 mDeviceID, sqlite3_int64 oDeviceID, unsigned short SynchType, sqlite3_int64 SynchState)
{
    //TODO: #28: ensure not already connected to a device
    return 0;
}

int synch_device_end(sqlite3 *db, sqlite3_int64 mDeviceID, sqlite3_int64 oDeviceID, unsigned short SynchType)
{
    //TODO: #28: ensure not already connected to a device
    return 0;
}

#ifdef USE_BASE_85
// static const char en85[] =
// {
//     '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
//     'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
//     'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
//     'U', 'V', 'W', 'X', 'Y', 'Z',
//     'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
//     'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
//     'u', 'v', 'w', 'x', 'y', 'z',
//     '!', '#', '$', '%', '&', '(', ')', '*', '+', '-',
//     ';', '<', '=', '>', '?', '@', '^', '_',	'`', '{',
//     '|', '}', '~'
// };
// static char de85[256];

// static void prep_base85(void)
// {
//     int i;
//     if (de85['Z'])
//         return;
//     for (i = 0; i < sizeof(en85); i++)
//     {
//         int ch = en85[i];
//         de85[ch] = i + 1;
//     }
// }

// int decode_85(char *dst, const char *buffer, int len)
// {
//     prep_base85();

//     while (len)
//     {
//         unsigned acc = 0;
//         int de, cnt = 4;
//         unsigned char ch;
//         do
//         {
//             ch = *buffer++;
//             de = de85[ch];
//             if (--de < 0)
//                 return -1;//error("invalid base85 alphabet %c", ch);
//             acc = acc * 85 + de;
//         } while (--cnt);
//         ch = *buffer++;
//         de = de85[ch];
//         if (--de < 0)
//             return -1;//error("invalid base85 alphabet %c", ch);
//         /* Detect overflow. */
//         if (0xffffffff / 85 < acc ||
//             0xffffffff - de < (acc *= 85))
//             return -1;//error("invalid base85 sequence %.5s", buffer-5);
//         acc += de;

//         cnt = (len < 4) ? len : 4;
//         len -= cnt;
//         do
//         {
//             acc = (acc << 8) | (acc >> 24);
//             *dst++ = acc;
//         } while (--cnt);
//     }

//     return 0;
// }

// void encode_85(char *buf, const unsigned char *data, int bytes, int terminate)
// {
//     while (bytes)
//     {
//         unsigned acc = 0;
//         int cnt;
//         for (cnt = 24; cnt >= 0; cnt -= 8)
//         {
//             unsigned ch = *data++;
//             acc |= ch << cnt;
//             if (--bytes == 0)
//                 break;
//         }
//         for (cnt = 4; cnt >= 0; cnt--)
//         {
//             int val = acc % 85;
//             acc /= 85;
//             buf[cnt] = en85[val];
//         }
//         buf += 5;
//     }

//     if (terminate)
//         *buf = 0;
// }

int decode_85(char *dst, const char *buffer, int len)
{
    int32_t out_max_length = (len * 4) / 5;
    if ((len * 4) % 5)
    {
        out_max_length++;
    }
    int32_t out_length = decode_ascii85((const uint8_t *)buffer, len, (uint8_t *)dst, out_max_length);
    return out_length;
}
void encode_85(char *buf, const unsigned char *data, int bytes)
{
    int32_t out_max_length = (bytes * 5) / 4;
    if ((bytes * 5) % 4)
    {
        out_max_length++;
    }
    int32_t out_length = encode_ascii85(data, bytes, (uint8_t *)buf, out_max_length);
}
#endif
#ifdef USE_BASE_64
static const char encoding_table[] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
    'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
    'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z', '0', '1', '2', '3',
    '4', '5', '6', '7', '8', '9', '+', '/' };

static const unsigned char decoding_table[256] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3e, 0x00, 0x00, 0x00, 0x3f,
    0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e,
    0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28,
    0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30, 0x31, 0x32, 0x33, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

//char* base64_encode(const unsigned char *data, size_t input_length, size_t *output_length) {
void encode_64(char *buf, const unsigned char *data, int bytes) {
    const int mod_table[] = { 0, 2, 1 };
    int output_length = 4 * ((bytes + 2) / 3);

    for (int i = 0, j = 0; i < bytes;) {

        uint32_t octet_a = i < bytes ? (unsigned char)data[i++] : 0;
        uint32_t octet_b = i < bytes ? (unsigned char)data[i++] : 0;
        uint32_t octet_c = i < bytes ? (unsigned char)data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        buf[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        buf[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        buf[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        buf[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];

    }

    for (int i = 0; i < mod_table[bytes % 3]; i++)
        buf[output_length - 1 - i] = '=';
};

//unsigned char* base64_decode(const char *data, size_t input_length, size_t *output_length) {
int decode_64(char *dst, const char *buffer, int len) {
    if (len % 4 != 0)
        return -1;

    int output_length = len / 4 * 3;

    if (buffer[len - 1] == '=') (output_length)--;
    if (buffer[len - 2] == '=') (output_length)--;

    for (int i = 0, j = 0; i < len;) {
        uint32_t sextet_a = buffer[i] == '=' ? 0 & i++ : decoding_table[buffer[i++]];
        uint32_t sextet_b = buffer[i] == '=' ? 0 & i++ : decoding_table[buffer[i++]];
        uint32_t sextet_c = buffer[i] == '=' ? 0 & i++ : decoding_table[buffer[i++]];
        uint32_t sextet_d = buffer[i] == '=' ? 0 & i++ : decoding_table[buffer[i++]];

        uint32_t triple = (sextet_a << 3 * 6)
            + (sextet_b << 2 * 6)
            + (sextet_c << 1 * 6)
            + (sextet_d << 0 * 6);

        if (j < output_length) dst[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < output_length) dst[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < output_length) dst[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return 0;
};
#endif

int load_json_connection(const char* DeviceName, const universal_id* puniversal_id, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused)
{
    int res;
#ifdef USE_BASE_85
    universal_id_base85 _universal_id_base85;
#endif
#ifdef USE_BASE_64
    universal_id_base64 _universal_id_base64;
#endif
    struct json_object *connection;
    struct json_object *connection_connection;
    struct json_object *connection_connection_name_value;
    struct json_object *connection_connection_id_value;
    const char* text;

#ifdef USE_BASE_85
    memset(_universal_id_base85.ID_base85, 0, UNIVERSAL_ID_BASE_85_SIZE_BYTES);
    encode_85(_universal_id_base85.ID_base85, (const unsigned char *)puniversal_id->ID, sizeof(puniversal_id->ID));
#endif
#ifdef USE_BASE_64
    memset(_universal_id_base64.ID_base64, 0, UNIVERSAL_ID_BASE_64_SIZE_BYTES);
    encode_64(_universal_id_base64.ID_base64, puniversal_id->ID, sizeof(puniversal_id->ID));
#endif

    connection = json_object_new_object();
    if (!connection)
    {
        return -1;
    }

    connection_connection = json_object_new_object();
    if (!connection_connection)
    {
        json_object_put(connection);
        return -1;
    }

    res = json_object_object_add(connection, JSON_CONNECTION_CONNECTION, connection_connection);
    if (res)
    {
        json_object_put(connection_connection);
        json_object_put(connection);
        return -1;
    }

    connection_connection_name_value = json_object_new_string(DeviceName);
    if (!connection_connection_name_value)
    {
        json_object_put(connection);
        return -1;
    }

    res = json_object_object_add(connection_connection, JSON_CONNECTION_CONNECTION_NAME, connection_connection_name_value);
    if (res)
    {
        json_object_put(connection_connection_name_value);
        json_object_put(connection);
        return -1;
    }

#ifdef USE_BASE_85
    connection_connection_id_value = json_object_new_string_len(_universal_id_base85.ID_base85, sizeof(_universal_id_base85.ID_base85));
#endif
#ifdef USE_BASE_64
    connection_connection_id_value = json_object_new_string_len(_universal_id_base64.ID_base64, sizeof(_universal_id_base64.ID_base64));
#endif
    if (!connection_connection_id_value)
    {
        json_object_put(connection);
        return -1;
    }

    res = json_object_object_add(connection_connection, JSON_CONNECTION_CONNECTION_UUID, connection_connection_id_value);
    if (res)
    {
        json_object_put(connection_connection_id_value);
        json_object_put(connection);
        return -1;
    }

    text = json_object_to_json_string_length(connection, JSON_C_TO_STRING_PLAIN, cbuffer_sizeused);
    strncpy(cbuffer, text, *cbuffer_sizeused);

    json_object_put(connection);

    return 0;
}

int load_json_algorithminstance(sqlite3 *db, const universal_id* puniversal_id, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused)
{
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    const unsigned char* blob;
    int res;
    struct json_object *algorithminstance;
    struct json_object *algorithminstance_algorithminstance;
    struct json_object *field_value;
#ifdef USE_BASE_85
    universal_id_base85 _universal_id_base85;
#endif
#ifdef USE_BASE_64
    universal_id_base64 _universal_id_base64;
#endif
    const char* text;

    res = sqlite3_prepare_v3(db, SQL_QUERY_ALGORITHMINSTANCES_BY_ALGORITHMINSTANCEID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    //AlgorithmInstanceID
    res = sqlite3_bind_blob(statement, 1, puniversal_id->ID, sizeof(puniversal_id->ID), SQLITE_STATIC);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    res = sqlite3_step(statement);
    if (res != SQLITE_ROW)
    {
        goto ERROR_STATEMENT;
    }

    algorithminstance = json_object_new_object();
    if (!algorithminstance)
    {
        return -1;
    }

    algorithminstance_algorithminstance = json_object_new_object();
    if (!algorithminstance_algorithminstance)
    {
        json_object_put(algorithminstance);
        return -1;
    }

    res = json_object_object_add(algorithminstance, JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE, algorithminstance_algorithminstance);
    if (res)
    {
        json_object_put(algorithminstance_algorithminstance);
        json_object_put(algorithminstance);
        return -1;
    }

    //JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_ID
#ifdef USE_BASE_85
    memset(_universal_id_base85.ID_base85, 0, UNIVERSAL_ID_BASE_85_SIZE_BYTES);
    encode_85(_universal_id_base85.ID_base85, (const unsigned char *)puniversal_id->ID, sizeof(puniversal_id->ID));
    field_value = json_object_new_string_len(_universal_id_base85.ID_base85, sizeof(_universal_id_base85.ID_base85));
#endif
#ifdef USE_BASE_64
    memset(_universal_id_base64.ID_base64, 0, UNIVERSAL_ID_BASE_64_SIZE_BYTES);
    encode_64(_universal_id_base64.ID_base64, puniversal_id->ID, sizeof(puniversal_id->ID));
    field_value = json_object_new_string_len(_universal_id_base64.ID_base64, sizeof(_universal_id_base64.ID_base64));
#endif
    if (!field_value)
    {
        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }
    res = json_object_object_add(algorithminstance_algorithminstance, JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_ID, field_value);
    if (res)
    {
        json_object_put(field_value);
        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }

    //JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_ALGORITHMID
    blob = sqlite3_column_blob(statement, 0);
#ifdef USE_BASE_85
    memset(_universal_id_base85.ID_base85, 0, UNIVERSAL_ID_BASE_85_SIZE_BYTES);
    encode_85(_universal_id_base85.ID_base85, blob, sizeof(puniversal_id->ID));
    field_value = json_object_new_string_len(_universal_id_base85.ID_base85, sizeof(_universal_id_base85.ID_base85));
#endif
#ifdef USE_BASE_64
    memset(_universal_id_base64.ID_base64, 0, UNIVERSAL_ID_BASE_64_SIZE_BYTES);
    encode_64(_universal_id_base64.ID_base64, blob, sizeof(puniversal_id->ID));
    field_value = json_object_new_string_len(_universal_id_base64.ID_base64, sizeof(_universal_id_base64.ID_base64));
#endif
    if (!field_value)
    {
        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }
    res = json_object_object_add(algorithminstance_algorithminstance, JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_ALGORITHMID, field_value);
    if (res)
    {
        json_object_put(field_value);
        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }

    //JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_TYPE
    field_value = json_object_new_int(sqlite3_column_int(statement, 1));
    if (!field_value)
    {
        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }
    res = json_object_object_add(algorithminstance_algorithminstance, JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_TYPE, field_value);
    if (res)
    {
        json_object_put(field_value);
        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }

    //JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_VERSION
    field_value = json_object_new_int(sqlite3_column_int(statement, 2));
    if (!field_value)
    {
        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }
    res = json_object_object_add(algorithminstance_algorithminstance, JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_VERSION, field_value);
    if (res)
    {
        json_object_put(field_value);
        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }

    //JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_RESULTTYPE
    field_value = json_object_new_int(sqlite3_column_int(statement, 3));
    if (!field_value)
    {
        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }
    res = json_object_object_add(algorithminstance_algorithminstance, JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_RESULTTYPE, field_value);
    if (res)
    {
        json_object_put(field_value);
        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    //TODO: #28: Add independent/dependent sensors
    //TODO: #32: Finish
    // res = sqlite3_prepare_v3(db, SQL_QUERY_ALGORITHMSENSORS_BY_ALGORITHMID, -1, 0, &statement, &sqlBufferUnused);
    // if (res)
    // {
    //     goto ERROR;
    // }
    // //AlgorithmID
    // res = sqlite3_bind_blob(statement, 1, puniversal_id, sizeof(_universal_id), SQLITE_STATIC);
    // if (res)
    // {
    //     goto ERROR_STATEMENT;
    // }

    // palgorithm_detail_v1->sensorcount = 0;
    // while (1)
    // {
    //     res = sqlite3_step(statement);
    //     if (res == SQLITE_DONE)
    //     {
    //         break;
    //     }
    //     if (res != SQLITE_ROW)
    //     {
    //         goto ERROR_STATEMENT;
    //     }

    //     palgorithm_detail_v1->SensorIDs[palgorithm_detail_v1->sensorcount] = sqlite3_column_int64(statement, 0);

    //     palgorithm_detail_v1->sensorcount++;
    // }

    text = json_object_to_json_string_length(algorithminstance, JSON_C_TO_STRING_PLAIN, cbuffer_sizeused);
    strncpy(cbuffer, text, *cbuffer_sizeused);

    res = json_object_put(algorithminstance);
    if (!res)
    {
        //TODO: log error

        return -1;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int insert_algorithm_by_json(sqlite3 *db, sqlite3_int64 DeviceID, const char* json)
{
    int res;
    struct json_object *algorithminstance;
    struct json_object *algorithminstance_algorithminstance;
    struct json_object *field_value;
    universal_id _universal_id;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    int text_length;
    const char* text;
    int loop;
    int sensor_loop;
    sqlite3_int64 SensorID;

    //TODO: #28: Add independent/dependent sensors

    //Insert algorithm
    res = sqlite3_prepare_v3(db, SQL_INSERT_ALGORITHMINSTANCES, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    algorithminstance = json_tokener_parse(json);
    if (!algorithminstance)
    {
        //TODO: log info: reject

        goto ERROR_STATEMENT;
    }

    algorithminstance_algorithminstance = json_object_object_get(algorithminstance, JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE);
    if (!algorithminstance_algorithminstance)
    {
        //TODO: log info: reject

        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }

    //JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_ID
    field_value = json_object_object_get(algorithminstance_algorithminstance, JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_ID);
    if (!field_value)
    {
        //TODO: log info: reject

        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }
    text_length = json_object_get_string_len(field_value);
    text = json_object_get_string(field_value);
#ifdef USE_BASE_85
    assert(text_length == sizeof(universal_id_base85));
    res = decode_85(_universal_id.ID, text, text_length);
#endif
#ifdef USE_BASE_64
    assert(text_length == sizeof(_universal_id_base64.ID_base64));
    res = decode_64(_universal_id.ID, text, text_length);
#endif
    if (res != UNIVERSAL_ID_SIZE_BYTES)
    {
        //TODO: log info: reject

        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }
    //AlgorithmInstanceID
    res = sqlite3_bind_blob(statement, 1, _universal_id.ID, sizeof(_universal_id.ID), SQLITE_TRANSIENT);
    if (res)
    {
        //TODO: log error

        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }

    //JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_ALGORITHMID
    field_value = json_object_object_get(algorithminstance_algorithminstance, JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_ALGORITHMID);
    if (!field_value)
    {
        //TODO: log info: reject

        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }
    text_length = json_object_get_string_len(field_value);
    text = json_object_get_string(field_value);
#ifdef USE_BASE_85
    assert(text_length == sizeof(universal_id_base85));
    res = decode_85(_universal_id.ID, text, text_length);
#endif
#ifdef USE_BASE_64
    assert(text_length == sizeof(_universal_id_base64.ID_base64));
    res = decode_64(_universal_id.ID, text, text_length);
#endif
    if (res != UNIVERSAL_ID_SIZE_BYTES)
    {
        //TODO: log info: reject

        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }
    //AlgorithmID
    res = sqlite3_bind_blob(statement, 2, _universal_id.ID, sizeof(_universal_id.ID), SQLITE_TRANSIENT);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }

    //AlgorithmType
    field_value = json_object_object_get(algorithminstance_algorithminstance, JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_TYPE);
    if (!field_value)
    {
        //TODO: log info: reject

        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }
    res = sqlite3_bind_int(statement, 3, json_object_get_int(field_value));
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }

    //AlgorithmVersion
    field_value = json_object_object_get(algorithminstance_algorithminstance, JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_VERSION);
    if (!field_value)
    {
        //TODO: log info: reject

        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }
    res = sqlite3_bind_int(statement, 4, json_object_get_int(field_value));
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }

    //ResultType
    field_value = json_object_object_get(algorithminstance_algorithminstance, JSON_ALGORITHMINSTANCE_ALGORITHMINSTANCE_RESULTTYPE);
    if (!field_value)
    {
        //TODO: log info: reject

        json_object_put(algorithminstance);
        goto ERROR_STATEMENT;
    }
    res = sqlite3_bind_int(statement, 5, json_object_get_int(field_value));
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }

    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }
    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    // if (palgorithm_detail_v1->sensorcount)
    // {
    //     for (sensor_loop = 0; sensor_loop < palgorithm_detail_v1->sensorcount; sensor_loop++)
    //     {
    //         res = sqlite3_prepare_v3(db, SQL_QUERY_FOREIGNSENSORS_BY_DEVICEID_FOREIGNSENSORID, -1, 0, &statement, &sqlBufferUnused);
    //         if (res)
    //         {
    //             goto ERROR;
    //         }

    //         //DeviceID
    //         res = sqlite3_bind_int64(statement, 1, DeviceID);
    //         if (res)
    //         {
    //             goto ERROR_STATEMENT;
    //         }
    //         //ForeignSensorID
    //         res = sqlite3_bind_int64(statement, 2, palgorithm_detail_v1->SensorIDs[sensor_loop]);
    //         if (res)
    //         {
    //             goto ERROR_STATEMENT;
    //         }
    //         res = sqlite3_step(statement);
    //         if (res != SQLITE_ROW)
    //         {
    //             goto ERROR_STATEMENT;
    //         }
    //         SensorID = sqlite3_column_int64(statement, 0);
    //         res = sqlite3_finalize(statement);
    //         if (res)
    //         {
    //             //TODO: log error
    //             goto ERROR;
    //         }

    //         //Insert algorithmInstance
    //         res = sqlite3_prepare_v3(db, SQL_INSERT_ALGORITHMINSTANCESENSORS, -1, 0, &statement, &sqlBufferUnused);
    //         if (res)
    //         {
    //             goto ERROR;
    //         }
    //         //AlgorithmInstanceID
    //         res = sqlite3_bind_blob(statement, 1, palgorithm_detail_v1->AlgorithmID.ID, sizeof(_universal_id), SQLITE_STATIC);
    //         if (res)
    //         {
    //             //TODO: log error
    //             goto ERROR_STATEMENT;
    //         }
    //         CARP;
    //         //SensorID
    //         res = sqlite3_bind_int64(statement, 2, SensorID);
    //         if (res)
    //         {
    //             //TODO: log error
    //             goto ERROR_STATEMENT;
    //         }
    //         res = sqlite3_step(statement);
    //         if (res != SQLITE_DONE)
    //         {
    //             //TODO: log error
    //             goto ERROR_STATEMENT;
    //         }
    //         res = sqlite3_finalize(statement);
    //         if (res)
    //         {
    //             //TODO: log error
    //             goto ERROR;
    //         }
    //     }
    // }

    json_object_put(algorithminstance);

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int load_sensor_detail_json(sqlite3 *db, unsigned int sensorcount, sqlite3_int64* SensorIDs, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused)
{
    int res;
    struct json_message* pjson_message;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    const unsigned char* text;
    struct json_object *sensor;
    struct json_object *sensor_sensors;
    struct json_object *sensor_sensors_unnamed;
    struct json_object *sensor_sensors_sensor;
    struct json_object *field_value;
    int loop;
    const char* sensor_json;

    sensor = json_object_new_object();
    if (!sensor)
    {
        return -1;
    }

    sensor_sensors = json_object_new_array();
    if (!sensor_sensors)
    {
        json_object_put(sensor);
        return -1;
    }

    res = json_object_object_add(sensor, JSON_SENSOR_SENSORS, sensor_sensors);
    if (res)
    {
        json_object_put(sensor_sensors);
        json_object_put(sensor);
        return -1;
    }

    res = sqlite3_prepare_v3(db, SQL_QUERY_SENSORS_BY_SENSORID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    for (loop = 0; loop < sensorcount; loop++)
    {
        //SensorID
        res = sqlite3_bind_int64(statement, 1, SensorIDs[loop]);
        if (res)
        {
            goto ERROR_STATEMENT;
        }

        res = sqlite3_step(statement);
        if (res != SQLITE_ROW)
        {
            goto ERROR_STATEMENT;
        }

        sensor_sensors_unnamed = json_object_new_object();
        if (!sensor_sensors_unnamed)
        {
            json_object_put(sensor);
            goto ERROR_STATEMENT;
        }

        res = json_object_array_add(sensor_sensors, sensor_sensors_unnamed);
        if (res)
        {
            json_object_put(sensor_sensors_unnamed);
            json_object_put(sensor);
            goto ERROR_STATEMENT;
        }

        sensor_sensors_sensor = json_object_new_object();
        if (!sensor_sensors_sensor)
        {
            json_object_put(sensor);
            goto ERROR_STATEMENT;
        }

        res = json_object_object_add(sensor_sensors_unnamed, JSON_SENSOR_SENSORS_SENSOR, sensor_sensors_sensor);
        if (res)
        {
            json_object_put(field_value);
            json_object_put(sensor);
            goto ERROR_STATEMENT;
        }

        //JSON_SENSOR_SENSORS_SENSOR_NAME
        text = sqlite3_column_text(statement, 1);
        field_value = json_object_new_string((const char*)text);
        if (!field_value)
        {
            json_object_put(sensor);
            goto ERROR_STATEMENT;
        }
        res = json_object_object_add(sensor_sensors_sensor, JSON_SENSOR_SENSORS_SENSOR_NAME, field_value);
        if (res)
        {
            json_object_put(field_value);
            json_object_put(sensor);
            goto ERROR_STATEMENT;
        }

        //JSON_SENSOR_SENSORS_SENSOR_ID
        field_value = json_object_new_int64(SensorIDs[loop]);
        if (!field_value)
        {
            json_object_put(sensor);
            goto ERROR_STATEMENT;
        }
        res = json_object_object_add(sensor_sensors_sensor, JSON_SENSOR_SENSORS_SENSOR_ID, field_value);
        if (res)
        {
            json_object_put(field_value);
            json_object_put(sensor);
            goto ERROR_STATEMENT;
        }

        //JSON_SENSOR_SENSORS_SENSOR_SENSORTYPE
        field_value = json_object_new_int(sqlite3_column_int(statement, 0));
        if (!field_value)
        {
            json_object_put(sensor);
            goto ERROR_STATEMENT;
        }
        res = json_object_object_add(sensor_sensors_sensor, JSON_SENSOR_SENSORS_SENSOR_SENSORTYPE, field_value);
        if (res)
        {
            json_object_put(field_value);
            json_object_put(sensor);
            goto ERROR_STATEMENT;
        }

        //JSON_SENSOR_SENSORS_SENSOR_VALUETYPE
        field_value = json_object_new_int(sqlite3_column_int(statement, 2));
        if (!field_value)
        {
            json_object_put(sensor);
            goto ERROR_STATEMENT;
        }
        res = json_object_object_add(sensor_sensors_sensor, JSON_SENSOR_SENSORS_SENSOR_VALUETYPE, field_value);
        if (res)
        {
            json_object_put(field_value);
            json_object_put(sensor);
            goto ERROR_STATEMENT;
        }

        //JSON_SENSOR_SENSORS_SENSOR_ADDEDS
        field_value = json_object_new_int(sqlite3_column_int(statement, 3));
        if (!field_value)
        {
            json_object_put(sensor);
            goto ERROR_STATEMENT;
        }
        res = json_object_object_add(sensor_sensors_sensor, JSON_SENSOR_SENSORS_SENSOR_ADDEDS, field_value);
        if (res)
        {
            json_object_put(field_value);
            json_object_put(sensor);
            goto ERROR_STATEMENT;
        }

        res = sqlite3_reset(statement);
        if (!field_value)
        {
            json_object_put(sensor);
            goto ERROR_STATEMENT;
        }
    }

    sensor_json = json_object_to_json_string_length(sensor, JSON_C_TO_STRING_PLAIN, cbuffer_sizeused);
    strncpy(cbuffer, sensor_json, *cbuffer_sizeused);

    json_object_put(sensor);

    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int insert_sensor_by_json(sqlite3 *db, sqlite3_int64 oDeviceID, const char* json, unsigned short* sensorcount, sqlite3_int64* SensorID)
{
    struct json_object *sensors;
    struct json_object *sensors_sensors;
    struct json_object *sensors_sensors_entry;
    struct json_object *sensors_sensors_sensor;
    struct json_object *field_value;
    int loop;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    int res;

    sensors = json_tokener_parse(json);
    if (!sensors)
    {
        //TODO: log info: reject

        goto ERROR;
    }

    sensors_sensors = json_object_object_get(sensors, JSON_SENSOR_SENSORS);
    if (!sensors_sensors)
    {
        //TODO: log info: reject

        json_object_put(sensors);
        goto ERROR;
    }

    *sensorcount = json_object_array_length(sensors_sensors);

    for (loop = 0; loop < *sensorcount; loop++)
    {
        sensors_sensors_entry = json_object_array_get_idx(sensors_sensors, loop);
        if (!sensors_sensors_entry)
        {
            //TODO: log info: reject

            json_object_put(sensors);
            goto ERROR;
        }

        sensors_sensors_sensor = json_object_object_get(sensors_sensors_entry, JSON_SENSOR_SENSORS_SENSOR);
        if (!sensors_sensors_sensor)
        {
            //TODO: log info: reject

            json_object_put(sensors);
            goto ERROR;
        }

        //Insert device
        res = sqlite3_prepare_v3(db, SQL_INSERT_SENSORS, -1, 0, &statement, &sqlBufferUnused);
        if (res)
        {
            //TODO: log error

            json_object_put(sensors);
            goto ERROR;
        }

        //DeviceID
        res = sqlite3_bind_int(statement, 1, oDeviceID);
        if (res)
        {
            //TODO: log error

            json_object_put(sensors);
            goto ERROR_STATEMENT;
        }

        //SensorType
        field_value = json_object_object_get(sensors_sensors_sensor, JSON_SENSOR_SENSORS_SENSOR_SENSORTYPE);
        if (!field_value)
        {
            //TODO: log info: reject

            json_object_put(sensors);
            goto ERROR_STATEMENT;
        }
        res = sqlite3_bind_int(statement, 2, json_object_get_int(field_value));
        if (res)
        {
            //TODO: log error

            json_object_put(sensors);
            goto ERROR_STATEMENT;
        }

        //SensorName
        field_value = json_object_object_get(sensors_sensors_sensor, JSON_SENSOR_SENSORS_SENSOR_NAME);
        if (!field_value)
        {
            //TODO: log info: reject

            json_object_put(sensors);
            goto ERROR_STATEMENT;
        }
        res = sqlite3_bind_text(statement, 3, json_object_get_string(field_value), -1, SQLITE_TRANSIENT);
        if (res)
        {
            //TODO: log error

            json_object_put(sensors);
            goto ERROR_STATEMENT;
        }

        //ValueType
        field_value = json_object_object_get(sensors_sensors_sensor, JSON_SENSOR_SENSORS_SENSOR_VALUETYPE);
        if (!field_value)
        {
            //TODO: log info: reject

            json_object_put(sensors);
            goto ERROR_STATEMENT;
        }
        res = sqlite3_bind_int(statement, 4, json_object_get_int(field_value));
        if (res)
        {
            //TODO: log error

            json_object_put(sensors);
            goto ERROR_STATEMENT;
        }

        res = sqlite3_step(statement);
        if (res != SQLITE_DONE)
        {
            //TODO: log error
            json_object_put(sensors);
            goto ERROR_STATEMENT;
        }
        *SensorID = sqlite3_last_insert_rowid(db);

        res = sqlite3_reset(statement);
        if (res)
        {
            //TODO: log error

            json_object_put(sensors);
            goto ERROR_STATEMENT;
        }
    }

    json_object_put(sensors);

    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int load_devices_json(sqlite3 *db, unsigned short devicecount, sqlite3_int64* DeviceIDs, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused)
{
    int res;
    int loop;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    sqlite3_stmt* statement_sensors;
    const unsigned char* text;
    __time_t updated;
    struct json_object *device;
    struct json_object *device_devices;
    struct json_object *device_devices_unnamed;
    struct json_object *device_devices_device;
    struct json_object *field_value;
    struct json_object *device_devices_device_sensors;
    const char* device_json;

    device = json_object_new_object();
    if (!device)
    {
        return -1;
    }

    device_devices = json_object_new_array();
    if (!device_devices)
    {
        json_object_put(device);
        return -1;
    }

    res = json_object_object_add(device, JSON_DEVICE_DEVICES, device_devices);
    if (res)
    {
        json_object_put(device_devices);
        json_object_put(device);
        return -1;
    }

    res = sqlite3_prepare_v3(db, SQL_QUERY_DEVICES_BY_DEVICEID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        json_object_put(device);
        goto ERROR;
    }

    res = sqlite3_prepare_v3(db, SQL_QUERY_SENSORID_BY_DEVICEID, -1, 0, &statement_sensors, &sqlBufferUnused);
    if (res)
    {
        json_object_put(device);
        goto ERROR;
    }

    for (loop = 0; loop < devicecount; loop++)
    {
        //DeviceID
        res = sqlite3_bind_int64(statement, 1, DeviceIDs[loop]);
        if (res)
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        res = sqlite3_step(statement);
        if (res != SQLITE_ROW)
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        device_devices_unnamed = json_object_new_object();
        if (!device_devices_unnamed)
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        res = json_object_array_add(device_devices, device_devices_unnamed);
        if (res)
        {
            json_object_put(device_devices_unnamed);
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        device_devices_device = json_object_new_object();
        if (!device_devices_device)
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        res = json_object_object_add(device_devices_unnamed, JSON_DEVICE_DEVICES_DEVICE, device_devices_device);
        if (res)
        {
            json_object_put(device_devices_device);
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        //JSON_DEVICE_DEVICE_ID
        field_value = json_object_new_int64(DeviceIDs[loop]);
        if (!field_value)
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }
        res = json_object_object_add(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_ID, field_value);
        if (res)
        {
            json_object_put(field_value);
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        //JSON_DEVICE_DEVICES_DEVICE_UUID
        text = sqlite3_column_text(statement, 0);
        field_value = json_object_new_string((const char*)text);
        if (!field_value)
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }
        res = json_object_object_add(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_UUID, field_value);
        if (res)
        {
            json_object_put(field_value);
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        //JSON_DEVICE_DEVICE_NAME
        text = sqlite3_column_text(statement, 3);
        field_value = json_object_new_string((const char*)text);
        if (!field_value)
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }
        res = json_object_object_add(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_NAME, field_value);
        if (res)
        {
            json_object_put(field_value);
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        //JSON_DEVICE_DEVICE_UPDATEDS
        if (sqlite3_column_type(statement, 6) == SQLITE_NULL)
        {//This is a preauthorized device so send no data
            field_value = json_object_new_int64(0);
            if (!field_value)
            {
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
        }
        else
        {
            text = sqlite3_column_text(statement, 6);
            res = sqlite_datetime_to_time_t(text, &updated);
            if (res)
            {
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
            field_value = json_object_new_int64(updated);
            if (!field_value)
            {
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
        }
        res = json_object_object_add(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_UPDATEDS, field_value);
        if (res)
        {
            json_object_put(field_value);
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        //JSON_DEVICE_DEVICE_TYPE
        field_value = json_object_new_int(sqlite3_column_int(statement, 1));
        if (!field_value)
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }
        res = json_object_object_add(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_TYPE, field_value);
        if (res)
        {
            json_object_put(field_value);
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        //JSON_DEVICE_DEVICE_STORAGESTYLE
        field_value = json_object_new_int(sqlite3_column_int(statement, 2));
        if (!field_value)
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }
        res = json_object_object_add(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_STORAGESTYLE, field_value);
        if (res)
        {
            json_object_put(field_value);
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        //JSON_DEVICE_DEVICE_BDADDR
        if (sqlite3_column_type(statement, 4) == SQLITE_NULL)
        {
            res = json_object_object_add(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_BDADDR, NULL);
            if (res)
            {
                json_object_put(field_value);
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
        }
        else
        {
            text = sqlite3_column_text(statement, 4);
            field_value = json_object_new_string((const char*)text);
            if (!field_value)
            {
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
            res = json_object_object_add(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_BDADDR, field_value);
            if (res)
            {
                json_object_put(field_value);
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
        }

        //JSON_DEVICE_DEVICES_DEVICE_RFCOMMCHANNEL
        if (sqlite3_column_type(statement, 5) == SQLITE_NULL)
        {
            res = json_object_object_add(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_RFCOMMCHANNEL, NULL);
            if (res)
            {
                json_object_put(field_value);
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
        }
        else
        {
            field_value = json_object_new_int(sqlite3_column_int(statement, 5));
            if (!field_value)
            {
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
            res = json_object_object_add(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_RFCOMMCHANNEL, field_value);
            if (res)
            {
                json_object_put(field_value);
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
        }

        //JSON_DEVICE_DEVICES_DEVICE_PSM
        if (sqlite3_column_type(statement, 6) == SQLITE_NULL)
        {
            res = json_object_object_add(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_PSM, NULL);
            if (res)
            {
                json_object_put(field_value);
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
        }
        else
        {
            field_value = json_object_new_int(sqlite3_column_int(statement, 6));
            if (!field_value)
            {
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
            res = json_object_object_add(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_PSM, field_value);
            if (res)
            {
                json_object_put(field_value);
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
        }

        //JSON_DEVICE_DEVICE_I2P
        if (sqlite3_column_type(statement, 7) == SQLITE_NULL)
        {
            res = json_object_object_add(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_I2P, NULL);
            if (res)
            {
                json_object_put(field_value);
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
        }
        else
        {
            text = sqlite3_column_text(statement, 7);
            field_value = json_object_new_string((const char*)text);
            if (!field_value)
            {
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
            res = json_object_object_add(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_I2P, field_value);
            if (res)
            {
                json_object_put(field_value);
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
        }

        res = sqlite3_reset(statement);
        if (res)
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        //Sensors
        device_devices_device_sensors = json_object_new_array();
        if (!device_devices_device_sensors)
        {
            json_object_put(device);
            return -1;
        }

        res = json_object_object_add(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_SENSORS, device_devices_device_sensors);
        if (res)
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        //DeviceID
        res = sqlite3_bind_int64(statement_sensors, 1, DeviceIDs[loop]);
        if (res)
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        while (1)
        {
            res = sqlite3_step(statement_sensors);
            if (res == SQLITE_DONE)
            {
                break;
            }
            if (res != SQLITE_ROW)
            {
                json_object_put(device);
                goto ERROR_STATEMENT;
            }

            field_value = json_object_new_int64(sqlite3_column_int64(statement, 0));
            if (!field_value)
            {
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
            res = json_object_array_add(device_devices_device_sensors, field_value);
            if (res)
            {
                json_object_put(field_value);
                json_object_put(device);
                goto ERROR_STATEMENT;
            }
        }

        res = sqlite3_reset(statement_sensors);
        if (res)
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }
    }

    device_json = json_object_to_json_string_length(device, JSON_C_TO_STRING_PLAIN, cbuffer_sizeused);
    strncpy(cbuffer, device_json, *cbuffer_sizeused);

    json_object_put(device);

    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    res = sqlite3_finalize(statement_sensors);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
    res = sqlite3_finalize(statement_sensors);
ERROR:
    return -1;
}

int upsert_foreign_devices_by_json(sqlite3 *db, sqlite3_int64 oDeviceID, const char* json, unsigned short* devicecount, sqlite3_int64* ForeignDeviceIDs, sqlite3_int64* DomesticDeviceIDs)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    int loop;
    struct json_object *device;
    struct json_object *device_devices;
    struct json_object *device_devices_entry;
    struct json_object *device_devices_device;
    struct json_object *field_value;

    device = json_tokener_parse(json);
    if (!device)
    {
        //TODO: log info: reject

        goto ERROR;
    }

    device_devices = json_object_object_get(device, JSON_DEVICE_DEVICES);
    if (!device_devices)
    {
        //TODO: log info: reject

        json_object_put(device);
        goto ERROR;
    }

    *devicecount = json_object_array_length(device_devices);

    for (loop = 0; loop < *devicecount; loop++)
    {
        device_devices_entry = json_object_array_get_idx(device_devices, loop);

        device_devices_device = json_object_object_get(device_devices_entry, JSON_DEVICE_DEVICES_DEVICE);
        if (!device_devices_device)
        {
            //TODO: log info: reject

            json_object_put(device);
            goto ERROR;
        }

        field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_ID);
        if (!field_value)
        {
            //TODO: log info: reject

            json_object_put(device);
            goto ERROR_STATEMENT;
        }
        ForeignDeviceIDs[loop] = json_object_get_int64(field_value);

        res = sqlite3_prepare_v3(db, SQL_QUERY_FOREIGNDEVICES_BY_DEVICEID_FOREIGNDEVICEID, -1, 0, &statement, &sqlBufferUnused);
        if (res)
        {
            json_object_put(device);
            goto ERROR;
        }

        //DeviceID
        res = sqlite3_bind_int64(statement, 1, oDeviceID);
        if (res)
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }
        //ForeignDeviceID
        res = sqlite3_bind_int64(statement, 2, ForeignDeviceIDs[loop]);
        if (res)
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }

        res = sqlite3_step(statement);
        if (res == SQLITE_ROW)
        {
            if (sqlite3_column_type(statement, 0) == SQLITE_NULL)
            {//It's null, as expected
                res = sqlite3_finalize(statement);
                if (res)
                {
                    //TODO: log error
                    json_object_put(device);
                    goto ERROR;
                }

                res = upsert_domesticdevice_by_json_device(db, device_devices_device, oDeviceID, ForeignDeviceIDs[loop], &DomesticDeviceIDs[loop]);
                if (res)
                {
                    //TODO: log error
                    json_object_put(device);
                    goto ERROR;
                }
            }
            else
            {//Already have this sensor mapped, so ignore for now
                DomesticDeviceIDs[loop] = sqlite3_column_int64(statement, 0);

                //TODO: consider what to do when the mapping already exists: should we update?

                res = sqlite3_finalize(statement);
                if (res)
                {
                    //TODO: log error
                    json_object_put(device);
                    goto ERROR;
                }
            }
        }
        else if (res == SQLITE_DONE)
        {//Need to map this device
            res = sqlite3_finalize(statement);
            if (res)
            {
                //TODO: log error
                json_object_put(device);
                goto ERROR;
            }

            res = upsert_domesticdevice_by_json_device(db, device_devices_device, oDeviceID, ForeignDeviceIDs[loop], &DomesticDeviceIDs[loop]);
            if (res)
            {
                //TODO: log error
                json_object_put(device);
                goto ERROR;
            }
        }
        else
        {
            json_object_put(device);
            goto ERROR_STATEMENT;
        }
    }

    res = json_object_put(device);

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int upsert_domesticdevice_by_json_device(sqlite3 *db, json_object *device_devices_device, sqlite3_int64 oDeviceID, sqlite3_int64 ForeignDeviceID, sqlite3_int64* DomesticDeviceID)
{
    struct json_object *field_value;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    int res;
    int loop;
    int hasnonzero;
    const unsigned char * text;
    __time_t updated;
    char cupdated[20];
    int update_Device;

    //Query device
    res = sqlite3_prepare_v3(db, SQL_QUERY_DEVICEID_BY_DEVICENAME, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    //DeviceName
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_NAME);
    if (!field_value)
    {
        //TODO: log info: reject
        goto ERROR_STATEMENT;
    }
    res = sqlite3_bind_text(statement, 1, json_object_get_string(field_value), -1, SQLITE_TRANSIENT);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    res = sqlite3_step(statement);
    if (res == SQLITE_ROW)
    {
        //DeviceID
        *DomesticDeviceID = sqlite3_column_int64(statement, 0);

        //Updated
        if (sqlite3_column_type(statement, 1) == SQLITE_NULL)
        {//Pre authorized
            update_Device = 1;
        }
        else
        {
            text = sqlite3_column_text(statement, 1);

            res = sqlite_datetime_to_time_t(text, &updated);
            if (res)
            {
                //TODO: log error
                goto ERROR_STATEMENT;
            }

            //JSON_DEVICE_DEVICE_UPDATEDS
            field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_UPDATEDS);
            if (!field_value)
            {
                //TODO: log error
                goto ERROR_STATEMENT;
            }
            else if (json_object_get_int64(field_value) == 0)
            {//This is a preauthorized device and cannot be updated since it has no data!
                update_Device = 0;
            }
            else
            {
                update_Device = (updated > json_object_get_int64(field_value)) ? 1 : 0;
            }
        }

        res = sqlite3_finalize(statement);
        if (res)
        {
            goto ERROR;
        }

        if (update_Device)
        {
            res = update_device_by_json_device(db, *DomesticDeviceID, device_devices_device);
            if (res)
            {
                //TODO: log error
                goto ERROR;
            }
        }

        res = update_foreign_device(db, oDeviceID, ForeignDeviceID, *DomesticDeviceID);
        if (res)
        {
            //TODO: log error
            goto ERROR;
        }
    }
    else if (SQLITE_DONE)
    {
        res = sqlite3_finalize(statement);
        if (res)
        {
            goto ERROR;
        }

        res = insert_device_by_json_device(db, oDeviceID, device_devices_device, DomesticDeviceID);
        if (res)
        {
            goto ERROR;
        }

        res = insert_foreign_device(db, oDeviceID, ForeignDeviceID, *DomesticDeviceID);
        if (res)
        {
            goto ERROR;
        }
    }
    else
    {
        goto ERROR_STATEMENT;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int insert_device_by_json_device(sqlite3 *db, sqlite3_int64 oDeviceID, json_object *device_devices_device, sqlite3_int64* DeviceID)
{
    struct json_object *field_value;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    sqlite3_stmt* statement_foreignsensors;
    int res;
    int loop;
    int hasnonzero;
    const char* text;
    __time_t updated;
    char cupdated[20];
    int update_Device;
    json_object *device_devices_device_sensors;
    json_object *device_devices_device_sensors_entry;
    unsigned int sensorcount;
    sqlite3_int64 SensorID;
    sqlite3_int64 DomesticSensorID;

    //Insert device
    res = sqlite3_prepare_v3(db, SQL_INSERT_DEVICES_FULL, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    res = sqlite3_prepare_v3(db, SQL_QUERY_FOREIGNSENSORS_BY_DEVICEID_FOREIGNDEVICEID_FOREIGNSENSORID, -1, 0, &statement_foreignsensors, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    //JSON_DEVICE_DEVICES_DEVICE_UUID
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_UUID);
    if (!field_value)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_bind_text(statement, 1, json_object_get_string(field_value), -1, SQLITE_TRANSIENT);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    //Updated
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_UPDATEDS);
    if (!field_value)
    {
        goto ERROR_STATEMENT;
    }
    else if (json_object_get_int64(field_value) == 0)
    {//This is a preauthorized device and cannot be updated since it has no data!
        res = sqlite3_bind_null(statement, 2);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }
    else
    {
        updated = json_object_get_int64(field_value);
        res = time_t_to_sqlite_datetime(&updated, (unsigned char *)cupdated);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_bind_text(statement, 2, cupdated, -1, SQLITE_STATIC);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }

    //DeviceType
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_TYPE);
    if (!field_value)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_bind_int(statement, 3, json_object_get_int(field_value));
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    //StorageStyle
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_STORAGESTYLE);
    if (!field_value)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_bind_int(statement, 4, json_object_get_int(field_value));
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    //DeviceName
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_NAME);
    if (!field_value)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_bind_text(statement, 5, json_object_get_string(field_value), -1, SQLITE_TRANSIENT);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    //BtAddress
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_BDADDR);
    if (!field_value)
    {
        hasnonzero = 0;
    }
    else
    {
        text = json_object_get_string(field_value);
        hasnonzero = 0;
        for (loop = 0; text[loop]; loop++)
        {
            if (loop % 3)
            {
                if (text[loop] != '0')
                {
                    hasnonzero = 1;
                    break;
                }
            }
        }
    }
    if (hasnonzero)
    {
        res = sqlite3_bind_text(statement, 6, text, -1, SQLITE_TRANSIENT);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }
    else
    {
        res = sqlite3_bind_null(statement, 6);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }

    //RFCOMMChannel
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_RFCOMMCHANNEL);
    if (!field_value)
    {
        res = sqlite3_bind_null(statement, 7);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }
    else
    {
        res = sqlite3_bind_int(statement, 7, json_object_get_int(field_value));
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }

    //PSM
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_PSM);
    if (!field_value)
    {
        res = sqlite3_bind_null(statement, 8);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }
    else
    {
        res = sqlite3_bind_int(statement, 8, json_object_get_int(field_value));
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }

    //I2PPublicKey
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_I2P);
    if (!field_value)
    {
        hasnonzero = 0;
    }
    else
    {
        text = json_object_get_string(field_value);
        hasnonzero = 0;
        for (loop = 0; text[loop]; loop++)
        {
            if (text[loop] != '0')
            {
                hasnonzero = 1;
                break;
            }
        }
    }
    if (hasnonzero)
    {//has I2P
        res = sqlite3_bind_text(statement, 9, text, -1, SQLITE_TRANSIENT);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }
    else
    {
        res = sqlite3_bind_null(statement, 9);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }

    //I2PPrivateKey
    res = sqlite3_bind_null(statement, 10);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        goto ERROR_STATEMENT;
    }
    *DeviceID = sqlite3_last_insert_rowid(db);

    //sensors
    device_devices_device_sensors = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_SENSORS);
    if (device_devices_device_sensors)
    {
        sensorcount = json_object_array_length(device_devices_device_sensors);

        for (loop = 0; loop < sensorcount; loop++)
        {
            device_devices_device_sensors_entry = json_object_array_get_idx(device_devices_device_sensors, loop);

            SensorID = json_object_get_int64(device_devices_device_sensors_entry);

            //DeviceID
            res = sqlite3_bind_int64(statement_foreignsensors, 1, oDeviceID);
            if (res)
            {
                goto ERROR_STATEMENT;
            }
            //ForeignDeviceID
            res = sqlite3_bind_int64(statement_foreignsensors, 2, *DeviceID);
            if (res)
            {
                goto ERROR_STATEMENT;
            }
            //ForeignSensorID
            res = sqlite3_bind_int64(statement_foreignsensors, 3, SensorID);
            if (res)
            {
                goto ERROR_STATEMENT;
            }

            res = sqlite3_step(statement_foreignsensors);
            if (res == SQLITE_DONE)
            {//Need to map this sensor, so insert as null map
                res = insert_unmapped_foreign_sensor(db, oDeviceID, *DeviceID, SensorID);
                if (res)
                {
                    //TODO: log error
                    goto ERROR_STATEMENT;
                }
            }
            else if (res == SQLITE_ROW)
            {
                if (sqlite3_column_type(statement_foreignsensors, 0) == SQLITE_NULL)
                {//It's still unmapped, so leave it be
                    //TODO: Reconsider this situation
                }
                else
                {//Already have this sensor mapped
                    DomesticSensorID = sqlite3_column_int64(statement, 0);
                }
            }
            else
            {
                goto ERROR_STATEMENT;
            }

            res = sqlite3_reset(statement_foreignsensors);
            if (res)
            {
                goto ERROR_STATEMENT;
            }
        }
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }
    res = sqlite3_finalize(statement_foreignsensors);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
    res = sqlite3_finalize(statement_foreignsensors);
ERROR:
    return -1;
}

int update_device_by_json_device(sqlite3 *db, sqlite3_int64 DeviceID, json_object *device_devices_device)
{
    struct json_object *field_value;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    int res;
    int loop;
    int hasnonzero;
    const char* text;
    __time_t updated;
    char cupdated[20];
    int update_Device;

    //Insert device
    res = sqlite3_prepare_v3(db, SQL_UPDATE_DEVICES, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    //UniversalID
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_UUID);
    if (!field_value)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_bind_text(statement, 1, json_object_get_string(field_value), -1, SQLITE_TRANSIENT);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    //Updated
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_UPDATEDS);
    if (!field_value)
    {
        goto ERROR_STATEMENT;
    }
    else if (json_object_get_int64(field_value) == 0)
    {//This is a preauthorized device and cannot be updated since it has no data!
        res = sqlite3_bind_null(statement, 2);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }
    else
    {
        updated = json_object_get_int64(field_value);
        res = time_t_to_sqlite_datetime(&updated, (unsigned char *)cupdated);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
        res = sqlite3_bind_text(statement, 2, cupdated, -1, SQLITE_STATIC);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }

    //DeviceType
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_TYPE);
    if (!field_value)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_bind_int(statement, 3, json_object_get_int(field_value));
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    //StorageStyle
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_STORAGESTYLE);
    if (!field_value)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_bind_int(statement, 4, json_object_get_int(field_value));
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    //DeviceName
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_NAME);
    if (!field_value)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_bind_text(statement, 5, json_object_get_string(field_value), -1, SQLITE_TRANSIENT);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    //BtAddress
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_BDADDR);
    if (!field_value)
    {
        hasnonzero = 0;
    }
    else
    {
        text = json_object_get_string(field_value);
        hasnonzero = 0;
        for (loop = 0; text[loop]; loop++)
        {
            if (loop % 3)
            {
                if (text[loop] != '0')
                {
                    hasnonzero = 1;
                    break;
                }
            }
        }
    }
    if (hasnonzero)
    {
        res = sqlite3_bind_text(statement, 6, text, -1, SQLITE_TRANSIENT);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }
    else
    {
        res = sqlite3_bind_null(statement, 6);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }

    //RFCOMMChannel
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_RFCOMMCHANNEL);
    if (!field_value)
    {
        res = sqlite3_bind_null(statement, 7);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }
    else
    {
        res = sqlite3_bind_int(statement, 7, json_object_get_int(field_value));
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }

    //PSM
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_PSM);
    if (!field_value)
    {
        res = sqlite3_bind_null(statement, 8);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }
    else
    {
        res = sqlite3_bind_int(statement, 8, json_object_get_int(field_value));
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }

    //I2PPublicKey
    field_value = json_object_object_get(device_devices_device, JSON_DEVICE_DEVICES_DEVICE_I2P);
    if (!field_value)
    {
        hasnonzero = 0;
    }
    else
    {
        text = json_object_get_string(field_value);
        hasnonzero = 0;
        for (loop = 0; text[loop]; loop++)
        {
            if (text[loop] != '0')
            {
                hasnonzero = 1;
                break;
            }
        }
    }
    if (hasnonzero)
    {//has I2P
        res = sqlite3_bind_text(statement, 9, text, -1, SQLITE_TRANSIENT);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }
    else
    {
        res = sqlite3_bind_null(statement, 9);
        if (res)
        {
            goto ERROR_STATEMENT;
        }
    }

    //I2PPrivateKey
    res = sqlite3_bind_null(statement, 10);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    //DeviceID
    res = sqlite3_bind_int64(statement, 11, DeviceID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        goto ERROR_STATEMENT;
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int handle_json_connection(const char* json, char* DeviceName, universal_id* puniversal_id)
{
    struct json_object *connection;
    struct json_object *connection_connection;
    struct json_object *connection_connection_name;
    struct json_object *connection_connection_name_value;
    struct json_object *connection_connection_id;
    struct json_object *connection_connection_id_value;
    int name_length;
    const char* name;
    int id_length;
    const char* id;
    int res;

    connection = json_tokener_parse(json);
    if (!connection)
    {
        //TODO: log info: reject

        return -1;
    }

    connection_connection = json_object_object_get(connection, JSON_CONNECTION_CONNECTION);
    if (!connection_connection)
    {
        //TODO: log info: reject

        json_object_put(connection);
        return -1;
    }

    connection_connection_name = json_object_object_get(connection_connection, JSON_CONNECTION_CONNECTION_NAME);
    if (!connection_connection_name)
    {
        //TODO: log info: reject

        json_object_put(connection);
        return -1;
    }

    name_length = json_object_get_string_len(connection_connection_name);
    name = json_object_get_string(connection_connection_name);
    strncpy(DeviceName, name, name_length);
    DeviceName[name_length] = '\0';

    connection_connection_id = json_object_object_get(connection_connection, JSON_CONNECTION_CONNECTION_UUID);
    if (!connection_connection_id)
    {
        //TODO: log info: reject

        json_object_put(connection);
        return -1;
    }

    id_length = json_object_get_string_len(connection_connection_id);
    id = json_object_get_string(connection_connection_id);
#ifdef USE_BASE_85
    assert(id_length == sizeof(universal_id_base85));
    res = decode_85(puniversal_id->ID, id, id_length);
#endif
#ifdef USE_BASE_64
    assert(id_length == sizeof(_universal_id_base64.ID_base64));
    res = decode_64(puniversal_id->ID, id, id_length);
#endif
    if (res != UNIVERSAL_ID_SIZE_BYTES)
    {
        //TODO: log info: reject

        json_object_put(connection);
        return -1;
    }

    json_object_put(connection);

    return 0;
}

int load_devicereqs_json(unsigned short devicecount, sqlite3_int64* ForeignDeviceID, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused)
{
    int res;
    struct json_object *devicereqs;
    struct json_object *devicereqs_array;
    struct json_object *field_value;
    int loop;
    const char* devicereqs_json;

    devicereqs = json_object_new_object();
    if (!devicereqs)
    {
        return -1;
    }

    devicereqs_array = json_object_new_array();
    if (!devicereqs_array)
    {
        json_object_put(devicereqs);
        return -1;
    }

    res = json_object_object_add(devicereqs, JSON_DEVICEREQ_DEVICEREQS, devicereqs_array);
    if (res)
    {
        json_object_put(devicereqs_array);
        json_object_put(devicereqs);
        return -1;
    }

    for (loop = 0; loop < devicecount; loop++)
    {
        field_value = json_object_new_int64(ForeignDeviceID[loop]);
        if (!field_value)
        {
            json_object_put(devicereqs);
            return -1;
        }
        res = json_object_array_add(devicereqs_array, field_value);
        if (res)
        {
            json_object_put(field_value);
            json_object_put(devicereqs);
            return -1;
        }
    }

    devicereqs_json = json_object_to_json_string_length(devicereqs, JSON_C_TO_STRING_PLAIN, cbuffer_sizeused);
    strncpy(cbuffer, devicereqs_json, *cbuffer_sizeused);

    json_object_put(devicereqs);

    //Success
    return 0;
}

int handle_json_devicereq(const char* json, unsigned int* devicecount, sqlite3_int64* DeviceIDs)
{
    struct json_object *devicereqs;
    struct json_object *devicereqs_array;
    struct json_object *devicereqs_array_entry;
    int loop;

    devicereqs = json_tokener_parse(json);
    if (!devicereqs)
    {
        //TODO: log info: reject

        return -1;
    }

    devicereqs_array = json_object_object_get(devicereqs, JSON_DEVICEREQ_DEVICEREQS);
    if (!devicereqs_array)
    {
        //TODO: log info: reject

        json_object_put(devicereqs);
        return -1;
    }

    *devicecount = json_object_array_length(devicereqs_array);

    for (loop = 0; loop < *devicecount; loop++)
    {
        devicereqs_array_entry = json_object_array_get_idx(devicereqs_array, loop);

        DeviceIDs[loop] = json_object_get_int64(devicereqs_array_entry);
    }

    json_object_put(devicereqs);

    //Success
    return 0;
}

int handle_json_devices(sqlite3 *db, sqlite3_int64 oDeviceID, const char* json)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    sqlite3_int64 DeviceID;
    sqlite3_int64 DomesticDeviceID;
    struct json_object *devices;
    struct json_object *devices_array;
    struct json_object *devices_array_entry;
    int loop;
    unsigned int devicecount;

    devices = json_tokener_parse(json);
    if (!devices)
    {
        //TODO: log info: reject

        return -1;
    }

    devices_array = json_object_object_get(devices, JSON_DEVICES_DEVICES);
    if (!devices_array)
    {
        //TODO: log info: reject

        json_object_put(devices);
        return -1;
    }

    devicecount = json_object_array_length(devices_array);

    res = sqlite3_prepare_v3(db, SQL_QUERY_FOREIGNDEVICES_BY_DEVICEID_FOREIGNDEVICEID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        json_object_put(devices);
        goto ERROR;
    }

    for (loop = 0; loop < devicecount; loop++)
    {
        devices_array_entry = json_object_array_get_idx(devices_array, loop);

        DeviceID = json_object_get_int64(devices_array_entry);

        //DeviceID
        res = sqlite3_bind_int64(statement, 1, oDeviceID);
        if (res)
        {
            json_object_put(devices);
            goto ERROR_STATEMENT;
        }
        //ForeignDeviceID
        res = sqlite3_bind_int64(statement, 2, DeviceID);
        if (res)
        {
            json_object_put(devices);
            goto ERROR_STATEMENT;
        }

        res = sqlite3_step(statement);
        if (res == SQLITE_DONE)
        {//Need to map this sensor, so insert as null map
            res = insert_unmapped_foreigndevice(db, oDeviceID, DeviceID);
            if (res)
            {
                json_object_put(devices);
                goto ERROR_STATEMENT;
            }
        }
        else if (res == SQLITE_ROW)
        {
            if (sqlite3_column_type(statement, 0) == SQLITE_NULL)
            {//It's still unmapped, so leave it be
                //TODO: Reconsider this situation
            }
            else
            {//Already have this sensor mapped
                DomesticDeviceID = sqlite3_column_int64(statement, 0);
            }
        }
        else
        {
            json_object_put(devices);
            goto ERROR_STATEMENT;
        }

        //reset
        res = sqlite3_reset(statement);
        if (res)
        {
            json_object_put(devices);
            goto ERROR_STATEMENT;
        }
    }

    json_object_put(devices);

    //Finish up
    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int insert_unmapped_foreigndevice(sqlite3 *db, sqlite3_int64 oDeviceID, sqlite3_int64 ForeignDeviceID)
{
    int res;
    sqlite3_stmt* statement_insert;
    const char* sqlBufferUnused;

    res = sqlite3_prepare_v3(db, SQL_INSERT_UNMAPPED_FOREIGNDEVICES_BY_DEVICEID_FOREIGNDEVICEID, -1, 0, &statement_insert, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    //DeviceID
    res = sqlite3_bind_int64(statement_insert, 1, oDeviceID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //ForeignDeviceID = pdevices_v1->DeviceIDs[loopDevice]
    res = sqlite3_bind_int64(statement_insert, 2, ForeignDeviceID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }

    res = sqlite3_step(statement_insert);
    if (res != SQLITE_DONE)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }

    res = sqlite3_finalize(statement_insert);
    if (res)
    {
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement_insert);
ERROR:
    return -1;
}

int load_json_devices(sqlite3 *db, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    int loop;
    struct json_object *devices;
    struct json_object *devices_array;
    struct json_object *field_value;
    const char* devices_json;

    devices = json_object_new_object();
    if (!devices)
    {
        return -1;
    }

    devices_array = json_object_new_array();
    if (!devices_array)
    {
        json_object_put(devices);
        return -1;
    }

    res = json_object_object_add(devices, JSON_DEVICE_DEVICES, devices_array);
    if (res)
    {
        json_object_put(devices_array);
        json_object_put(devices);
        return -1;
    }

    res = sqlite3_prepare_v3(db, SQL_QUERY_DEVICEID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        json_object_put(devices);
        goto ERROR;
    }

    for (loop = 0; ; loop++)
    {
        res = sqlite3_step(statement);
        if (res == SQLITE_DONE)
        {
            break;
        }
        if (res != SQLITE_ROW)
        {
            json_object_put(devices);
            goto ERROR_STATEMENT;
        }

        field_value = json_object_new_int64(sqlite3_column_int64(statement, 0));
        if (!field_value)
        {
            json_object_put(devices);
            goto ERROR_STATEMENT;
        }
        res = json_object_array_add(devices_array, field_value);
        if (res)
        {
            json_object_put(field_value);
            json_object_put(devices);
            goto ERROR_STATEMENT;
        }
    }

    devices_json = json_object_to_json_string_length(devices, JSON_C_TO_STRING_PLAIN, cbuffer_sizeused);
    strncpy(cbuffer, devices_json, *cbuffer_sizeused);
    json_object_put(devices);

    //Finish up
    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int load_json_sensor_detail_req(sqlite3 *db, sqlite3_int64 oDeviceID, unsigned short* sensorcount, sqlite3_int64* ForeignDeviceIDs, sqlite3_int64* ForeignSensorIDs, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused)
{
    int res;
    struct json_object *sensorreqs;
    struct json_object *sensorreqs_sensors;
    struct json_object *field_value;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    const char* devices_json;

    sensorreqs = json_object_new_object();
    if (!sensorreqs)
    {
        return -1;
    }

    sensorreqs_sensors = json_object_new_array();
    if (!sensorreqs_sensors)
    {
        json_object_put(sensorreqs);
        return -1;
    }

    res = json_object_object_add(sensorreqs, JSON_SENSORREQS_SENSORREQS, sensorreqs_sensors);
    if (res)
    {
        json_object_put(sensorreqs_sensors);
        json_object_put(sensorreqs);
        return -1;
    }

    res = sqlite3_prepare_v3(db, SQL_QUERY_UNMAPPED_FOREIGNSENSORS_BY_DEVICEID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //DeviceID
    res = sqlite3_bind_int64(statement, 1, oDeviceID);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }

    *sensorcount = 0;
    while(1)
    {
        res = sqlite3_step(statement);
        if (res == SQLITE_DONE)
        {
            break;
        }
        if (res != SQLITE_ROW)
        {
            //TODO: log error
            goto ERROR_STATEMENT;
        }

        ForeignDeviceIDs[*sensorcount] = sqlite3_column_int64(statement, 0);
        ForeignSensorIDs[*sensorcount] = sqlite3_column_int64(statement, 1);

        field_value = json_object_new_int64(ForeignSensorIDs[*sensorcount]);
        if (!field_value)
        {
            json_object_put(sensorreqs);
            goto ERROR_STATEMENT;
        }
        res = json_object_array_add(sensorreqs_sensors, field_value);
        if (res)
        {
            json_object_put(field_value);
            json_object_put(sensorreqs);
            goto ERROR_STATEMENT;
        }

        (*sensorcount)++;
    }

    devices_json = json_object_to_json_string_length(sensorreqs, JSON_C_TO_STRING_PLAIN, cbuffer_sizeused);
    strncpy(cbuffer, devices_json, *cbuffer_sizeused);
    json_object_put(sensorreqs);

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int handle_json_sensor_detail_req(const char* json, unsigned short* sensorcount, sqlite3_int64* DomesticSensorIDs)
{
    struct json_object *sensorreqs;
    struct json_object *sensorreqs_array;
    struct json_object *sensorreqs_array_entry;
    int loop;

    sensorreqs = json_tokener_parse(json);
    if (!sensorreqs)
    {
        //TODO: log info: reject

        return -1;
    }

    sensorreqs_array = json_object_object_get(sensorreqs, JSON_SENSORREQS_SENSORREQS);
    if (!sensorreqs_array)
    {
        //TODO: log info: reject

        json_object_put(sensorreqs);
        return -1;
    }

    *sensorcount = json_object_array_length(sensorreqs_array);

    for (loop = 0; loop < *sensorcount; loop++)
    {
        sensorreqs_array_entry = json_object_array_get_idx(sensorreqs_array, loop);

        DomesticSensorIDs[loop] = json_object_get_int64(sensorreqs_array_entry);
    }

    json_object_put(sensorreqs);

    //Success
    return 0;
}

int load_json_observation_req(sqlite3 *db, unsigned short sensorcount, sqlite3_int64* ForeignSensorIDs, char* cbuffer, size_t cbuffer_size, size_t* cbuffer_sizeused)
{
    return -1;
}

int handle_json_observation_req(sqlite3 *db, const char* json, unsigned short* sensorcount, sqlite3_int64* SensorIDs, struct timespec *tp)
{
    return -1;
}

int get_devices_i2p(sqlite3 *db, sqlite3_int64 mDeviceID, unsigned short* devicecount, sqlite3_int64* DeviceIDs, char* i2p_public_key_strings)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;
    const unsigned char* I2PPublicKey;

    res = sqlite3_prepare_v3(db, SQL_QUERY_DEVICEID_I2PPUBLICKEY, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }

    //DeviceID
    res = sqlite3_bind_int64(statement, 1, mDeviceID);
    if (res)
    {
        //TODO: log error
        goto ERROR_STATEMENT;
    }

    for ((*devicecount) = 0; ; (*devicecount)++)
    {
        res = sqlite3_step(statement);
        if (res == SQLITE_DONE)
        {
            break;
        }
        if (res != SQLITE_ROW)
        {
            goto ERROR_STATEMENT;
        }

        DeviceIDs[*devicecount] = sqlite3_column_int64(statement, 0);

        I2PPublicKey = sqlite3_column_text(statement, 1);
        if (!I2PPublicKey)
        {
            goto ERROR_STATEMENT;
        }
        memset(i2p_public_key_strings, 0, sqlite3_column_bytes(statement, 1) + 1);
        strncpy(i2p_public_key_strings, (const char*)I2PPublicKey, sqlite3_column_bytes(statement, 1));\
        i2p_public_key_strings += sqlite3_column_bytes(statement, 1) + 1;
    }

    //Finish up
    res = sqlite3_finalize(statement);
    if (res)
    {
        goto ERROR;
    }

    //Success
    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int get_last_logged_observationsint32(sqlite3 *db, sqlite3_int64 SensorID, struct timespec* tp)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;

    res = sqlite3_prepare_v3(db, SQL_QUERY_LAST_OBSERVATIONSINT_BY_SENSORID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //DeviceID = DeviceID
    res = sqlite3_bind_int64(statement, 1, SensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //ForeignDeviceID = ForeignDeviceID
    res = sqlite3_bind_int64(statement, 2, SensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res == SQLITE_DONE)
    {//None in DB
        tp->tv_sec = 0;
        tp->tv_nsec = 0;
    }
    else if (res != SQLITE_ROW)
    {
        //DateLoggedS
        tp->tv_sec = sqlite3_column_int64(statement, 0);
        //DateLoggedN
        tp->tv_nsec = sqlite3_column_int64(statement, 1);
    }
    else
    {
        goto ERROR_STATEMENT;
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int get_last_logged_observationsint64(sqlite3 *db, sqlite3_int64 SensorID, struct timespec* tp)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;

    res = sqlite3_prepare_v3(db, SQL_QUERY_LAST_OBSERVATIONSINT_BY_SENSORID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //DeviceID = DeviceID
    res = sqlite3_bind_int64(statement, 1, SensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //ForeignDeviceID = ForeignDeviceID
    res = sqlite3_bind_int64(statement, 2, SensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res == SQLITE_DONE)
    {//None in DB
        tp->tv_sec = 0;
        tp->tv_nsec = 0;
    }
    else if (res != SQLITE_ROW)
    {
        //DateLoggedS
        tp->tv_sec = sqlite3_column_int64(statement, 0);
        //DateLoggedN
        tp->tv_nsec = sqlite3_column_int64(statement, 1);
    }
    else
    {
        goto ERROR_STATEMENT;
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int get_last_logged_observationsreal(sqlite3 *db, sqlite3_int64 SensorID, struct timespec* tp)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;

    res = sqlite3_prepare_v3(db, SQL_QUERY_LAST_OBSERVATIONSREAL_BY_SENSORID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //DeviceID = DeviceID
    res = sqlite3_bind_int64(statement, 1, SensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //ForeignDeviceID = ForeignDeviceID
    res = sqlite3_bind_int64(statement, 2, SensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res == SQLITE_DONE)
    {//None in DB
        tp->tv_sec = 0;
        tp->tv_nsec = 0;
    }
    else if (res != SQLITE_ROW)
    {
        //DateLoggedS
        tp->tv_sec = sqlite3_column_int64(statement, 0);
        //DateLoggedN
        tp->tv_nsec = sqlite3_column_int64(statement, 1);
    }
    else
    {
        goto ERROR_STATEMENT;
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int get_last_logged_observationstext(sqlite3 *db, sqlite3_int64 SensorID, struct timespec* tp)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;

    res = sqlite3_prepare_v3(db, SQL_QUERY_LAST_OBSERVATIONSTEXT_BY_SENSORID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //DeviceID = DeviceID
    res = sqlite3_bind_int64(statement, 1, SensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //ForeignDeviceID = ForeignDeviceID
    res = sqlite3_bind_int64(statement, 2, SensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res == SQLITE_DONE)
    {//None in DB
        tp->tv_sec = 0;
        tp->tv_nsec = 0;
    }
    else if (res != SQLITE_ROW)
    {
        //DateLoggedS
        tp->tv_sec = sqlite3_column_int64(statement, 0);
        //DateLoggedN
        tp->tv_nsec = sqlite3_column_int64(statement, 1);
    }
    else
    {
        goto ERROR_STATEMENT;
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int get_last_logged_observationsblob(sqlite3 *db, sqlite3_int64 SensorID, struct timespec* tp)
{
    int res;
    const char* sqlBufferUnused;
    sqlite3_stmt* statement;

    res = sqlite3_prepare_v3(db, SQL_QUERY_LAST_OBSERVATIONSBLOB_BY_SENSORID, -1, 0, &statement, &sqlBufferUnused);
    if (res)
    {
        goto ERROR;
    }
    //DeviceID = DeviceID
    res = sqlite3_bind_int64(statement, 1, SensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    //ForeignDeviceID = ForeignDeviceID
    res = sqlite3_bind_int64(statement, 2, SensorID);
    if (res)
    {
        goto ERROR_STATEMENT;
    }
    res = sqlite3_step(statement);
    if (res == SQLITE_DONE)
    {//None in DB
        tp->tv_sec = 0;
        tp->tv_nsec = 0;
    }
    else if (res != SQLITE_ROW)
    {
        //DateLoggedS
        tp->tv_sec = sqlite3_column_int64(statement, 0);
        //DateLoggedN
        tp->tv_nsec = sqlite3_column_int64(statement, 1);
    }
    else
    {
        goto ERROR_STATEMENT;
    }

    res = sqlite3_finalize(statement);
    if (res)
    {
        //TODO: log error
        goto ERROR;
    }

    return 0;

ERROR_STATEMENT:
    res = sqlite3_finalize(statement);
ERROR:
    return -1;
}

int log_observationsint32(sqlite3_stmt* statement, int value)
{
    int res;
    struct timespec tp;

    //DateLogged = NOW()
    res = NOW(&tp);
    if (res)
    {
        return -1;
    }
    res = sqlite3_bind_int64(statement, 2, tp.tv_sec);
    if (res)
    {
        return -1;
    }
    res = sqlite3_bind_int64(statement, 3, tp.tv_nsec);
    if (res)
    {
        return -1;
    }
    //Value = value
    res = sqlite3_bind_int(statement, 4, value);
    if (res)
    {
        return -1;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        return -1;
    }
    res = sqlite3_reset(statement);
    if (res)
    {
        return -1;
    }

    return 0;
}

int log_observationsint64(sqlite3_stmt* statement, sqlite3_int64 value)
{
    int res;
    struct timespec tp;

    //DateLogged = NOW()
    res = NOW(&tp);
    if (res)
    {
        return -1;
    }
    res = sqlite3_bind_int64(statement, 2, tp.tv_sec);
    if (res)
    {
        return -1;
    }
    res = sqlite3_bind_int64(statement, 3, tp.tv_nsec);
    if (res)
    {
        return -1;
    }
    //Value = value
    res = sqlite3_bind_int64(statement, 4, value);
    if (res)
    {
        return -1;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        return -1;
    }
    res = sqlite3_reset(statement);
    if (res)
    {
        return -1;
    }

    return 0;
}

int log_observationsreal(sqlite3_stmt* statement, double value)
{
    int res;
    struct timespec tp;

    //DateLogged = NOW()
    res = NOW(&tp);
    if (res)
    {
        return -1;
    }
    res = sqlite3_bind_int64(statement, 2, tp.tv_sec);
    if (res)
    {
        return -1;
    }
    res = sqlite3_bind_int64(statement, 3, tp.tv_nsec);
    if (res)
    {
        return -1;
    }
    //Value = value
    res = sqlite3_bind_double(statement, 4, value);
    if (res)
    {
        return -1;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        return -1;
    }
    res = sqlite3_reset(statement);
    if (res)
    {
        return -1;
    }

    return 0;
}

int log_observationstext(sqlite3_stmt* statement, const char* value)
{
    int res;
    struct timespec tp;

    //DateLogged = NOW()
    res = NOW(&tp);
    if (res)
    {
        return -1;
    }
    res = sqlite3_bind_int64(statement, 2, tp.tv_sec);
    if (res)
    {
        return -1;
    }
    res = sqlite3_bind_int64(statement, 3, tp.tv_nsec);
    if (res)
    {
        return -1;
    }
    //Value = value
    res = sqlite3_bind_text(statement, 4, value, -1, SQLITE_STATIC);
    if (res)
    {
        return -1;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        return -1;
    }
    res = sqlite3_reset(statement);
    if (res)
    {
        return -1;
    }

    return 0;
}

int log_observationsblob(sqlite3_stmt* statement, const char* value, unsigned int length)
{
    int res;
    struct timespec tp;

    //DateLogged = NOW()
    res = NOW(&tp);
    if (res)
    {
        return -1;
    }
    res = sqlite3_bind_int64(statement, 2, tp.tv_sec);
    if (res)
    {
        return -1;
    }
    res = sqlite3_bind_int64(statement, 3, tp.tv_nsec);
    if (res)
    {
        return -1;
    }
    //Value = value
    res = sqlite3_bind_blob(statement, 4, value, length, SQLITE_STATIC);
    if (res)
    {
        return -1;
    }
    res = sqlite3_step(statement);
    if (res != SQLITE_DONE)
    {
        return -1;
    }
    res = sqlite3_reset(statement);
    if (res)
    {
        return -1;
    }

    return 0;
}

int sqlite_datetime_to_time_t(const unsigned char* Datetime, __time_t* time_t)
{
    char tmp[5] = { 0 };
    //The datetime() function returns "YYYY-MM-DD HH:MM:SS"
    if (strlen((const char*)Datetime) != 19)
    {
        return -1;
    }

    struct tm _tm = {0};
    strncpy(tmp, (const char*)&Datetime[0], 4);
    tmp[4] = '\0';
    _tm.tm_year = atoi(tmp);
    strncpy(tmp, (const char*)&Datetime[5], 2);
    tmp[2] = '\0';
    _tm.tm_mon = atoi(tmp);
    strncpy(tmp, (const char*)&Datetime[8], 2);
    _tm.tm_mday = atoi(tmp);
    strncpy(tmp, (const char*)&Datetime[11], 2);
    _tm.tm_hour = atoi(tmp);
    strncpy(tmp, (const char*)&Datetime[14], 2);
    _tm.tm_min = atoi(tmp);
    strncpy(tmp, (const char*)&Datetime[17], 2);
    _tm.tm_sec = atoi(tmp);

    *time_t = mktime(&_tm);

    return 0;
}

int time_t_to_sqlite_datetime(const __time_t* time_t, unsigned char* Datetime)
{
    struct tm* _tm = gmtime(time_t);
    snprintf((char*)Datetime, 20, "%04d-%02d-%02d %02d:%02d:%02d",
        _tm->tm_year,
        _tm->tm_mon,
        _tm->tm_mday,
        _tm->tm_hour,
        _tm->tm_min,
        _tm->tm_sec);

    return 0;
}
