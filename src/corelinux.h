#ifndef _CORELINUX_H_
#define _CORELINUX_H_

#include <unistd.h>
#include "core.h"

//Config
int create_configfile(const char* ConfigFilename, const char* dbFilename, sqlite3_int64 DeviceID);
int check_configfile(const char* ConfigFilename, const char* dbFilename, sqlite3_int64* DeviceID);
int delete_configfile(const char* ConfigFilename);

#endif
