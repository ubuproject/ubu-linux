#include <stdio.h>
#include <sodium.h>
#include <libconfig.h>
#include "core.h"
#include "corelinux.h"
#include "bluetooth.h"
#include "i2pjava.h"
#include "xwindows.h"

int main(int argc, const char** argv)
{
    int res;
    sqlite3 *db;
    const char* ConfigFilename;
    char* dbFilename;
    sqlite3_int64 DeviceID;
    sqlite3_int64 SensorID;
    sqlite3_stmt* statement;

    if (sodium_init() == -1)
    {
        return 1;
    }

    ConfigFilename = NULL;
    dbFilename = NULL;

    res = check_configfile(ConfigFilename, dbFilename, &DeviceID);
    if (!res)
    {
        fprintf(stderr, "check_config\n");
        return -1;
    }

    res = create_db(dbFilename, NULL, &db, &DeviceID);
    if (res)
    {
        fprintf(stderr, "create_db\n");
        return -1;
    }

    res = create_configfile(ConfigFilename, dbFilename, DeviceID);
    if (res)
    {
        fprintf(stderr, "create_config\n");
        return -1;
    }

    res = connect_to_db(dbFilename, DeviceID, &db);
    if (res)
    {
        fprintf(stderr, "connect_to_db\n");
        return -1;
    }

    //XWindow
    res = create_db_sensor(db, DeviceID, XWINDOWS_SENSOR, XWINDOWS_SENSOR_TYPE, XWINDOWS_OBSERVATION_TYPE, &SensorID);
    if (res)
    {
        fprintf(stderr, "create_db_sensor\n");
        return -1;
    }

    res = prepare_log_observationstext(db, SensorID, &statement);
    if (res)
    {
        fprintf(stderr, "prepare_log_observations_text\n");
        return -1;
    }

    res = close_log_observationstext(statement);
    if (res)
    {
        fprintf(stderr, "close_log_observationstext\n");
        return -1;
    }

    res = delete_db(dbFilename, db);
    if (res)
    {
        fprintf(stderr, "delete_db\n");
        return -1;
    }

    res = delete_configfile(ConfigFilename);
    if (res)
    {
        fprintf(stderr, "delete_config\n");
        return -1;
    }

    return 0;
}
